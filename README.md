# unibackend
行业后端
子目录special-equipment-backend才是主工程。Gradle面板任务要针对子目录工程。
主要技术 springboot, JPA, graphQL, Elasticsearch, QueryDSL。

# 运行
IDEA开发工具;  
进入设置: 选择Settings/Build../Build tools/Gradle
Settings配置界面上Build and run using 请用Gradle方式。
Gradle面板--special-equipment-backend--Tasks--other--compileQuerydsl要先运行。
然后再--Tasks--build--build点击运行。
gradle栏目点选special-equipment-backend/Tasks/build/bootJar编译打包。
命令行启动后端 CMD> 窗口；
cd \unibackend\special-equipment-backend\build\libs 目录;
java -jar -Dspring.profiles.active=dev spec-SNAPSHOT???.jar

# 调试
IDEA开发工具;
Gradle面板 --Tasks--build--build或bootJar执行之后；
然后IDEA抬头工具栏的铁锤图标右边列表框，选择StartApplication程序;
程序列表框选择Edit Configurations...进入配置：
在Active profiles 输入 dev
在Shorten command line 选择 @argfile java9+;
配置完再点程序列表框右边Debug图标。
启动ES服务器 E:\del\elasticsearch-7.13.2\bin> .\elasticsearch 。
