package org.fjsei.yewu.service;

import md.cm.base.Person;
import md.cm.geography.Adminunit;
import md.specialEqp.Eqp;
import md.specialEqp.type.PipingUnit;
import org.fjsei.yewu.dto.SearchRequest;
import org.fjsei.yewu.dto.SearchResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class SearchServiceTest {
    

    @Autowired
    private SearchService searchService;


    @Test
    public void testSearchService() {

        SearchRequest request = new SearchRequest("Maßnahmen", "", null,null);
        //SearchResponse<? extends Eqp> result = searchService.searchQuery(request);
        SearchResponse<PipingUnit> result = searchService.searchQuery(request);
        assertEquals(2, result.getResult().size());
    
    }
}
