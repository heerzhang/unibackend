package org.fjsei.yewu.repository;

import md.cm.geography.AdminunitRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class ArticleRepositoryTest {
    
    @Autowired
    private AdminunitRepository adminunitRepository;
    
    @Test
    public void should_return_all_articles() {
        var articles = adminunitRepository.count();
        assertEquals(2, articles);

    }
}
