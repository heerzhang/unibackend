package org.fjsei.yewu;

import com.fasterxml.jackson.core.type.TypeReference;
import org.fjsei.yewu.bpm.camunda.constants.ProcessConstants;
import org.fjsei.yewu.bpm.camunda.model.vo.TaskVo;
import org.fjsei.yewu.bpm.common.enums.RespCodeEnum;
import org.fjsei.yewu.bpm.common.model.result.RespResult;
import org.fjsei.yewu.bpm.common.test.BaseTest;
import org.fjsei.yewu.bpm.common.test.JsonFileSource;
import org.fjsei.yewu.bpm.common.utils.JsonUtils;
import org.fjsei.yewu.constants.PaymentProcessConstants;
import org.fjsei.yewu.pojo.enums.ConfirmResultEnum;
import org.fjsei.yewu.model.entity.BizPaymentProcessInfo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**测试类和被测试主类的目录不一致的，无法注入！
 * 原本可以正常启动，装入org.fjsei.yewu旧的整体包后报错：jakarta.websocket.server.ServerContainer not available
 * 测试类的包应该和被测试类保持一致!
 *【限制】测试类的基本包路径名称必须和母包一致；Controller无法调用和注入？。
 * 测试类所在包与启动类所在包名不一致时需要手动指定！
 * 把@SpringBootTest(classes = StartApplication.class)加上=报错jakarta.websocket.server.ServerContainer not available
 * camunda - 支付流程 - 测试
 * 被测试类包路径Controller目录必须能够注入资源否则：No qualifying bean of type 'org.fjsei.yewu.service.JpaService' available:
 */

@SpringBootTest(webEnvironment=SpringBootTest.WebEnvironment.DEFINED_PORT)
@TestMethodOrder(MethodOrderer.MethodName.class)
public class StartApplicationTest extends BaseTest {

    private final String BASE_PATH = "/payment";

    @ParameterizedTest
    @JsonFileSource(jsonKey = "$.startPayment")
    void test01_startPayment(String paymentRequestJson) throws Exception {
        //paymentRequestJson ="{"productName": "单板吉他","productPrice": 2100,"paymentAssignee": "luo"}" ;
        this.mockMvc.perform(post(BASE_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(paymentRequestJson))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_RESP_CODE).value(RespCodeEnum.SUCCESS.getCode()));
    }

    @Test
    void test02_getTasks() throws Exception {
        String taskListResultJson = this.mockMvc.perform(get(BASE_PATH + "/tasks")
                .queryParam(PARAM_ORDER_BY, ProcessConstants.COLUMN_CREATE_TIME)
                .queryParam(PARAM_ORDER_SORT, ProcessConstants.SORT_DESC)
                .queryParam("assignee", "demo")
                .queryParam("processDefinitionKey", PaymentProcessConstants.PAYMENT_PROCESS_ID)
                .queryParam("taskDefinitionKey", PaymentProcessConstants.PAYMENT_USER_CONFIRM_TASK_ID)
                .queryParam("withProcessVariables", "true"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_RESP_CODE).value(RespCodeEnum.SUCCESS.getCode()))
                .andReturn()
                .getResponse().getContentAsString();
        //获取最近创建的待处理任务
        RespResult<TaskVo<BizPaymentProcessInfo>> respResult = JsonUtils.fromJson(taskListResultJson, new TypeReference<RespResult<TaskVo<BizPaymentProcessInfo>>>() {});
        TaskVo<BizPaymentProcessInfo> curTaskVo = respResult.getRows().get(0);
    }

    /*controller目录是在不同一个路径的包，不能测试：
        无法映射
    [           main] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped to ResourceHttpRequestHandler [Classpath [META-INF/resources/], Classpath [resources/], Classpath [static/], Classpath [public/], ServletContext [/]]
    [           main] o.s.w.s.r.ResourceHttpRequestHandler     : Resource not found
        正常是
    [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped to com.luo.camunda.app.controller.P
    */
    @Test
    void test03_confirmPayment() throws Exception {
        //setAuthentication("user", "password", session);
        //查询用户的待处理任务列表
        //改成mockMvc.perform(get("/teacher" + "/tasks") 就无法实际调用成功，@Test MockMvc无法像真实的http访问那样被过滤器filter拦截？
        String taskListResultJson = mockMvc.perform(get(BASE_PATH + "/tasks")
                .queryParam(PARAM_ORDER_BY, ProcessConstants.COLUMN_CREATE_TIME)
                .queryParam(PARAM_ORDER_SORT, ProcessConstants.SORT_DESC)
                .queryParam("assignee", "camunda-admin")
                .queryParam("processDefinitionKey", PaymentProcessConstants.PAYMENT_PROCESS_ID)
                .queryParam("taskDefinitionKey", PaymentProcessConstants.PAYMENT_USER_CONFIRM_TASK_ID)
                .queryParam("withProcessVariables", "true"))
                .andDo(print())
                .andReturn()
                .getResponse().getContentAsString();
        //获取最近创建的待处理任务
        RespResult<TaskVo<BizPaymentProcessInfo>> respResult = JsonUtils.fromJson(taskListResultJson, new TypeReference<RespResult<TaskVo<BizPaymentProcessInfo>>>() {});
        TaskVo<BizPaymentProcessInfo> curTaskVo = respResult.getRows().get(0);
        Assertions.assertNotNull(curTaskVo);

        //拼接用户确认参数
        String confirmJsonParamFormat = "{\"taskId\":\"%s\", \"processInstanceId\":\"%s\", \"bizKey\": \"%d\", \"approvalResult\": %d}";
        String confirmJsonParam = String.format(confirmJsonParamFormat,
                curTaskVo.getId(),
                curTaskVo.getProcessInstanceId(),
                curTaskVo.getBizProcessData().getId(),
                1);
        //调用确认接口
        this.mockMvc.perform(put(BASE_PATH + "/confirm")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(UTF_8)
                .content(confirmJsonParam))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_RESP_CODE).value(RespCodeEnum.SUCCESS.getCode()));
    }

    @Test
    void test04_getHistoryTasks() throws Exception {
        this.mockMvc.perform(get(BASE_PATH + "/tasks/history")
                .queryParam(PARAM_ORDER_BY, ProcessConstants.COLUMN_START_TIME)
                .queryParam(PARAM_ORDER_SORT, ProcessConstants.SORT_DESC)
                .queryParam("assignee", "luo")
                .queryParam("processDefinitionKey", PaymentProcessConstants.PAYMENT_PROCESS_ID)
                //.queryParam("taskDefinitionKey", PaymentProcessConstants.PAYMENT_USER_CONFIRM_TASK_ID)
                .queryParam("withProcessVariables", "true"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_RESP_CODE).value(RespCodeEnum.SUCCESS.getCode()));
    }

    @Test
    void test05_getPayments_all() throws Exception {
        this.mockMvc.perform(get(BASE_PATH)
                .queryParam(PARAM_PAGE_NO, "1")
                .queryParam(PARAM_PAGE_SIZE, "10")
                .queryParam(PARAM_ORDER_BY, "createTime")
                .queryParam(PARAM_ORDER_SORT, "desc"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_RESP_CODE).value(RespCodeEnum.SUCCESS.getCode()));
    }

    @Test
    void test06_getPayments_not_confirm() throws Exception {
        this.mockMvc.perform(get(BASE_PATH)
                .queryParam(PARAM_PAGE_NO, "1")
                .queryParam(PARAM_PAGE_SIZE, "10")
                .queryParam(PARAM_ORDER_BY, "createTime")
                .queryParam(PARAM_ORDER_SORT, "desc")
                .queryParam("approvalResult", ConfirmResultEnum.NOT_CONFIRM.getCode().toString()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_RESP_CODE).value(RespCodeEnum.SUCCESS.getCode()));
    }

    @Test
    void test07_getPayments_with_condition() throws Exception {
        this.mockMvc.perform(get(BASE_PATH)
                .queryParam(PARAM_PAGE_NO, "1")
                .queryParam(PARAM_PAGE_SIZE, "10")
                .queryParam(PARAM_ORDER_BY, "createTime")
                .queryParam(PARAM_ORDER_SORT, "desc")
                .queryParam("approvalResult", ConfirmResultEnum.CONFIRM_PAY.getCode().toString())
                .queryParam("paymentResult", PaymentResultEnum.PAY_SUCCESS.getCode().toString())
                .queryParam("productName", "吉他")
                .queryParam("productDiscountPriceStart", "1000")
                .queryParam("productDiscountPriceEnd", "5000")
                .queryParam("paymentTimeStart", "2022-01-01 00:00:00")
                .queryParam("paymentTimeEnd", "2022-12-31 12:03:32")
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_RESP_CODE).value(RespCodeEnum.SUCCESS.getCode()));
    }
}
