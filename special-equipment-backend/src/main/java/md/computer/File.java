package md.computer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import md.specialEqp.Report;
import md.system.User;

import jakarta.persistence.*;
import java.util.UUID;


/**上传文件： 数量也不少；
 * 根据文件性质 分级别 预定过期日期，太久的文件可删除？ 报告20年内都会用关联图片文件。
 * File模型存在的意义？ 文件URL,<->报告申请单实体表ID;
 * */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "FILES")
public class File {
    /*@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "filesSeq")
    @SequenceGenerator(name = "filesSeq", initialValue = 1, allocationSize = 1, sequenceName = "SEQUENCE_FILES")
    protected Long id;*/

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    /**实际的文件系统的路径名, 本地或者云挂载的文件系统、存储路径;
     * 关联对象其实可以直接File的ID就可以了， 也可以直接http://:GetFile(ID); 还可能直接给对方底层实际的存储url。
     * 文件类型 *.pdf , *.jpg 根据后缀判定
     * 上传文件后，转移存储路径；给哪一个申请单报告书做的上传附件文件。
    */
    @Column(length =512)
    private String  url;

    //纳入用户系统控制。
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn
    private User creator;

    private boolean anyoneCanSee=false;

    /**报告=原始记录的附带图片等的文件
     * 文件删除前 必须处理完成关联实体对象Report报告 的锁定需求。
     * 单个文件只能由唯一个报告来使用的。 多人次复用情形=文件复制模式。 除了单线图其它图片或文件可重复引用的机会不大。
     * 单线图可能由于管道 工程 变动后的再次修改。?单线图也是要单独复制一份给单独的一次性报告。
    */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private Report report;

    //【难道】其他各种种类的关联申请单等实体？ 申请书的附件上传++对方实体ID。

}

