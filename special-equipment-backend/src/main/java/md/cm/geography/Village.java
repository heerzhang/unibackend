package md.cm.geography;

import graphql.schema.DataFetchingEnvironment;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import md.cm.base.Company;
import md.specialEqp.Eqp;
import org.fjsei.yewu.filter.NodeName;
import org.fjsei.yewu.filter.Uunode;
import org.fjsei.yewu.util.Tool;
import org.hibernate.search.engine.backend.types.Aggregable;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.KeywordField;

import jakarta.persistence.*;
import java.util.Set;
import java.util.UUID;

//楼盘的实体概念。 community ，小区名字；楼盘=地址的泛房型表达式; 广义模糊地址；
/**SQL查詢可以分辨城Ⅲ期不同城III期，可是唯一索引@UniqueConstraint(columnNames={"name", "aid"}無法分辨這個的不同點。
 * 導致報錯 Duplicate entry '永安市五洲第一城Ⅲ期-432' for key 'village.UKm8khvk7rbhctnh3yewrooe6rb' 永安市五洲第一城III期
 * 检验BUILD_ID楼盘，监察是BUILDNAME;
 * Village没有单独的ES索引；可是EqpEs索引直接为Village附带了{id+name}两个字段的ES索引。不用它只能走关系数据库查询更慢。
 * */

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table( uniqueConstraints = {@UniqueConstraint(columnNames={"name", "aid"})} ,
        indexes={@Index(columnList = "oldId",unique=false), }
)
public class Village implements Uunode, NodeName {
    //报错 Field 'id' doesn't have a default value 解决办法1删除表重建，办法2数据库手动id字段上加Auto Inc的✔选择。
    @KeywordField(aggregable=Aggregable.YES)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    //简单公用叫法的。
    /**楼盘 维护管理：后台进行，管理员的接口。
     * */
    @FullTextField
    private String  name;  //楼盘名     BUILD_NAME  TB_HOUSE_MGE
    //监察平台=〉维保单位覆盖楼盘统计　楼盘名称：from eqp  Where mtU.=UNT_NAME  Group by pos.vlg.BUILDNAME;

    /**楼盘性质： Village仅仅是为了添加楼盘性质的？
     * INST_BUILD_TYPE 监察没有
     * .INST_BUILD_TYPE楼盘性质。,商品房、复建房、拆迁安置房、廉租房、回迁房、经济适用房、限价房,棚户区。
     * 计费有用， 谁来裁定的。？？还有实际上意义吗
     * */
    private String type;

    //小区楼盘底下的　所有已经声明的地址。
   // @OneToMany(mappedBy = "vlg")
   // private Set<Address>  adrs;
    /**楼盘只能关联于最小行政区划Town乡镇级别下面，不允许关联高级别的县城市区,还未决定的允许null。
     * 楼盘只能属于一个Adminunit; 最低行政级别的Adminunit？ 若是有重叠的/高级别+低结别的要选择低的！。
     *【原则】一个楼盘只能位于一个 乡镇街道的 内部底下:Adminunit ad都要分解到了街道级别；
     * 假如 国外地址？或Adminunit中间有些行政级别缺失没数据？也必须敲定最小级别的行政区域（乡镇街道）。楼盘字段用处也不大，使用得到？
     【注意】行政区划必须尽量地按照最低级别的选定；若是选择更高级别的会导致关联对象的集合列表太长，降低搜索性能。
     * */
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name = "aid")
    private Adminunit  ad;       //行政区划

    /**监察平台 楼盘只有name没有楼盘id; 对于厦门地区的设备就无法和旧检验同步！
     * 厦门地区的设备派生的楼盘 ，Adminunit!=null前提下+：[同步时刻]在本平台自动增加一个Village： 但设置oldId=0；address=null; 特别标记!
     * */
    private Long  oldId;    //对接旧系统TB_HOUSE_MGE
    /*原先没觉得需要，单独再指明说明 地址的。区域+楼盘名字自己就是就能覆盖掉地址含义。
    该字段是多此一举？？反而导致Eqp的录入的地址变味，变成随意的楼盘和使用单位底下的附带的地址叙述。
    * */
    //private String  address;     //前面行政地理描述部分要省略掉。
    private String address;     //对接旧系统的楼盘的地址  BUILD_ADDR

    //Hibernate Search 要求添加的字段！
    @OneToMany(mappedBy = "vlg")
    private Set<Eqp>  eqps;


    public String getId(DataFetchingEnvironment env) {
        return Tool.toGlobalId(this.getClass().getSimpleName(), this.id);
    }
}

//旧的　TB_HOUSE_MGE   "BUILD_ID"
//楼盘+管理小区场所；大厦标志名。　private String BUILD_NAME;
