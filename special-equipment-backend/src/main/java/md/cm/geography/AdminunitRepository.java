package md.cm.geography;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;


@RepositoryRestResource
public interface AdminunitRepository extends JpaRepository<Adminunit, UUID>, JpaSpecificationExecutor<Adminunit>, QuerydslPredicateExecutor<Adminunit> {
    Adminunit findByTownIs(Town town);   //一个Town最多只能一个的；
    Adminunit findTopByAreacode(String  areacode);
    Adminunit readAdminunitByProvinceAndAndCity(Province province,City city);
    Adminunit readAdminunitByCityAndAndCounty(City city,County county);
    Adminunit readAdminunitByCountyAndTown(County county,Town town);
}
