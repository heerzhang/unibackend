package md.cm.geography;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.UUID;

public interface VillageRepository extends JpaRepository<Village, UUID>, JpaSpecificationExecutor<Village>, QuerydslPredicateExecutor<Village> {
    Village findTopByOldId(Long oldId);
    Village findByAdAndName(Adminunit adminunit,String name);

}
