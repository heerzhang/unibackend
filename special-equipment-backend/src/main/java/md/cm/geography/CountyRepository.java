package md.cm.geography;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.UUID;

public interface CountyRepository extends JpaRepository<County, UUID>, JpaSpecificationExecutor<County> {
}