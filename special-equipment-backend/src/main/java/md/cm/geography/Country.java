package md.cm.geography;

import graphql.schema.DataFetchingEnvironment;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.fjsei.yewu.filter.Node;
import org.fjsei.yewu.filter.Uunode;
import org.fjsei.yewu.util.Tool;
import org.hibernate.search.engine.backend.types.Aggregable;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.KeywordField;

import jakarta.persistence.*;
import java.util.Set;
import java.util.UUID;

//国家和地区：不代表是主权政治国；是通信邮政划分，全球第一级分区/ 国际级地区级别。
//Country头上面没有更高一级的行政机构：前端选择行政区域会需要 0号的 查询点。
@Getter
@Setter
@NoArgsConstructor
@Entity
public class Country  implements Uunode {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @KeywordField(aggregable = Aggregable.YES)
    private UUID id;
    //name=搜索名，不一定要全称呼的。 中华xxx;
    //null=世界;
    @Column(unique = true)
    private String  name;        //世界--》国家和地区。
    @OneToMany(mappedBy = "country")
    private Set<Adminunit> ads;
    //没有 ： 上级行政关系的关联：
    //下级行政关系的关联：
    @OneToMany(mappedBy = "country")
    private Set<Province>   provinces;
    //下一级行政关系坍塌,下一级关联区划实际上是摆设：只有一条虚拟的记录。
    //国家级新加坡->省一级新加坡->地市一级新加坡；可以省略掉2层区划级别。
    //搜索的时候，支持 按照城市那一级， 按国家级，都算有效的搜索匹配区域。
    private Boolean collapse;
    /**区分 7个大洲；     直接上文本Enum;
     * 归属哪一个洲；亚洲、欧洲、北美、南美、非洲、大洋洲,南极洲
     * */
    private String  continent;

    @ManyToOne
    private Adminunit  adm;

}
//新加坡实际是地市级别提升到国家级。

//报错Object type 'Country' implements a known interface, but no class could be found for that type name.  Please pass a class for type 'Country' in the parser's dictionary.
//但是Province不会报错，因为Country内部关联用了Province，所以只要有用到的就不会被省略掉而导致报错。
//最后解决：规避graphQL运行报错用的。增加一个关联引用到了就可以了，比如加入一个字段：type Adminunit{  country: Country }，有使用了就不会自动舍弃。
