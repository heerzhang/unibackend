package md.cm.unit;


import md.cm.base.Company;
import md.cm.base.Person;
import org.fjsei.yewu.jpa.NormalExecutor;
import org.springframework.graphql.data.GraphQlRepository;

import java.util.UUID;

@GraphQlRepository
public interface Units extends NormalExecutor<Unit, UUID> {
    Unit findUnitByPerson_Id(UUID id);
    Unit findUnitByCompany_Id(UUID id);
    Unit findUnitByPerson(Person person);
    Unit findUnitByCompany(Company company);
  //  Unit getUnitByJcId(Long jcid);
  //  Unit getUnitByOldId(Long oldid);
    Unit findUnitByCompany_Name(String companyName);
    /*
    这里添加的 函数如何不合格的，将会导致启动报错 无法启动，但编译器不会报错的！！
    */
    Unit findUnitByCompany_No(String companyNo);
}

