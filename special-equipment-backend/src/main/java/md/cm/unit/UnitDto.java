package md.cm.unit;
import lombok.Value;
import md.specialEqp.BusinessCat_Enum;

import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import java.time.LocalDate;
import java.util.Date;
import java.util.UUID;

/**
 * Unit 实体类投影
 * 不能替换实体类！
 * */

@Value
public class UnitDto {
    private UUID id;
    private String indCod;
    private String area;

    public  UnitDto(Unit unit){
        this.id =unit.getId();
        this.indCod =unit.getIndCod();
        this.area =unit.getArea();
    }
}

