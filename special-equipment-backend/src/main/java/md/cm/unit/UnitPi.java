package md.cm.unit;



import md.cm.base.Company;
import md.cm.base.PersonPi;

import java.util.UUID;

/**为提高性能 投影用的
 * Interface-based Projections方案
 * 非得手动再写函数名： lombok.Value; @Value is only supported on a class.
* */
public interface UnitPi {
    UUID getId();
    String getIndCod();
    String getArea();
    Boolean getCancel();

    //CompanyPi  getCompany();    //嵌套的投影过滤字段; 默认全部字段都读取； Company getCompany()
    Company getCompany();       //CompanyPi Company两个都能支持的！

    PersonPi getPerson();         //PersonIF  getPerson()

    //void setArea(String s);  做了setXXX只能影响到返回给前端看得对象内容。无法影响数据库的。
}

