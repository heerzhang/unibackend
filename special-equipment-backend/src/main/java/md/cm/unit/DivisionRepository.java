package md.cm.unit;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.UUID;

public interface DivisionRepository extends JpaRepository<Division, UUID>, JpaSpecificationExecutor<Division>, QuerydslPredicateExecutor<Division> {

    //Division findTopByOldId(Long id);
    //Division findByNameAndUnit(String name, Unit unit);  @常用的过滤字段要放在前面！
    Division findByUnitAndName(Unit unit, String name);
}
