package md.cm.unit;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import md.cm.geography.Adminunit;
import md.system.User;
import org.fjsei.yewu.filter.Uunode;
import org.fjsei.yewu.util.Tool;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import jakarta.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;

/** 科室。 Unit目前只由检验机构才需要3级配置的。
 * 特检院组织架构 , Office必定属于某一个特检院 底下的某一个Division部门{=department};
 * 目前旧平台同步模块，还未涉及到科室数据的生成和导入。
*/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL, region ="Slow")
public class Office implements Uunode {
    /*@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "commonSeq")
    //@SequenceGenerator(name = "commonSeq", initialValue = 1, allocationSize = 1, sequenceName = "SEQUENCE_COMMON")
    protected Long id;*/

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    /**科室 名字
    */
    private String name;


    /**本科室 到底归属于该特检院的哪一个大业务部门(分院)的？　
     * 管理意义上的细化：　大业务部门Division  1-->N 科室。
    */
    @NotNull
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn
    private Division  dep;

    /**用户； 下级的关联：
    */
    @OneToMany(mappedBy = "office")
    private Set<User>  staff;

    /**行政区划, 监察机构 ？管理需要：
     * 需要判定哪一个行政区划级别的，按照行政区划隶属关系。...新店所，相互隔离：晋安区市场监督管理局福新市场监督管理所；
     *若设立一个Unit='福州市市场监督管理局', 那么： 人员User->Unit{+部门Division【晋安区市场监督管理局】 + 科室【新店所】Office}。
     * */
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn
    private Adminunit lare;

    //目前，假设UntSecudept 和UntDept 两个表的ID不重复！
   // private Long oldId;  //对接旧系统 内设管理部门 .SAFE_DEPT_ID  关联TB_UNT_DEPT ,内设分支机构 .SECUDEPT_ID 关联TB_UNT_SECUDEPT
//    public String id() {
//        return Tool.toGlobalId(this.getClass().getSimpleName(), String.valueOf(this.id));
//    }

}

