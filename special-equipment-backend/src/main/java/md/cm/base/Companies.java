package md.cm.base;

import org.fjsei.yewu.jpa.ProjectionRepository;
import org.fjsei.yewu.jpa.QuerydslNcExecutor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.graphql.data.GraphQlRepository;

import java.util.UUID;

@GraphQlRepository
public interface Companies extends ProjectionRepository<Company, UUID>, QuerydslNcExecutor<Company>, JpaRepository<Company, UUID> {
    Company findByName(String name);    //找到多条就程序抛出异常！
    Company findByNo(String no);
    Company findByNameAndNo(String name,String no);  //确保最多只有一条
    Company findTopByName(String name);
}
