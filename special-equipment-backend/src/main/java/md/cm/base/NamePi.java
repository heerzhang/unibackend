package md.cm.base;

import java.util.UUID;

/**为提高性能 投影用的
 * Interface-based Projections方案
 * 非得手动再写函数名： lombok.Value; @Value is only supported on a class.
* */
public interface NamePi {
    String getName();
}
