package md.cm.base;

import org.fjsei.yewu.jpa.ProjectionRepository;
import org.fjsei.yewu.jpa.QuerydslNcExecutor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.graphql.data.GraphQlRepository;

import jakarta.persistence.QueryHint;
import java.util.List;
import java.util.UUID;

@GraphQlRepository
public interface Persons extends ProjectionRepository<Person, UUID>, QuerydslNcExecutor<Person>, JpaRepository<Person, UUID> {

    Person findByNo(String no);
    Person findByNameAndNo(String name,String no);  //确保最多只有一条
    Person findTopByNo(String no);
    List<Person>  findAllByNameLike(String namepair);
    //不加这个，缺省的没有启动查询缓存的。
    @QueryHints(value ={ @QueryHint(name = org.hibernate.jpa.QueryHints.HINT_CACHEABLE, value ="true") } )
    Person findByName(String name);  //非唯一性的 报错
}

