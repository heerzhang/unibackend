package md.cm.flow;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface ApprovalStmRepository extends JpaRepository<ApprovalStm, Long>, JpaSpecificationExecutor<ApprovalStm> {
}
