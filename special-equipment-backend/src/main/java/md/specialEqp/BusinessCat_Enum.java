package md.specialEqp;
//先有旧的数据，才再去搞的enum定义，数据库DB存储用Byte 1个字节/数字

/**业务大类 OPE_TYPE; 共性形式的业务分类抽象，
 * 为了方便兼容旧平台， 特意让byte取值和原来一致。
 * 定检业务受理需排除2,14,15三个业务。
 */

public enum BusinessCat_Enum {
    _0,
    /**制造监检=1*/
    MANUFACT,
    /**安装监检=2*/
    INSTA,
    /**定期（内部、全面）检验=3
     * 对应 isp2系列的；
     *  */
    REGUL,
    /**进口安全性能检验=4*/
    SAFETYINS,
    /**鉴定检验*/
    IDENTIFIC,
    _6,
    /**型式试验*/
    TYPETST,
    /**年度（外部、在线）检验=8 */
    ANNUAL,
    /**耐压试验*/
    PRESSURE,
    /**验收=10*/
    DELIVERY,
    /**检测=11*/
    TEST,
    /**试验*/
    EXPERIMENT,
    /**其它=13*/
    OTHER,
    /**改造监检=14*/
    REFORM,
    /**重大修理=15*/
    REPAIR,
    /**首检=16*/
    FIRST,
    /**产品质量检验*/
    PRODUCT,
    /**评估*/
    ESTIMATE,
    /**热效率测试==19*/
    THERMAL;

    /**监察的业务类型id映射到本平台 需要特别处理
     * 旧监察平台的业务类型：若是非法定=委托的呢？考虑;
     * */
    public static BusinessCat_Enum  ofFjJC(int id) {
        int idx=0;
        //if(NULL==id)  return  null; 监察直接用对象的ID;替代类型Enum index;
        if(2==id)  idx=2;
        else if(3==id)  idx=14;
        else if(id>=4 && id<=8)  idx=2;
        else if(9==id)  idx=4;
        else if(id>=11 && id<=15)  idx=2;
        else if( (id>=22 && id<=25) || 20==id)  idx=10;
        else if(21==id || 26==id)  idx=16;
        else if(40==id || 41==id || 43==id)  idx=3;
        else if(42==id || 44==id)  idx=8;
        else if(80==id)  idx=11;
        else if(1==id)  idx=1;
        else if(200==id)  idx=13;
        else if(202==id)  idx=19;
        if(0==idx)  return null;
        return  BusinessCat_Enum.values()[idx];
    }
}



/*
TB_DICT_OPETYPE似乎没内涵36个压缩到实际28个 /,委托法定=14个{[旧检验平台]委托要+600 数值,  60x}。
	3定期（内部、全面）检验？/？细的检
	13其它  in协议
	2安装监检 in协议       ?实际数据还会出现602委托的?
	8年度（外部、在线）检验？/？粗的检
	11检测
	1制造监检 in协议   , 601=制造监检-在委托检验下的；
	16首检  in协议
	19热效率测试
	10验收
	12试验
	14改造监检 in协议
	15重大修理 in协议
	4进口安全性能检验-only法定
	18评估-only委托
	5鉴定检验-only委托
	17产品质量检验-only委托
	7型式试验-noUsed
	9耐压试验-noUsed
一共18种OPE_TYPE；最大整数是19；
LAST_ISPOPE_TYPE 数字，  监察通用字典表：JC_DICT_COMMON@JCDB
当前真实有数据的：监察对检验的OPE_TYPE映射关系;
0: {DICT_NAME: "制造监检", DICT_ID: "1", DICT_TYPE: "101"} =1
1: {DICT_NAME: "安装监检", DICT_ID: "2", DICT_TYPE: "101"} =2
2: {DICT_NAME: "改造与重大维修监检", DICT_ID: "3", DICT_TYPE: "101"}  =14
3: {DICT_NAME: "组装锅炉安装监检", DICT_ID: "4", DICT_TYPE: "101"}  =2
4: {DICT_NAME: "散装锅炉安装监检", DICT_ID: "5", DICT_TYPE: "101"}  =2
5: {DICT_NAME: "整装锅炉安装监检", DICT_ID: "6", DICT_TYPE: "101"}  =2
6: {DICT_NAME: "整装压力容器安装监检", DICT_ID: "7", DICT_TYPE: "101"}  =2
7: {DICT_NAME: "压力容器现场组焊监检", DICT_ID: "8", DICT_TYPE: "101"}  =2
8: {DICT_NAME: "进口安全性能监检", DICT_ID: "9", DICT_TYPE: "101"}   =4
9: {DICT_NAME: "球罐安装监检", DICT_ID: "11", DICT_TYPE: "101"}    =2
10: {DICT_NAME: "金属工业管道安装监检", DICT_ID: "12", DICT_TYPE: "101"}  =2
11: {DICT_NAME: "医用氧舱安装监督", DICT_ID: "14", DICT_TYPE: "101"}  =2
12: {DICT_NAME: "燃气管道安装监检", DICT_ID: "15", DICT_TYPE: "101"}  =2
13: {DICT_NAME: "新装验收", DICT_ID: "20", DICT_TYPE: "101"}   =10
14: {DICT_NAME: "首检(在用)", DICT_ID: "21", DICT_TYPE: "101"}  =16
15: {DICT_NAME: "重新验收", DICT_ID: "22", DICT_TYPE: "101"}   =10
16: {DICT_NAME: "改造验收", DICT_ID: "23", DICT_TYPE: "101"}  =10
17: {DICT_NAME: "大修验收", DICT_ID: "24", DICT_TYPE: "101"} =10
18: {DICT_NAME: "移装验收", DICT_ID: "25", DICT_TYPE: "101"}  =10
19: {DICT_NAME: "首检", DICT_ID: "26", DICT_TYPE: "101"}   =16
20: {DICT_NAME: "定期检验", DICT_ID: "40", DICT_TYPE: "101"}  =3
21: {DICT_NAME: "内部检验", DICT_ID: "41", DICT_TYPE: "101"}  =3
22: {DICT_NAME: "外部检验", DICT_ID: "42", DICT_TYPE: "101"}  =8
23: {DICT_NAME: "全面检验", DICT_ID: "43", DICT_TYPE: "101"}  =3
24: {DICT_NAME: "年度（在线,定期）检验", DICT_ID: "44", DICT_TYPE: "101"}    =8
25: {DICT_NAME: "压力容器年度委托检查报告", DICT_ID: "80", DICT_TYPE: "101"}  =11
26: {DICT_NAME: "厂车委托检验", DICT_ID: "200", DICT_TYPE: "101"}    =13
27: {DICT_NAME: "热效率测试", DICT_ID: "202", DICT_TYPE: "101"}    =19

[例外数据？] 检验平台出现 LAST_ISPOPE_TYPE2==40? 24？ 纠正？
 */
