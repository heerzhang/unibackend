package md.specialEqp.inspect;

import org.fjsei.yewu.filter.Node;
import org.fjsei.yewu.pojo.SliceSyncRes;

import java.util.UUID;

/**为提高性能 投影用的
 * Interface-based Projections方案
 * 同步需要的extends SliceSyncRes,
 * 添加extends SliceSyncRes ｛void setFail(String fail);｝ 或加void setTisp(Isp isp); 运行setXxx()不会报错！ 但不会保存修改数据。 #这丫都不报错。
* */
public interface DetailPi extends Node<UUID>, SliceSyncRes {
    UUID getId();
    String getFail();
    Isp  getIsp();
    Isp  getTisp();
//    void setTisp(Isp isp);
}

