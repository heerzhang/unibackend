package md.specialEqp.inspect;

import com.querydsl.core.types.Predicate;
import org.fjsei.yewu.jpa.NormalExecutor;
import org.fjsei.yewu.jpa.QuerydslNcExecutor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.lang.Nullable;

import java.util.UUID;

/* 旧的是
interface DetailRepository extends JpaRepository<Detail, Long>, JpaSpecificationExecutor<Detail>, QuerydslNcExecutor<Detail>
* */

public interface DetailRepository extends NormalExecutor<Detail, UUID> {
    //IDEA: 在这个里面才会正常的 提示可用的 函数名字。
    //Slice<Detail> findAllOrderById(Pageable pageable, Class<?> type);

}

