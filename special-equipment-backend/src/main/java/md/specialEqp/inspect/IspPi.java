package md.specialEqp.inspect;

import md.specialEqp.BusinessCat_Enum;

import java.time.LocalDate;
import java.util.Date;
import java.util.UUID;

/**为提高性能 投影用的
 * Interface-based Projections方案
* */
public interface IspPi {
    UUID getId();
    String getNo();
}

/* 原来的可行的方式:
public interface IspPi {
    UUID getId();
    BusinessCat_Enum getBsType();
    Boolean getEntrust();
    String getNo();
    Date getNextIspDate();
    LocalDate getIspDate();
    String getConclusion();

    UnitPi getIspu();    //嵌套关联的投影也行：

    interface UnitPi {
        UUID getId();
        String getIndCod();
        String getArea();
    }
}
* */