package md.specialEqp;

import lombok.*;
import md.cm.unit.Unit;
import org.fjsei.yewu.filter.Uunode;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import jakarta.persistence.*;
import java.util.UUID;


/** 合法的检验机构列表。
 * 特殊 个性化定做的
*/


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder=true)
@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL, region = "Fast")
public class IspAgency implements Uunode {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    /**对应单位:  必须是企业的： Company company;
     * */
    @OneToOne( fetch=FetchType.LAZY)
    @JoinColumn( referencedColumnName = "ID")
    private Unit  unit;

    /**检验机构的 资格 核准号码。
     */
    private String  apno;

     /**是否业务上可以接入使用本平台？
     * */
    private Boolean  enable;

}


/* @数据库修改脚本：

* */