package md.specialEqp;

import md.specialEqp.inspect.Detail;
import md.specialEqp.inspect.Isp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

//JPA的核心，数据库装入,到对应的实体类去。
/**看来json字段的嵌套内部属性过滤操作也是可行的，就是性能慢了。
 * json字段无法使用QueryDSL技术，java id关联也不能直接访问;
* */
public interface ReportRepository extends JpaRepository<Report, UUID>, JpaSpecificationExecutor<Report>, QuerydslPredicateExecutor<Report> {

    List<Report> findByIsp(Isp isp);

    //List<Report> findAllByDataMatchesRegex( 正则 ); ?正则表达式string如何？非逻辑严谨的=不精确

    /*  无中生有的实体类!
    @Query("select new space.xxhui.ec.interaction.POJO.RelationTopic( c.topicId , 2 ,c.createTime) from CollectionEntity c where c.userId in (select r.focusId from RelationEntity r where r.fanId = ?1 ) ")
    List<RelationTopic> getRelationTopic(Long fanId);
    * */

    /*  @Query支持原生sql查询
         @Query(value="update Cat t set t.name=?1 where t.age=?2 and t.id=?3")
    //针对特殊需求预留：数据库直接过滤JSON的对象字段。 select * from report where data->'$.字段2' like '%数值%' 就避免压力上升到后端服务器做过滤了。
    @Query(value="select count(1) from Cat t",nativeQuery=true)
    public int getAllCatCount();
        @Query("from Cat t where t.addressId=:id and t.age=:age")
      public List<Cat> getCatByaddressIdAndByAge1(@Param("age")int age,@Param("id")Long id);
      或者用 json_extract(t.data, "$.字段2")
      根据json数组查询，用 JSON_CONTAINS(字段, JSON_OBJECT('json属性', "内容"))
        @Query(value="select id from report t where t.data->'$.字段2' like '%:tag%'" ,nativeQuery=true)
    List<Long>  findAllByDataMatchJson(@Param("tag")String tag);
        @Query(value="select id from report t where t.data->'$.字段2' like '%?1%'" ,nativeQuery=true)
    List<Long>  findAllByDataMatchJson(String tag);
    @Query(value="select id from report t where t.data->'$.字段2' ='2223' " ,nativeQuery=true)
    成功的有@Query(value="select id from report t where t.data->'$.字段2' =:tag " ,nativeQuery=true)
    就是Json里面搞 like ''行不通了！ 【结论】和直接在IDEA console输入SQL语法不一致的。where t.data->'$.字段2' like %:tag% " ,
     */
    //@Query(value="select count(1) from Cat t",nativeQuery=true) ##必须上nativeQuery 直接送给jdbc:mysql的语句！
    //固定JSON里面某个属性字段匹配
    @Query(value="select * from report t where t.data->'$.字段2' like %:tag% " ,nativeQuery=true)
    List<Report>  findAllByDataMatchJson(@Param("tag")String tag);

    /*nativeQuery=true还是会做转换sql语句的：  ' $.'字段2' like '%数值%' '
    * */
    //报错的 value="select * from report t where t.data->'$.:key' like %:tag% " ,这个:key必须包括前面$.前缀符号的;
    //灵活针对JSON里面某个可变名字属性字段Key的匹配 ;调用办法:findAllByDataJsonKeyIs("$." + "字段2", "数值");
    @Query(value="select * from report t where t.data->:key like %:tag% " ,nativeQuery=true)
    List<Report>  findAllByDataJsonKeyIs(@Param("key")String key, @Param("tag")String tag);

}

