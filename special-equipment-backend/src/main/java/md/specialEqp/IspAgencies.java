package md.specialEqp;

import org.fjsei.yewu.jpa.ProjectionRepository;
import org.fjsei.yewu.jpa.QuerydslNcExecutor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.UUID;


public interface IspAgencies extends ProjectionRepository<IspAgency, UUID>, QuerydslNcExecutor<IspAgency>,
        JpaRepository<IspAgency, UUID>, JpaSpecificationExecutor<IspAgency> {

}

