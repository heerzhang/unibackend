package md.specialEqp;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.UUID;

public interface Requests extends JpaRepository<Request, UUID>, JpaSpecificationExecutor<Request>, QuerydslPredicateExecutor<Request> {

}
