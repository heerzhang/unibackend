package md.specialEqp.type;

import lombok.*;
import lombok.experimental.SuperBuilder;
import md.specialEqp.Eqp;
import org.fjsei.yewu.filter.Uunode;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;

import jakarta.persistence.Entity;

//不增加实体类的继承子类就能解决简单问题，直接做EQP.otherParamJSON字段。
/**大型游乐设施: 6000大型游乐设施 TB_AMUS_PARA
 * A级别的是国家级一家检验的有资格检验？？。对于普通特检院机构： B/C级别才有业务，游乐设施A级的在监察平台都没技术参数和报告。
 *9000客运索道,F000安全附件,7000压力管道元件:这三个type特种设备没有做独立参数表，减少派生实体类。客运索道9000是全国唯一一家专门检验的。
 客运索道9000 只能当成普通Eqp，目前普通机构无法检验，监察是有索道设备数据,监察没有'两工地'特种设备,监察针对气瓶设备也是独立存储的孤立管理系统,特检院不做气瓶业务。
 【例外】碰碰车的：很可能了多辆的车辆/游船总数?==登记为一个Eqp?
 */

@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder=true)
@Getter
@Setter
@Entity
@Indexed(enabled=true, index="ea6")
public class Amusement extends Eqp implements Uunode {
    //EQP_LEVEL比起AMUS_TYPE更为准确！，
    //游乐设施级别AMUS_TYPE 实际等同 EQP_LEVEL: "B级"，字段要合并；
    //父类Eqp.level; 字段一个概念的？？ABC==>123仅仅代码描述差异？前端给的名称不同罢了。
    //private String  matr;   //游乐AMUS_TYPE游乐设施等级类型，游乐设施类型(检验)@[{id:'A',text:'A'},{id:'B',text:'B'},{id:'C',text:'C'}]

    /**IF_SHIFT是否移动 大型游乐*/
    private Boolean  mbig;

    /**长度"LENGTH"*/
    private Float  leng;
    /**游乐设施高度"HEIGHT" 设备伸展触手高*/
    private Float  high;
    /**运行高度"MOV_HIGH"
     * 运行高度：有2个字段RUNHEIGH  MOV_HIGH， 取后一个MOV_HIGH；
     * */
    private Float  hlf;
    /**额定乘客人数(人)"RATEDPASSENGERNUM"*/
    private Short  pnum;
    /**额定速度"RATEDVELOCITY"    */
    private Float  vl;
    /**回转直径"TURNINGDIAMETER"*/
    private Float  sdia;
    /**倾夹角或坡度"GRADE"*/
    private Float  grad;
    /**摆角"SWINGANGLE"*/
    private Float  angl;

    //游乐设施额定载荷不一定是数值，可以是区间
}


//不可改技术参数：长度"LENGTH"运行高度"MOV_HIGH"额定乘客人数(人)"RATEDPASSENGERNUM"摆角"SWINGANGLE"额定速度"RATEDVELOCITY"回转直径"TURNINGDIAMETER"游乐设施高度"HEIGHT"倾夹角或坡度"GRADE"

