package md.specialEqp.type;

import lombok.*;
import lombok.experimental.SuperBuilder;
import md.specialEqp.Eqp;
import org.fjsei.yewu.filter.Uunode;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import java.util.LinkedList;
import java.util.List;

/**管道装置;8000压力管道/ TB_PIPELINE_PARA  JC_TEMP_PIPELINE_PARA
 * 管道总体的相关参数，不是具体单元的，是合计数据或者都统一的参数。
 * 工程（装置）名称：装置名=设备名『就是设备品种的俗称』;
 * 指定装置后点击合并，将会将该单位底下的所有同类别、同区域（公用管道按照区县，长输管道按照市）下装置的管道单元合并到该装置，其余装置处理成垃圾数据；
 * 安全管理部门：设备所在地址与使用单位地址不在同一处的，将设备所在地的管理机构视为分支机构，需写明具体地址和所在县区、乡镇（街道）。
 * 同一管理部门管理的多个管道装置可共用一张使用登记表{使用登记证编号，识别码：都同一个}。
 * 管道装置关键字：name(应该移入 ? fno)字段+; 【唯一性关键字组合/虚拟】
 * 管道对应的制造单位=使用单位，  出厂编号=装置名称{不一定相同的}；? ?避免重复数据?
 * 同一个使用单位下的（工程装置名称、管道编号）不允许重复。  + 单位内部编号! + 出厂编号 + EQP_NAME 设备名称 + 地理定位pos也不同;
 * 管道设备 没有移装的说法！ 管道单元都是一次性的。
 * '中国特种设备检测研究院'负责检验的管道DE10339,在监察平台有些没有技术参数表,在检验平台更是没有数据。中特院负责的管道参数表都是空(单元有参数表)很多只有一个单元而且报告针对单个单元出。
 * 工业管道检验周期最短，最频繁：https://wenku.baidu.com/view/786d6ae426d3240c844769eae009581b6bd9bdcb.html
*/

@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder=true)
@Getter
@Setter
@Entity
@Indexed(enabled=true, index="ep8")
public class Pipeline extends Eqp implements Uunode {
    /*装置名称： BOX_NAME ； 设备(装置)名称：可搜索字段。 #有一大半管道设备没有设置装置名称!?单位工厂小就一个装置?
     * 压力管道{底下链接很多单元的}本身名称：装置名称只能唯一一个的。
     * 装置名称：并不一定就等于出厂编号的！；  [需要额外加个]装置名称字段。
    * 市场化的检验竞争，以整个管道装置设备oid/使用证、工程（装置）名称，统一划归某个检验机构。
    * 责任分解： 看单线图{设计单位}和工程图{安装单位}；
    *管道特别！ 旧平台直接用父类 EQP_NAME 字段,设备名称。初始化数据要倒腾转移。
     * 旧平台混乱，为数据一致性还是加上独立字段，搜索尽量不要用这个字段，本字段是揭示详细信息/兼容旧平台。
     * [为方便搜索功能！] Pipeline.insn和父类Eqp.titl的针对旧平台导入时刻数据对调，字段本义也对调。
     * 或者直接覆盖Eqp.titl数据，把旧平台Eqp.titl设备名称字段[管道搞什么名字]隐藏转给非搜索字段svp[管道设备名];
    */
    //private String  insn;   //可直接使用父类Eqp.titl;设备名称; @监察平台没有"BOX_NAME"装置名称"字段！旧监察检验主表有"PIPE_BOX_NAME"字段

    /**管道底下的具体的许多个单元组成集合： TB_PIPELINE_UNIT_PARA  JC_TEMP_PIPELINE_UNIT_PARA
    *单元也可以合并。
     *同一个使用单位下的同一工程装置名称=装置名称 BOX_NAME（Eqp.titl） 、PipingUnit的管道编号的组合不允许重复。
     *一个Pipeline底下所有单元都只能是统一的一个 使用单位名称 :基类Eqp.useu;
     */
    @Builder.Default
    @OneToMany(mappedBy="pipe")
    @org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL,region ="Slow")
    private List<PipingUnit>  cells= new LinkedList<>();
    //private Set<PipingUnit> cells;

    /**管道材质
     * 全部的，所有管道单元的都是PIPELINE_MEDIUM管道材质，@[{id:'钢制',text:'钢制'},{id:'PE管',text:'PE管'}]
     * 实际是标号， PE管； 20 ；钢制；碳钢；无缝钢管（牌号不清）; 详见管道特性表
    */
    private String  matr;

    //合成属性123级别+ABCD类别；PIPELINE_LEVEL = "";// 管道级别@[{id:'GA1',text:'GA1'},{id:'GA2',text:'GA2'},{id:'GB1',text:'GB1'},{id:'GB2',text:'GB2'},{id:'GC1',text:'GC1'},{id:'GC2',text:'GC2'},{id:'GC3',text:'GC3'},{id:'GD1',text:'GD1'},{id:'GD2',text:'GD2'}]
    //管道级别PIPELINE_LEVEL，管道的各个所属单元都可有自己单独设置的级别。
    //父类Eqp.level 代替

    /**管道介质 "WORK_MEDIUM" 见特性表
     * 乙炔/粗BYD/BYD+水
     */
    private String  mdi;

    /**设计压力 "DESIGN_PRESS" 见特性表 详见管道特性表,
     * 高压部分/低压部分：2.0/1.5    高压侧：2.0；低压侧1.5
     * {"高":, "低":, "某部分管":, }
     */
    private String  prs;

    /**设计温度 "DESIGN_TEMP"  常温; 见管道单线图
     * 蒸汽：190、空气：60 ; 80(PL8005～S8068段）/60(S8068～S8069段);  -25/-50(PG0410)
     */
    private String  temp;

    //管道长度：	LENGTH  实际该是统计值,后端可自己统计， 统计平台也是快照模式数据统计。

    /*IF_FS_EQP 【旧平台】检验平台才有/监察不管:
    IF_FS_EQP是否附属+MAIN_EQP_COD归属主设备 =非空的有328台; 都是:锅炉附属的工业管道；=?淘汰字段？
    "IF_FS_EQP", COMMENTS: "是否附属设备","MAIN_EQP_COD","主设备号，对应附属管道/附属起重",
    * */

    /*替代 *.graphqls 模型文件上的@dbpage注解：Spring Graphql直接可以调用这里的。应该要移入到Controller才能访问数据库Repository。
     * 优先级不够：没有进入这里运行？
     * 被这个 @SchemaMapping(typeName="Pipeline", field="cell_list" )截胡了。
     *  */
/*    public Connection<PipingUnit> getCell_list(DataFetchingEnvironment env) {
        return new MemoryListConnection(Arrays.asList(cells.toArray())).get(env);
    }*/

    //监察整的汇总表编号？数据整理专项。卷宗编号。整治排查 ==>OID 使用证号
    //分页模式需要直接访问数据库 XxxRepository 只好转移到 graphql的Query{}定义的接口调用模式
/*    public Connection<PipingUnit> cell_list(int first, String after, int last, String before, DataFetchingEnvironment env) {
        //[不会执行到这里]！ 初始化graphQL运行不报错需要的。
        return new MemoryListConnection(Arrays.asList(cells.toArray())).get(env);
    }*/

}




//不可改技术参数： 设计压力"DESIGN_PRESS" 设计温度"DESIGN_TEMP" 管道介质"WORK_MEDIUM"

//7000压力管道元件=制造库才用的　=制造流水表的type。
//按装置分别登记管道信息，按单位统一发证；由分支机构管理的，需填写分支机构地址和分支机构所在区域。
