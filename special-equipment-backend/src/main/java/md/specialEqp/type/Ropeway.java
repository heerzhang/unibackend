package md.specialEqp.type;

import lombok.*;
import lombok.experimental.SuperBuilder;
import md.specialEqp.Eqp;
import org.fjsei.yewu.filter.Uunode;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;

import jakarta.persistence.Entity;

/**9000 客运索道 【不属于普通特检院的管辖范围】 ；
 * 监察平台才有几条数据的； 但检验平台没有数据。
 * TB_CABLEWAY_PARA没， JC_CABLEWAY_PARA JC_TEMP_CABLEWAY_PARA 技术参数;
 * 监察才有，检验没权搞这个品种业务。福建一共才13台设备。
 * 只有国际级别的检验机构才做检验，中国就一家啊能搞检验。 Ropeway根本对检验平台没啥意义，对监察来说才有。
 */

@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder=true)
@Getter
@Setter
@Entity
@Indexed(enabled=true, index="er9")
public class Ropeway extends Eqp implements Uunode {
    /**"长度(m) ROPEWAYLENGTH"*/
    private Float  leng;

    /**"运行速度m/s RUNVELOCITY"*/
    private Float  vl;

    /**"输送能力(运量), 人/小时, TRANSPORTATIONABILITY"
     * */
    private Integer rtl;

    /**"支架数目 BRANUM"*/
    private Short  flo;

    //对接外部系统，EQUDATASYSID: "-6735c775:12c6d2ac6be:-2b9a"
}

//不可改技术参数：