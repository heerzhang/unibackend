package md.specialEqp.type;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.UUID;


public interface PipingUnitRepository extends JpaRepository<PipingUnit, UUID>, JpaSpecificationExecutor<PipingUnit>, QuerydslPredicateExecutor<PipingUnit> {

    //直接依据函数名称定义模式： 新加 new add, 更新update set 无法支持的；可指出删除 delete read find exists findOne get all count统计计数,save()+saveAll()。
    // List<PipingUnit>

     //注解JPQL的模式，@Query()；可复杂一点的：
    /*
     * @Modifying：可以使用该注解来实现通过JPQL修改和删除（JPQL不支持添加）
     * 注意：更新和删除操作需要事务支持
     * 在@Query中编写JPQL语句，但是必须添加@Modifying进行修饰来告诉JPA这是修改的操作
     @Modifying
     @Query(value="update Cat t set t.name=?1 where t.age=?2 and t.id=?3")
     @Transactional
     public void updateName(String name,int age,int id);
     */


    /*  @Query支持原生sql查询
    //针对特殊需求预留：数据库直接过滤JSON的对象字段。 select * from report where data->'$.字段2' like '%数值%' 就避免压力上升到后端服务器做过滤了。
    @Query(value="select count(1) from Cat t",nativeQuery=true)
    public int getAllCatCount();
     */


    /*
     * spring data允许在占位符上添加%（适用于模糊查询like：%占位符%）
    @Query("from Cat t where t.name like %?1%")
    public Cat getCatByNa(String name);
     */

     /* 使用占位符：参数顺序必须和JPQL中的顺序一致
          * addressId要与Cat实体类的属性名保持一致，而非表中字段名
          * 参数类型要与Cat实体类中对应的属性类型保持一致
      @Query("from Cat t where t.addressId=?1 and t.age=?2")
      public List<Cat> getCatByaddressIdAndByAge(Long id,int age);
      * @param age
      * @param id
      * @return
      * 使用命名参数：这种写法可以随便设置参数顺序
      @Query("from Cat t where t.addressId=:id and t.age=:age")
      public List<Cat> getCatByaddressIdAndByAge1(@Param("age")int age,@Param("id")Long id);
      */
}

