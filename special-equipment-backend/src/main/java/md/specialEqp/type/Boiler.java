package md.specialEqp.type;

import lombok.*;
import lombok.experimental.SuperBuilder;
import md.specialEqp.Eqp;
import md.specialEqp.Equipment;
import org.fjsei.yewu.filter.Uunode;
import org.fjsei.yewu.pojo.SliceSyncRes;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;

import jakarta.persistence.Entity;

/**1000锅炉  TB_BOIL_PARA  锅炉技术参数;
 * 锅炉/电梯，在监察首检录入中不允许新增？ 施工告急中添加的。要监检
 * 告知单编号自己填，电梯设备代码可null;锅炉告知有设备代码。
 * 监察只看识别码oid。
 */

@AllArgsConstructor
@SuperBuilder(toBuilder=true)
@Getter
@Setter
@NoArgsConstructor
@Entity
@Indexed(enabled=true, index="eb1")
public class Boiler extends Eqp implements Uunode{
    /**TB_BOIL_PARA.IF_BOIL_WALL'是否有炉墙锅炉；
     * 【特殊】 监察没有该字段！   ?收费标准中已经排除掉了'工业锅炉炉墙检验'。
     * */
    private Boolean wall;

    /**蒸发量（t/h）;  约等于 ≈ 额定功率
     * TB_BOIL_PARA.RATCON '1000额定功率(MW)/蒸发量；RATCONS , RATCON
     *监察:"额定蒸发量（T/H)", 检验:"额定功率(MW)/蒸发量(T/H)|101",; ?复合二者选择1？额定功率(MW)/蒸发量(T/H)：
       #【针对需求的可能性作特别考虑】并不是一定需要把该字段 安排进入 EqpEs 的索引中的，感觉必要性不是很大！ 解决问题办法也很多的。统计分析要独立出去。
       #特别的需求？ 根据蒸发量功率 来决定当地 分配给哪一个检验部门干活的！ findAll(锅炉.地区)->Loop{ (each)->{} }==?不会要求直接过滤搜索该字段的。
     不能为空!
    【稀奇！】热水锅炉按0.7MW折合1t/h蒸汽锅炉收费: 蒸发量（t/h）;对非电站锅炉低压力锅炉可以成立; 来源自http://www.fjtj.com/fyshow?pid=442887&ctlgid=521213&Id=28533
     * */
    private Float power;

    // IF_VERTICAL ？ MAINSTRFORM 合并字段； TB_BOIL_PARA.IF_VERTICAL安装形式（作废）
    /**锅炉结构形式MAINSTRFORM: "锅壳式", 立式很少；
     * IF_VERTICAL锅炉结构形式@[{id:'立式',text:'立式'},{id:'卧式',text:'卧式'} "锅壳式"]
     *  锅炉结构形式=['锅壳','水管','盘管','其他'];
     */
    private String  form;

    /**BURNINGTYPE燃料种类 [{id:'无烟煤',text:'无烟煤'},{id:'烟煤',text:'烟煤'},{id:'褐煤',text:'褐煤'},{id:'煤矸石',text:'煤矸石'},{id:'柴油',text:'柴油'},
     * {id:'重油',text:'重油'},{id:'渣油',text:'渣油'},{id:'天然气',text:'天然气'},{id:'管道液化气',text:'管道液化气'},{id:'城市煤气',text:'城市煤气'},{id:'高炉煤气',text:'高炉煤气'},
     * {id:'电加热',text:'电加热'},{id:'余热',text:'余热'},{id:'再生生物资',text:'再生生物资'},{id:'黑液',text:'黑液'},{id:'垃圾',text:'垃圾'}]
   #特别的需求？ 需要统计聚合的或过滤的，可以选择1=数据库SQL查；2=提取数据TEL另外建立大数据库做分析统计(非实时/阶段性/突发性需求！)。
     燃料种类=['无烟煤','烟煤','褐煤','煤矸石','柴油','重油','渣油','天然气','城市煤气','高炉煤气','电加热','余热','生物质','黑液','垃圾','醇基燃料','其他'];
     %垃圾焚烧发电的电站锅炉 额定功率 相对小点。
     *  电站锅炉收费用；
     */
    private String  fuel;

    /**额定工作压力DESWORKPRESS
     * */
    private Float  pres;
    /**燃烧方式BURNMODE
     * 燃烧方式=['层式燃烧','悬浮燃烧','沸腾燃烧','气化燃烧','余热','其他'];
     * */
    private String  bmod;
    /**装置型式=整组装 组装 的吗？
     * 工业锅炉 + OPE_TYPE":"2，14,15"的场景：才需要区分“整组装、散装”。
     * 散装锅炉一般都是大型锅炉，组装锅炉多是中小型锅炉; 散装锅炉的安装全部在使用地现场，且全部是零散件;整装锅炉多为小型工业锅炉，锅炉出厂时已经组装完毕;
     * 工业锅炉计费用。
     * 默认值是 散装锅炉。 Assembling boiler； =['整组装','散装'];
     * 原来是 svp?.装置型式；
     * */
    private Boolean  asemb;
}


//不可改技术参数：额定工作压力DESWORKPRESS  燃料种类BURNINGTYPE  燃烧方式BURNMODE  锅炉结构形式MAINSTRFORM: "锅壳式"

