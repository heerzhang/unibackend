package md.specialEqp.type;

import lombok.*;
import lombok.experimental.SuperBuilder;
import md.specialEqp.Eqp;
import org.fjsei.yewu.filter.Uunode;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;

import jakarta.persistence.Entity;
/**厂车 5000  场（厂）内专用机动车辆   TB_VEHIC_PARA
 *  TB_VEHIC_PARA  JC_VEHIC_PARA
* */
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder=true)
@Getter
@Setter
@Entity
@Indexed(enabled=true, index="ef5")
public class FactoryVehicle extends Eqp implements Uunode {

    /* 厂车牌照"CATLICENNUM" 汽车罐车也有？  CATLICENNUM重复出现 在厂车技术参数中；
    * */
    //private String  plat;  移入到Eqp. ;

    /**额定载荷"RATEDLOADWEIG"*/
    private Float rtlf;
    /**发动机型号"ENGINEMODEL"*/
    private String  mtm;

    /**动力方式"DYNAMICMODE"*/
    private String  pow;
}


//不可改技术参数：动力方式"DYNAMICMODE" 额定载荷"RATEDLOADWEIG" 发动机型号"ENGINEMODEL" 厂车牌照"CATLICENNUM"