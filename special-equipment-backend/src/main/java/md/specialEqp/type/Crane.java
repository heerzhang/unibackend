package md.specialEqp.type;

import lombok.*;
import lombok.experimental.SuperBuilder;
import md.specialEqp.Eqp;
import org.fjsei.yewu.filter.Uunode;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;

import jakarta.persistence.Entity;

/**旧平台4000起重机械 TB_CRANE_PARA技术参数，sort=4800升降机 TB_LIFT_PARA 独立分开做的参数表。
 * JC_CRANE_PARA &&  EQP_SORT='4800'
 *升降机更像是电梯的技术参数表; 升降机主要还是'两工地'的,但并不归属特检院归口范围的(管理权划归建设行政主管部门),省监察平台也没有这些'两工地'设备。
*/

@AllArgsConstructor
@SuperBuilder(toBuilder=true)
@Getter
@Setter
@NoArgsConstructor
@Entity
@Indexed(enabled=true, index="ec4")
public class Crane extends Eqp implements Uunode {
    //4000起重机械  TB_CRANE_PARA技术参数 ;4800升降机 TB_LIFT_PARA 独立分离的参数
    //其实4800升降机更像Elevator电梯啊，为何和普通的起重机械混成一个设备种类啊{参数字段差异太大了?管理责任为导向的}。
    /**TB_CRANE_PARA.IF_UNNORMAL非标;  QZ_IF_UNNORMAL
     * 是否非标(桥门)
     * */
    private Boolean nnor;
    /**额定载重(升降机4800)"RATED_LOAD"; 升降机也没CHAENGLOAMAIN这个参数字段; 升降机收费也不用RATED_LOAD;
     * 起重 CHAENGLOAMAIN {不算4D00,4800}主钩）,起重机械载荷； 但是4D00,4800不用该字段。
     * 起重机械载荷；CHAENGLOA :吨 ；     pk 电梯 是 kg单位，整数型？
     * 起重机械载荷(t)（收费用，原始记录里面不能用这个字段，要用MAXRATEDCARRYMASS额定起重量
     * mysql无法用load字段名!     类型Float精确度只能5到6个位数==不能非常精确。
     */
    private Float rtlf;
    /**额定起重量"MAXRATEDCARRYMASS"     "80+80/10"
     * 升降机 没有该字段
     * */
    private String cap;
    /**额定速度(升降机)"RATED_V"  额定速度 TB_CRANE_PARA.RATEDSPEED ??复合范围字符串,;
     * 升降机 上行/下行额定速度区分开？？vlu vld？ vl是最关心速度,下行快啊。
     *有 速度　这名字字段实在太多了； vl=最主要的核心速度定义。
     * 不参与收费?
     */
    private String  vls;
    /**运行速度(门座,塔式,轻小型)"RUN_V"
     * 范围/复合的 字符串 ; 特定设备附加{范围/数字偏大的} "纵行:19.0,横行:20.0"
     */
    private String  rvl;
    /**起升速度(主钩) "LIFTESPEEDMAIN"
     * */
    private Float  mvl;
    /**大车速度（桥门）机臂运行速度（架桥）"L_CAR_V"
     * 监察数据还不如检验的全，我这优先从监察同步？问题： 有个设备监察正式表没有临时申请表有的就是这样=实际该算垃圾数据。
     * */
    private String  cvl;
    /**小车速度（桥门:旋臂）"S_CAR_V" */
    private String  scv;
    /**横向移动速度 "LANMOVSPE"   '底层7.8和二层4.8"
     * */
    private String  lmv;

    /**回转速度 "ROTATESVELOCITY" "0.75/0.38"
     * */
    private String  rtv;
    /**变幅速度 "ALTERRANGEVELOCITY",有些双数值*/
    private String  luff;

    /**升降机: 站数"STATION_NUM";  #起重机械：没有该字段！！
     * 监察平台"LIFT_STATIONNUM"升降机, COMMENTS: "站数(升降机)", : "NUMBER"@对起重来说监察平台"LIFT_STATIONNUM"是没实际数据的。
    #类似字段: 起重"层站数" 检验起重表 类似字段 "LAYERSTAGE", COMMENTS: "层站数", DATA_TYPE: "VARCHAR2"  "2层2站2门"
     * */
    private Short  ns;

    /**层数"FLOOR_NUM"  || 停车层数"TC_FLOORNUM"
     * 检验起重表"TC_FLOORNUM", COMMENTS: "停车层数", DATA_TYPE: "NUMBER"
     * 检验升降机表 "FLOOR_NUM", COMMENTS: "层数", DATA_TYPE: "VARCHAR2" "室内"？；
     * 监察"TC_FLOORNUM", COMMENTS: "层数(停车)", DATA_TYPE: "NUMBER"
     * 监察"LIFT_FLOORNUM", COMMENTS: "层数(升降机)", DATA_TYPE: "NUMBER"
     *  */
    private Short  flo;
    /**BERNUM停车设备 泊位数4D00， 泊位数量"BERNUM"*/
    private Short  pnum;

    /**计费字段  “起升高度(主钩）” ："ELEHEIGHTMAIN"； 检验平台 搞的独立字段
     * #升降机： 没有该字段！
     * #监察平台:  没有ELEHEIGHTMAIN 字段！#
     *  */
    private Float  hlfm;

    /**升降机:"UP_HIGH"  "提升高度", "VARCHAR2"
     * 提升高度(m)  ELEHEIGHT;
     * 监察平台有的："ELEHEIGHT", "提升高度(m)", DATA_TYPE: "NUMBER"}
     *  */
    private Float  hlf;

    /**幅度(m) "RANGE"    "3~30"*/
    private String  rang;
    /**SPAN跨度（m；
     * 另外有类似字段 :跨度(桥门):"SPAN_LEN"
     * */
    private Float  span;
    /**IF_TWO_CAB双司机室，  "是" */
    private Boolean two;
    /**合并字段  设备类型不同的叫法不同/没有存储上重叠冲突只是前端显示上的差别；
     * IF_TWO_LCAR 有否双小车;   "是";
    $#升降机IF_TWO_HOIS 是否双笼? 独立分离4800  "44条" "是"，升降机不能直接用IF_TWO_LCAR字段，监察平台没有对应数据！
     *升降机 监察没有对应字段 ！"IF_TWO_HOIS","是否双笼
    */
    private Boolean twoc;

    /**IF_GRAB_B否抓斗， "是" ;升降机没有*/
    private Boolean grab;
    /**IF_SUCTORIAL有否起重吸盘， "是"*/
    private Boolean suck;
    /**IF_CONTAIN_H有否集装箱吊具， "是"*/
    private Boolean cotr;
    /** IF_XZS 否行走式, "是" 是否行走式(塔式): */
    private Boolean walk;

    /**CHAADVMOM起重力矩（吨米;起重力矩"CHAADVMOM"
     * #有效录入数据量比例较少 # kN.m  "5.14～14.86"
     * 有些场景 CHAADVMOM 必须用于收费计算，必须能转为有效的数字： 最大起重力矩 大于50吨米,小于10000吨米的。@只能针对性的控制，个性化报错。
     * 共享字段的:制约-监察才能修改？ 必须绕道监察平台改好了，再次回传来检验平台? 扯皮了!。 @?再克隆多搞一个类似字段？或者扔进Detail每一次检验都再填写。
     * */
    private String  mom;
    /**IF_ZJCC 是否整机出厂起重机;  "是" 区区5条？ 该字段存在有何意义？整机上岸的门座式起重机械按收费标准50%收取-"-(tmpCalPrice*50)/100",
     * 监察表没有字段。 升降机没有字段。
     * */
    private Boolean whole;
    /**升降机 ：曳引机型号"DRAG_TYPE"； @起重机械:没有该字段。
     *监察表 TRACANGTYPE 字段；
     * */
    private String  tm;
    /**升降机 ：曳引机编号"DRAG_COD"  @起重机械:没有该字段。
     *监察表  "TRACANGLEAFACNUMBER",
     * */
    private String  tno;
    /**升降机 ：控制屏型号"CON_SCREEN_TYPE"  @起重机械:没有该字段。
     *监察表 "CONSCRTYPE", COMMENTS: "控制屏型号"
     * */
    private String  cpm;
    /**升降机 ：控制屏编号"CON_SCREEN_COD"  @起重机械:没有该字段。
     *监察表 "CONTSCRCODE", COMMENTS: "控制屏出厂编号"
     * */
    private String  cpi;

    /**停车 适停汽车尺寸"TC_CARSIZE"*/
    private String  pcs;
    /**停车 适停汽车质量(kg)"TC_CARWEIGHT"
     * '≤2000（1层）、≤1700（2-5层）'
     * */
    private String  pcw;
    /**停车 单车最大进（出）时间(s)"TC_IO_MAXTIME"
     *   "≤210"
     *  */
    private Float  miot;
    /**操作方式(桥门) "OPER_STYTLE" 地面+司机室
     * || 升降机: 控制方式 CONTROL_TYPE;
     * */
    private String  opm;
    /**变幅形式"ALTERRANGEMODE"*/
    private String  luf;
    /**工作级别"WORKGRADE" 和设备等级Eqp.level的概念不同吗？  升降机: WORK_LEVL
     * 起重机工作级别共8种，分别是A1-A8。其中A1工作级别最低，A8工作级别最高;二种决定，其一是起重机的使用频繁程度，称为起重机利用等级；其二是起重机承受载荷的大小，称为起重机的载荷状态。
     */
    private String   jobl;
    /**停车设备高度(m)"TC_EQPHIGH",
     * " 1-2层：2.05；2-8层：1.55";
     * ??!=设备外形高; 有些数据是单层高度？
     * */
    private String  highs;
    /**起升机构部件(桥门,轻小型,旋臂)"UP_BODY" ,'钢丝绳'*/
    private String  part;

    /**适用场合，EQP_USE_OCCA 使用场合{设计目标}　.EQP_USE_OCCA起重才用来敲定 ISP_CYCLE = 12
     * 其它种类设备该数据没意义。
     * 从父类Eqp移动到此子类; 等待规范 Enum{中文描述的-？转成 英文graphQL接口才能对接前端的Enum} 或者前端限定文本列表输入选定/直接用String。
     * "室内,室外，吊运熔融金属，防爆，绝缘" 其中一个， "吊运熔融金属"才是有用的。
     * 只有4000起重机械才会使用登记证上注明本字段的{混淆！}; 不同于 EQP_USE_PLACE 设备使用场所--字典库；可用于JC_TASK_EQP和报告。
     * */
    private String occa;

    /**IF_METALLURGY 是否冶金(桥门)(检验);  "是"
    * "是".equals( IF_METALLURGY 是否冶金(桥门)(检验))) 如果是冶金起重机，检验周期为一年用来敲定 ISP_CYCLE = 12;
     * 类似 EQP_USE_OCCA=吊运熔融金属
     * 是否吊运熔融(桥门) IF_METALLURGY
     */
    private Boolean metl;

    /**IF_VICE_LOAD 桥（门）式等起重机，有副钩  "是"
     * 原来仅在前端介入的： '有副钩'; Auxiliary hook 升降机没该字段。
     */
    private Boolean auxh;
    /** IF_VICE_ARM  有副臂，加收20%  "是"
     * 因为是收费需要的参数所以从svp移入到DB结构化字段：有副臂: With jib;  升降机没该字段。
     */
    private Boolean wjib;
}



//升降机不可改技术参数：额定载重"RATED_LOAD" 额定速度"RATED_V" 站数"STATION_NUM"曳引机编号"DRAG_COD" 控制屏型号"
//      CON_SCREEN_TYPE"控制屏编号"CON_SCREEN_COD"层数"FLOOR_NUM"曳引机型号"DRAG_TYPE"
//起重机械不可改技术参数：幅度(m) "RANGE" 停车适停汽车尺寸"TC_CARSIZE" 停车层数"TC_FLOORNUM" 大车速度（桥门）机臂运行速度（架桥）"L_CAR_V"
//   变幅形式"ALTERRANGEMODE" 起重力矩"CHAADVMOM" 泊位数量"BERNUM" 工作级别"WORKGRADE"
//   跨度(m)"SPAN" 额定起重量"MAXRATEDCARRYMASS" 停车适停汽车质量(kg)"TC_CARWEIGHT" 停车设备高度(m)"TC_EQPHIGH"
//   小车速度（桥门:旋臂）"S_CAR_V" 停车单车最大进（出）时间(s)"TC_IO_MAXTIME" 起升高度(主钩） "ELEHEIGHTMAIN"
//   起升速度(主钩)"LIFTESPEEDMAIN" 横向移动速度 "LANMOVSPE" 回转速度 "ROTATESVELOCITY" 运行速度(门座,塔式,轻小型)"RUN_V"
//   变幅速度 "ALTERRANGEVELOCITY" 起升机构部件(桥门,轻小型,旋臂)"UP_BODY" 操作方式(桥门) "OPER_STYTLE"

//升降速度 速度:起重表UP_DOWN_SPEED=复合，PROMOTESPEED作废?　
//  RATACTSPE就一条有数据,RATEDSPEED复合范围字符串,  LIFTESPEED复合字符串={起升速度(m/min)：}，
//      LIFTESPEEDVALUE是 LIFTESPEED的主要字段浮点版。
//  RUN_V特定设备附加{范围/数字偏大的}
//主起升机钩1起重量 CHAENGLOAMAIN{不算4D00,4800}主钩） 主起升机钩2起重量CHAENGLOAMAIN2

