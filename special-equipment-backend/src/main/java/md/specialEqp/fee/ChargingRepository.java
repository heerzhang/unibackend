package md.specialEqp.fee;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.UUID;


public interface ChargingRepository extends JpaRepository<Charging, UUID>, JpaSpecificationExecutor<Charging> {

}
