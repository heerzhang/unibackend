package md.log;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface CirculationRepository extends JpaRepository<Circulation, Long>, JpaSpecificationExecutor<Circulation> {

}
