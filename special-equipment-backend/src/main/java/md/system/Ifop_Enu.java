package md.system;

/**通用 汇集模式的 接口方法的操作类型识别码
 */
public enum Ifop_Enu {
    /**新增加一条
     * */
    ADD,

    /**修改一条记录
     * */
    UPD,

    /**删除一条记录
     * */
    DEL;
}
