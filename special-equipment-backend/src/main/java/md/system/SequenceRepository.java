package md.system;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.UUID;


public interface SequenceRepository extends JpaRepository<Sequence, UUID> {
    Sequence getByName(String name);
    /*
    修改查询@Modifying只能使用void或int/Integer作为返回类型！ 没办法，拆开了。
    * */
    @Query(value ="SELECT lno FROM Sequence WHERE name=:name")
    Long nextByName(@Param("name") String name);

    @Modifying
    @Query(value ="UPDATE Sequence SET lno = lno+1 WHERE name=:name")
    void IncByName(@Param("name") String name);

    /**序列 获取当前下值
     修改查询@Modifying只能使用void或int/Integer作为返回类型！ 没办法，拆开了。
     hibernate启动失败：@Query(value ="SELECT nextval('sequence_repno')")
     @Query(value="select id from report t where t.data->'$.字段2' like '%:tag%'" ,nativeQuery=true)
     List<Long>  findAllByDataMatchJson(@Param("tag")String tag);
     在hql中不能“\”反斜杠对单引号进行转义，需要将一个单引号转换成两个单引号。
      * */
    @Query(value="select nextval(:name)" ,nativeQuery=true)
    Long nextSeqNo(@Param("name")String name);
    /**修改序列的起点值,sequence当前值
     * SELECT setval('sequence_repno', 333, false);
     * Modifying queries can only use void() or int/Integer as return type! 不能返回Long;
     *若添加加上 @Modifying 就报错unimplemented: multiple active portals not supported
     * */
    @Query(value="select setval(:name, :start, false)" ,nativeQuery=true)
    Integer setSeqStart(@Param("name")String name, @Param("start")Long start);

    //创建序列

    //删除序列

    //后台管理，查询所有序列
}

