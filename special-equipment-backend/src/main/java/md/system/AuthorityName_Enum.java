package md.system;

//[奇葩了]! 关联靠id;而这里权限的名字和id对应关系很关键，
//数据库AUTHORITY表的实际内容已做id关联的角色名必须登记在这里，否则启动报错!
//目前只能人工修改权限配置的角色名称实体表"AUTHORITY"。　角色ID属于稳定很少改动的系统性质配置；
//要 ROLE_  开头的；
//注意！ id不应该随着AuthorityName的增删改而导致ID变动。  看Table( AUTHORITY )
//Authority需要人工匹配enum AuthorityName的数据。

/**目前只能人工修改权限配置的角色名称实体表"AUTHORITY"。手动增加数据库authority表：@系统维护初始化。
 * graphQL不支持中文的名字；graphql还要配套对照这个文件 graphql/common.graphql 前端对应名字。
* enum AuthorityName {  ROLE_SOMEONE,  ROLE_USER,  ROLE_ADMIN,   ROLE_Manager 不支持中文的}
* 根本不需要数字编号［数据库id不能改动］，只需要字符串EnumType.STRING。
 * 实体表"AUTHORITY"数据库直接输入 字符串 EnumType.STRING。 Id配对=系统保障id:name映射; id跟随数据库备份数据消亡。
 * TODO: 为了灵活性，需要在应用层面加入权限控制机制。而spring security自带的机制只能当作底层的权限过滤，相当于失去作用另外自己定做。
 * CRDB实际自带Enum数据类型支持，也可以采用小强物理数据库Enum String方案。这里方案是服务端Java映射Enum变身数据库Int方式的,优劣？。
 * 业务系统 的可能角色 Role: 一整个后端可见的业务领域范围的， 用户不一定有某些模块领域的访问授权。
 * ROLE_ANONYMOUS 保留的 系统关键字！不可使用。
 * 严格按照名字字符串来和外部系统进行同步适配的。SSO注入是名字：ROLE_JyUser, 就对应"JyUser"。每个后端自己规划角色划分和具体用途，每个后端都不一样。
 */
public enum AuthorityName_Enum {
    ROLE_Master,    //管理员 整个后端的数据维护者
    ROLE_JyUser,    //检验员 普通的。
    //ROLE_Common   普通登录用户: 没必要单独设的！！
    ROLE_Sponsor,      //特种设备检验检测机构单位的员工之 窗口人员职责细分，协议收单的和转移交给上级。【尽量减少】该角色人员数量。
}

