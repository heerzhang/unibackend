package org.fjsei.yewu.exception;

public class UserAccessDenyException extends RuntimeException {
    public UserAccessDenyException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserAccessDenyException(String message) {
        super(message);
    }
}
