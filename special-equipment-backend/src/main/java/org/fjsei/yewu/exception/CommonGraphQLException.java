package org.fjsei.yewu.exception;

import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.language.SourceLocation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class CommonGraphQLException extends RuntimeException implements GraphQLError {

    private Map<String, Object>  extensions = new HashMap<>();

    public CommonGraphQLException(String message, Long invalidId) {
        super(message);
        extensions.put("invalidId", invalidId);
    }
    public CommonGraphQLException(String message, UUID invalidId) {
        super(message);
        extensions.put("invalidId", invalidId);
    }
    public CommonGraphQLException(String message, String invalidId) {
        super(message);
        extensions.put("invalidId", invalidId);
    }
    public CommonGraphQLException(String message) {
        super(message);
    }

    @Override
    public List<SourceLocation> getLocations() {
        return null;
    }

    @Override
    public Map<String, Object> getExtensions() {
        return extensions;
    }

    @Override
    public ErrorType getErrorType() {
        return ErrorType.DataFetchingException;
    }
}
