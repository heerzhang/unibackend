package org.fjsei.yewu.input;

import lombok.Getter;
import lombok.Setter;
import md.specialEqp.inspect.Procedure_Enum;

import java.time.LocalDate;

@Getter
@Setter
public class TaskDetailInput {
    Integer  version;
    //设备相关
    String  type;
    String  sort;
    String  vart;
    String  subv;
    String  cod;
    String  oid;
    String  fno;
    String  plno;
    String  cert;
    String  lpho;
    String  plat;
    LocalDate used;
    //报告相关
    Boolean feeOk;
    String  ident;
    Procedure_Enum stmsta;
}

