package org.fjsei.yewu.input;

import lombok.Getter;
import lombok.Setter;
import md.specialEqp.BusinessCat_Enum;
import md.specialEqp.inspect.TaskState_Enum;

import java.time.LocalDate;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;

//规则：没有在input当中出现的属性字段名，那它就省略掉，就是不知道有没有变化;

/** 专用于前端输入参数，输入类似REST的搞的DTO，graphql接口函数参数不能直接上实体类。
 *.graphqls模型文件DeviceCommonInput没有对应参数 会报错json不会进入接口函数后端就报错Object null。
 */

@Getter
@Setter
public class TaskInput {
    String  liabler;     //责任人ID
    String  office;     //科室ID
    String  dep;       //检验部门ID
    String  servu;    //服务对象单位ID
    LocalDate date1;        //任务区间选择：开始时间
    LocalDate date2;        //任务区间选择：结束时间
    List<TaskState_Enum>  statusx;      //任务单的状态:前端可设置快捷选择组合。
    Boolean  entrust;        //null: 任意 委托法定
    List<BusinessCat_Enum>  bsTypex;         //业务类型过滤[]
}

