package org.fjsei.yewu.input;

import lombok.Getter;
import lombok.Setter;

/**前端 业务信息编辑 输入参数，
 */

@Getter
@Setter
public class DetailInput {
    String ident;
    Float  ccost;
    Float  mreprc;
    Boolean nsite;
    Boolean test;
    Boolean cheap;
    Boolean apprp;
    Boolean online;
    Boolean impor;
    Boolean ntscop;
    String  fcode;
    Float varea;
    Float opprn;
    Float totm;
    Float diame;
    Float rustc;
    Byte turbi;
    Byte hardd;
    Byte phval;
    Byte conduc;
    Byte palka;
    Byte totalk;
    Byte dsolid;
    Byte phosph;
    Byte oilw;
    Byte tiron;
    Byte sulfit;
    Byte rebasi;
    Byte chrion;
    Byte dioxy;
    Byte carbon;
    Byte captem;
    Short num;
    Short isum;
    //graphQL接口层次：直接上类型meter: String代替Double；从数据库=0,前端接口接收到"0.0",JS页面显示value={ Number(meter) }再次转化成0;
    Double  meter;
    //还没进入接口函数：Cannot deserialize value of type `java.lang.Double` from String "5级": not a valid `Double` value
    //String  zmoney; : 在graphQL接口层次直接用string, 上层次java转换成double
    Double  zmoney;
    //LocalDate  regd;
}

