package org.fjsei.yewu.input;

import lombok.Getter;
import lombok.Setter;
import md.specialEqp.UseState_Enum;

import java.time.LocalDate;


/** 专用于前端 管道单元 输入参数，
 */

@Getter
@Setter
public class PipingUnitInput {
    String  code;
    String  name;
    String  proj;
    String  start;
    String  stop;
    UseState_Enum  ust;
    LocalDate  regd;
    Float dia;
    String thik;
    Float leng;
    String level;
    String  lay;
    String  matr;
    String  mdi;
    LocalDate nxtd1;
    LocalDate nxtd2;
    String  svp;
    String  pa;
    LocalDate used;
    LocalDate insd;
    String desu;    //只需要ID就可
    String insu;
    String safe;
// 单元所在区域 UNIT_AREA_COD ? UNIT_AREA_NAME?还未处理：编辑需要选择关联ID；
    String  pos;            //已确认地址选择的Address ID
// [关联Isp选择] 年检报告 定检检验报告 监检报告 ??不该直接输入，服务端报告终结去关联！
    String isp1;
    String isp2;    //关联ID
    String ispsv;
    String  ad;     //行政区划管理单元id
}


//前端胡乱添加这里没有声明的字段后，报错！ graphql.AssertException: Object required to be not null 接口函数都进不去啊。
