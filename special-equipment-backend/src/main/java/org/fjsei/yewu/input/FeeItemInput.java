package org.fjsei.yewu.input;

import lombok.Getter;
import lombok.Setter;

/**专用于添加-修改-删除 收费项，前端提交给后端反的信息，参数，
 * XxxInput实际上就是接口打包传递参数字段的容器，只要字段名字和数据类型正好一致的，有些字段可以省略的，那就可以到处复用同一个Input定义模型。
 * 也可以用于类似的变更操作接口：比如接口confirmTaskFee(id:ID!, inp: FeeItemInput!) : TaskComResp!
 */
@Getter
@Setter
public class FeeItemInput {
    String  code;
    Integer  fm;
    //graphQL：前端采用String,后端映射为Double的：中间件可对数据类型自动转换。
    Double  amount;
    Double  mnum;
    String  memo;
    /**【更不容易混淆】这个id 是和其它字段一致，属于同一个实体的一条记录的多个字段属性。
     * 删除对应的 Charging 实体ID; Global ID
     * 添加时刻，不需要id=null;
     * 更新-删除时刻，要id;
     * 接口函数里面的上一级参数id字段是针对：共同的父辈关联关键ID,增加命令只能依赖接口函数的父辈关联关键ID。
     * 更新删除命令，必须提供FeeItemInput.id, 但是一般地赖接口函数的父辈关联关键ID也必须一起提供。
     * */
    String  id;
}
