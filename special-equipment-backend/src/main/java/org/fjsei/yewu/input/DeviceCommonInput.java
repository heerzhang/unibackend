package org.fjsei.yewu.input;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
//graphQL内省和接口参数输入的场合，实际只用Getter就可以。
//[语法核心限制] 查询和变更Query, Mutation的接口输入的参数类型不可以用外模型object对象类型=type定义的啊！！。
//Input Object：输入对象 定义的数据类型，因为 Object 的字段可能存在循环引用a->b->a，可以嵌套，但是不能死循环，或者字段引用了不能作为查询输入对象的接口和联合类型。
//因为type普通对象直接用于输入参数的话,语义比较模糊,所以graphQL要求另外单独建
//都是String类型的，input弱类型化。 前端往后端这边的输入数据接口直接json/无类型，无需要区分/数值/日期/bool/字符Byte/字符串。
//弱类型String顺带可以鉴别null还是""的区分，能强化语义场景。
//规则：没有在input当中出现的属性字段名，那它就省略掉，就是不知道有没有变化;

//特别注意！变量取名有限制的！若是fNo就无法解析出取值，必须修改为fno才可肯通行正常的；第二个字母大写特别注意，不允许？？。
/** 专用于前端输入参数，输入类似REST的搞的DTO，graphql接口函数参数不能直接上实体类。
 * 没办法直接用Eqp的实体类，报错.SchemaError: Expected type 'Eqp' to be a GraphQLInputType, but it wasn't!
 * 大杂烩也可以，只要是有个名字{同名类型不一样就不行了}，用途不会交叉的(另外办法：每个用途各自定义一个Input类型)。
 *.graphqls模型文件DeviceCommonInput没有对应参数 会报错json不会进入接口函数后端就报错Object null。
 */

@Getter
@Setter
public class DeviceCommonInput {
    Integer  version;
    //这里字段名字必须和外模型相同的。
    String  type;
    String  sort;
    String  vart;
    String  subv;
    String  cod;
    String  address;     //地址描述文本
    String  oid;
    //String   lat;
    String   fno;   //原先预定是fNo变量名字的，无法正常！只好修改成fno来通过底层接口graphQL的解析层。
    String   plno;      //单位内部编号
    //Long    ownerId;    //产权人的单位ID, ownerId:ID,
    String  cert;
    String  rcod;
    String  addr;
    String  model;
    //Long    useUid;     //使用单位ID
    String  id;       //共通ID,实际上输出模型的id不应该出现在输入中，Mutation接口都额外有关键的定位参数;
    //共用的 json类型 汇聚字段。
    String  svp;     //数据库存储可以null或"{}" 但是不可以是""，否则前端报错。json里面有没有以及各字段类型只能前端自己做主了。不受到控制。
    //可信赖的前端用户，用户自己负责任的参数字段，伪造变造可能，和权限适配管理。
    String  pa;
    //事故隐患类别
    Byte cpa;
    Boolean vital;
    String  level;
    String titl;
    String lpho;
    String  plat;
    LocalDate used;
    LocalDate expire;
    Boolean move;
    Float money;
    Byte impt;
    LocalDate mkd;      //新版本日期类型报错；模型文件必须注解Date标量 typeMismatch.target.mkd,java.time.LocalDate
    String makeu;    //只需要ID就可
    String sno;
    String remu;
    String repu;
    String insu;
    String mtu;
    //String mtud;  交给监察的平台去做  //维堡部门分支机构
    String ispu;        //监察认定检验单位 法定检验机构
    String ispud;      //检验归属部门：必须是ispu底下的一个部门。
    //[实际上] graphqls用的ID定义String owner;若前端对象直接扔过来的，接收String但是无法完美解析还原对象的全部字段。
    String owner;    //产权人
    //这个是id类型 GlobalId<UUID>，下面哪个才是名称直接搜。
    String useu;     //Unit ID,前端发回来的都是GlobalID类型的。两步走搜索可用的：首先定位company|person的ID，回关系数据库查Unit,然后依据Unit.ID匹配。
    String useuName;     //为了ES搜索的特例而加！useuName对应(Unit)useu.company.name||useu.person.name，直接ES嵌套对象搜索，避免两次搜索：缺点是不一定精确某个Unit。
    String usud;    //使用单位部门分支机构
    //String  pos;    //已确认地址选择的Address ID
    String   lon;   //地图经度
    String   lat;
    //AdminunitEs特别需求：
    String  ad;     //行政区划管理单元id
    //若是graphql模型文件定义成 ID而不是String，会导致前端允许随意接受发送json对象给后端。前端给我：{id=VmlsbGFnZTozODE4, name=晋安区光明港湾}
    //前端发出{id: "VmlsbGFnZTozODE4", name: "晋安区光明港湾"}；ID接受变成了K/V：不是正常后端应该看见{id:'yyy', name:'xxx'}
    String  vlg;        //ID, 输入啊：实际只需要id就足够了。#graphql模型文件定义ID不能够透传json对象的！
    String  vlgName;     //设备列表直接对楼盘小区名字搜索时刻用的，不是变更保存的场景；该字段对应(Village)vlg.name为了ES搜索的特例而加！
    String  plcls;
    Boolean dense;
    Boolean seimp;
    LocalDate insd;
    //电梯的：
    Short  flo; //层数
    Boolean spec;
    Boolean nnor;
    Boolean oldb;
    Float  vl;
    String vls;   //字符型的 vl 版本
    Float  hlf;
    Float  hlfm;
    Float  lesc;
    Float  wesc;
    String  cpm;
    String  tm;
    String  mtm;
    String  buff;
    Integer rtl;
    String aap;
    String  prot;
    String  doop;
    String  limm;
    String  opm;
    LocalDate lbkd;
    LocalDate nbkd;
    //锅炉:
    Boolean wall;
    Float power;
    String form;
    String fuel;
    Float pres;
    String bmod;
    Boolean asemb;
    //起重机械:     有些字段上面已经存在了。
    Float rtlf;  //重名不同类型啊! 没办法只能改名字最简单的方案。
    String cap;
    String  rvl;
    Float  mvl;
    String  cvl;
    String  scv;
    String  lmv;
    String  rtv;
    String  luff;
    Short  ns;
    Short  pnum;
    String  rang;
    Float  span;
    Boolean two;
    Boolean twoc;
    Boolean grab;
    Boolean suck;
    Boolean cotr;
    Boolean walk;
    String mom;
    Boolean whole;
    String  tno;
    String  cpi;
    String  pcs;
    String  pcw;
    Float  miot;
    String  luf;
    String   jobl;
    Float   high;
    String  highs;  //容器Float修改 String; 干脆例外命名避免 类型冲突
    String  part;
    String occa;
    Boolean metl;
    Boolean auxh;
    Boolean wjib;
    //压力容器:
    String  vol;
    String  prs;
    Float  weig;
    Float fulw;
    String  mdi;
    String  jakm;
    String  insul;
    String  mont;
    //场（厂）内专用机动车辆:
    String  pow;
    //大型游乐设施:
    Boolean  mbig;
    Float  leng;
    Float  sdia;
    Float  grad;
    Float  angl;
    //压力管道:
    String  matr;
    String  temp;
}




//基本类型Built-in GraphQL::ScalarType有5种 String Int  ID{任意对象}  Boolean Float {精度小数点后5位}。
/*为什么死活不行？ 接收到了请求包数据正常的。为何解析不出来String fNo;这个变量的取值，都是null??。
		"where": {
			"fNo": "dc5kk555ds",
			"cod": "csdfsg3yyy3od"
		},
*/
