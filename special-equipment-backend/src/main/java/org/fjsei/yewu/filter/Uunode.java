package org.fjsei.yewu.filter;

import graphql.schema.DataFetchingEnvironment;
import org.fjsei.yewu.util.Tool;

import java.util.UUID;

/**
 * 所有模型 需要独立node()接口读取数据的模型都要实现。
 * */
public interface Uunode extends Node<UUID>{
    UUID  getId();

    public default String getId(DataFetchingEnvironment env){
        String typeName= this.getClass().getSimpleName();
        return Tool.toGlobalId(typeName, this.getId());
    }
}

