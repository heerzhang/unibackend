package org.fjsei.yewu.filter;

/**
 * 所有模型 需要独立node()接口读取数据的模型都要实现。
 * 不上也错"md.cm.unit.Unit cannot be cast to class org.fjsei.yewu.filter.Node (md.cm.unit.Unit and org.fjsei.yewu.filter.Node are in unnamed
 * */
public interface NodeName<ID> {
    ID  getId();
    String getName();
}
