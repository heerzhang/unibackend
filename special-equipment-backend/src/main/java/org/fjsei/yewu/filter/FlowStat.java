package org.fjsei.yewu.filter;

import md.cm.flow.ApprovalStm;

/**
 * 所有zeebe流程引擎要代理的实体模型都要实现。 比如报告，申请单的DB实体。
 * */
public interface FlowStat {
    ApprovalStm  getStm();
}
