package org.fjsei.yewu.filter;

import graphql.schema.DataFetchingEnvironment;
import md.cm.base.Company;
import md.cm.base.NamePi;
import md.cm.base.Person;
import org.fjsei.yewu.util.Tool;

import java.util.UUID;

/**
 * 所有模型 需要独立node()接口读取数据的模型都要实现。
 * */
public interface UnitName {
    UUID  getId();
    NamePi  getCompany();
    NamePi  getPerson();
}

