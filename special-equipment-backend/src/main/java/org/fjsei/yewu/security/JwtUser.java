package org.fjsei.yewu.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;


/* 對比雷同于 org.springframework.security.core.userdetails.User;
    UserDetails接口 那里实际可简单到就是个字符串就行了，代表用户Spring Security认证信息。
 * Spring Security 验证专用的，和数据库－应用系统Ｕｓｅｒ没关系。
 * SpringBoot 安全 给自己做的Repository,和数据库JPA和应用都无关的。具体应用程序还需要再映射一次User模型对象。
 */
public class JwtUser implements UserDetails {

    private final UUID id;
    private final String username;
    private final String password;
    private final Collection<? extends GrantedAuthority> authorities;
    private final boolean enabled;
    //和证书有效期相关:  没有配置上发行时间"iat"没法做到!   @Deprecated
    private final Date lastPasswordResetDate;

    public JwtUser(
        UUID id,
        String username,
        String password,
        Collection<? extends GrantedAuthority> authorities,
        boolean enabled,
        Date lastPasswordResetDate
    ) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.authorities = authorities;
        this.enabled = enabled;
        this.lastPasswordResetDate = lastPasswordResetDate;
    }

    @JsonIgnore
    public UUID getId() {
        return id;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @JsonIgnore
    public Date getLastPasswordResetDate() {
        return lastPasswordResetDate;
    }

    //获取当前用户id,未登陆/匿名的是 -1。
    public static UUID getUserId() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(null==auth)  return null;
        Object principal=auth.getPrincipal();
        if(principal instanceof JwtUser) {
            UUID userid = ((JwtUser) principal).getId();
            return userid;
        }else
             return null;
    }
}

