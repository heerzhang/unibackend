package org.fjsei.yewu.graphql;

import graphql.GraphQLError;
import graphql.schema.DataFetchingEnvironment;
import org.fjsei.yewu.exception.CommonGraphQLException;
import org.springframework.context.annotation.Configuration;
import org.springframework.graphql.execution.DataFetcherExceptionResolver;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

/**手动添加一个异常转换器，
 * 【注意】不加这个的话： 就只会有一个默认的哪一个报错处理器发给前端的提示没法看清具体原因的: 都是INTERNAL ERROR。
 * 有些没打印日志的报错可能在这里捕捉到的，前端接收包也显示错误信息。
 * */
@Configuration
public class GlobalExceptionTranslator implements DataFetcherExceptionResolver {

    @Override
    public Mono<List<GraphQLError>> resolveException(Throwable exception, DataFetchingEnvironment environment) {
        List<GraphQLError> errorList=new ArrayList<>();
        if(exception instanceof GraphQLError) {
            errorList.add((GraphQLError) exception);
        }
        else {
            //【影响巨大】可能导致错误无法定位精准！后端也没报错，前端依赖出错的导致未授权前端可能不会跳转登录
            String invalidId= environment.getField().toString();
            String errStr=exception.getMessage();
            if(exception instanceof AccessDeniedException) {
                errStr="您没有被授权不能访问";
            }
            Throwable  nestExp01=exception.getCause();
            if(null!=nestExp01 && null!=nestExp01.getCause())
                errStr=nestExp01.getCause().getMessage();      //更为明确的错误
            if(!StringUtils.hasText(errStr)) {
                if(null!=nestExp01) {
//                    exception.getCause().printStackTrace();
                    errStr = nestExp01.getMessage();
                }
                else
                    errStr= exception.toString();
            }
            errorList.add(new CommonGraphQLException(errStr, invalidId));
        }
        return Mono.just(errorList);
    }
}

