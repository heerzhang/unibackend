package org.fjsei.yewu.graphql;

import java.util.List;

/**searchAfter 分页获取单一个对象的标识定位。
* */
public interface AfterValues {
    public List<String>  getAfterValues();
}

