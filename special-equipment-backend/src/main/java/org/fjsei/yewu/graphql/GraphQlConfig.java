package org.fjsei.yewu.graphql;

import graphql.scalars.ExtendedScalars;
import org.fjsei.yewu.graphql.directive.DateFormatting;
import org.fjsei.yewu.graphql.directive.RangeDirective;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.graphql.execution.RuntimeWiringConfigurer;

/**
微服务架构与 GraphQL 之间设计思路冲突的地方，也就是去中心化与中心化。 服务端没法上fragment;  https://cloud.tencent.com/developer/article/1475125?from=article.detail.1746277
通过数据库等基础软件构件的集群化手段来弥补该裂缝。 CRDB小强原生分布式数据库集群。 主体上来讲，GraphQL保留中心化查询操作的特征不变，去中心化的微服务架构只是在局部化集成进来的思路。
无法自动导入该包graphql.scalars   #两个包graphql-java都用一个基本路径的？
compileOnly 'com.graphql-java:graphql-java-extended-scalars:18.1'
拷贝jar包到工程目录 ./libs\graphql-java-extended-scalars-18.1.jar
最后我在External Libraries工程列表中单独做添加 Project Struct:Project Setting:Libraries手动添加成功后可以针对ExtendedScalars显示出自动导入提示的下划横线了。
GraphQL基础  内置指令 @include @skip；  https://cloud.tencent.com/developer/article/1746277
*/


@Configuration
public class GraphQlConfig {

/** 针对*.graphqls模型文件上的 @注解 @auth(role : "manager") 文档见      https://www.graphql-java.com/documentation/sdl-directives
 * 后端Spring for graphql切换后就还没修改的接口addTask: null 直接给null也没报错啊！
 * Scalar类型 Coercing，不然无法修改输入参数
 * 旧kickstart包的参考https://github.com/graphql-java-kickstart/graphql-spring-boot
* */
    @Bean
    public RuntimeWiringConfigurer runtimeWiringConfigurer() {

        //比如： DateFormatting    增加一个注解 .directive("dateFormat", new DateFormatting())
        //日期标量类型扩展GraphQLScalarType 实际就是Coercing<LocalDate, String>; 因为使用directive @date仅仅能验证抛出异常无法转换输入参数的。
        return builder -> builder.directive("range",new RangeDirective())
                .directive("date",new DateFormatting())
                .scalar(ExtendedScalars.Date);
    }

}

