/*
 * Copyright 2002-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fjsei.yewu.maintenance;

import md.specialEqp.inspect.IspRepository;
import org.fjsei.yewu.repository.maint.JsliceMang;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.stereotype.Controller;

@Controller
public class MaintenanceController {
	private final IspRepository ispRepository;


	public MaintenanceController(IspRepository ispRepository) {
		this.ispRepository = ispRepository;
	}


	@MutationMapping
	public JsliceMang syncEqpFromLegacy_New(@Argument String type,@Argument String arg,@Argument String pasw,@Argument Integer offset) {
		String employeeId = arg;
		Integer salary = offset;
		JsliceMang ackjm=new JsliceMang();
		ackjm.setOffs(222);
		return ackjm;
	}

}
