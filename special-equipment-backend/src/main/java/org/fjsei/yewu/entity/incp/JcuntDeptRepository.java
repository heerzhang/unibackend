package org.fjsei.yewu.entity.incp;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface JcuntDeptRepository extends JpaRepository<JcuntDept, Long>, JpaSpecificationExecutor<JcuntDept>, QuerydslPredicateExecutor<JcuntDept> {

    //函数命名【注意！】有局限性： 只能修改名字  Column(name = "UNT_ID")   private Long  jcuntid;
   // int  countAllByJcuntid(Long unitid);

    int countAllByUntId(Long uid);
}

