package org.fjsei.yewu.entity.fjtj;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface UntDeptRepository extends JpaRepository<UntDept, Long>, JpaSpecificationExecutor<UntDept>, QuerydslPredicateExecutor<UntDept> {

}