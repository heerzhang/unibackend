package org.fjsei.yewu.entity.incp;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.UUID;

//同步多个平台的总的数据源关键部分 - 设备部分。

/**监察自身机构 监察自己。
 * 数据集中在：3电梯 2容器 4起重; 电梯再按区域SUBSTR(EQP_AREA_COD,1,4)划分前3个："3501","3502","3505"地市。
 * 拆分成7大块的： 电梯其他地市"99439条", "3501","3502","3505"电梯；2容器 4起重; 其他设备类型。【7个同步大块的数据集合汇总】
 * 中间过渡形式，执行一轮的同步时刻，初始化要清空掉，同步完成不再新增加。
 * JC_JCUNT
 * */

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "JC_JCUNT")
public class Jcdunit {
   // @Id
   // @GeneratedValue(strategy = GenerationType.IDENTITY)
   // private Long id;
    //要确保唯一性！
    @Id
    private Long  jcuntId;

    private Long  supJcuntId;  //上级单位

    private String untAddr; //: "仙游县鲤南镇新园街196号"
    private String untAreaCod; // : "350322"
    private String untLkmen;
    private String untMark; //: "闽B"
    private String untName; //: "仙游县市场监督管理局"
    private String untOrgCod; //: "003719862"
    private String untPhone;
    private String unitsysid;  //: "DW_673"
    private String reconFyuntName;      //RECON_FYUNT_NAME: "莆田市人民政府"
    private String reconJcuntName;     //RECON_JCUNT_NAME: "福建省质量技术监督局"


    //映射新平台Unit
    //@ManyToOne(fetch= FetchType.LAZY) 无法跨越数据库做Hibernate实体链接。
    //@JoinColumn
     //private Unit ourunt; 跨越多数据库！， 这样也都不允许！只能定义为基本数据类型。
    private UUID uunit;    //新平台对应的Unit ID;
}


//本机8GB内存的mysql配置参数  https://www.it610.com/article/1174760213530599424.htm
//set global innodb_buffer_pool_size = 2097152; //缓冲池字节大小，单位kb，如果不设置，mysql安装后默认为128M的！！
//你的MySQL服务器与其它应用共享资源，那么上面80%的经验就不那么适用了。
