package org.fjsei.yewu.entity.incp;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface JcPermtUntRepository extends JpaRepository<JcPermtUnt, Long>, JpaSpecificationExecutor<JcPermtUnt>, QuerydslPredicateExecutor<JcPermtUnt> {

}