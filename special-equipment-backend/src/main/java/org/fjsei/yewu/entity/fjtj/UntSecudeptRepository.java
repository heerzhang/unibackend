package org.fjsei.yewu.entity.fjtj;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface UntSecudeptRepository extends JpaRepository<UntSecudept, Long>, JpaSpecificationExecutor<UntSecudept>, QuerydslPredicateExecutor<UntSecudept> {

}