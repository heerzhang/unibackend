package org.fjsei.yewu.entity.incp;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.fjsei.yewu.pojo.SliceSyncRes;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;


@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "JC_UNT_SECUDEPT" )
public class JuntSecudept implements SliceSyncRes {
    @Id
    //@Column(name = "ID", insertable=false, updatable=false)
    protected Long id;

    private String name;
    //对外标识地址
    private String SECUDEPT_ADDR;
    private String  SECUDEPT_AREA_COD;

    private String LKMEN;

    private String PHONE;
    private String MOBILE;      //优先使用

    /*这个单位ID 关联于 JC_UNT_MGE表！
    * */
    private Long  UNT_ID;
    //同步数据环节的处理结果
    private String fail;
}


