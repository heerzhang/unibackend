package org.fjsei.yewu.entity.fjtj;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface DictAreaRepository extends JpaRepository<DictArea, Long>, JpaSpecificationExecutor<DictArea>, QuerydslPredicateExecutor<DictArea> {
    //DictEqpType  findByIdCodEquals(String cod);
}

