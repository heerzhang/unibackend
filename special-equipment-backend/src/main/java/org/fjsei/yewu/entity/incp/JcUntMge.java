package org.fjsei.yewu.entity.incp;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.fjsei.yewu.pojo.SliceSyncRes;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.UUID;

/**监察的也有单位表 JC_UNT_MGE
 * */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "JC_UNT_MGE"
   //   ,uniqueConstraints={@UniqueConstraint(columnNames={"UNT_ID"}),
   //           @UniqueConstraint(columnNames={"JC_UNT_ID"}) }
        )
public class JcUntMge implements SliceSyncRes {
/*    @Id   没有必要新搞一个id, 来源表UNT_ID本身就是primary KEY+Uniqe!;唯一性主键字段。
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;*/

    //多亏：实际数据UNT_ID是唯一的。
    //旧数据导入的，不会我自己新增加的；不需要设置@GeneratedValue
    //重启动后：手动修改已经生成已经有数据的表！ 把原先id字段 AotoInc primary属性去除，UNT_ID字段新设置primary属性！
    @Id
    private Long  UNT_ID;

    private String  UNT_NAME;

    private Integer  UNT_STATE;  //: "1"

    private String UNT_ADDR;      //'单位详细地址'  填写很乱，最终门栋地址。
    //个人的 也算=单位？
    private String UNT_ORG_COD;      //组织机构代码
    //居民身份证号就是你的社会信用代码，组织统一社会信用代码  18位+验证码；
    private String UNT_LKMEN;      //单位联系人
    private String UNT_MOBILE;      //单位联系人手机
    private String UNT_PHONE;   //: "85287568/13906994526/13805919406"

    private String POST_COD;      //单位邮编
    //需要牵涉到的字段： "Z99"
    private String INDUSTRY_PROP_COD;       //行业性质
    private String UNT_PROP_COD;    //"单位性质代码", DATA_TYPE: "NUMBER"
    private String  ECONM_TYPE_COD; //: ""  "经济类型代码", DATA_TYPE: "NUMBER"

    private String  UNT_AREA_COD;

    private String LEADER; //: "无" 法定代表人或负责人
    private String LEADER_PHONE; //: ""
    private Long  MANAGE_UNT; //: ""
    //UNT_LAT,　UNT_LONG, 大多都没有数据的   == 干脆放弃引用; ?地址吞并问题？ 地址同名错用的。
    private Double  UNT_LAT; //: "24.711565"
    private Double  UNT_LONG; //: "118.626611"
    //监察JC_UNT_MGE有"ZMQ_COD", COMMENTS: "所属自贸区 0 非自贸区 1 福州片区 2 厦门片区 3 平潭片区", DATA_TYPE: "NUMBER"}
    private String  ZMQ_COD;
    //"KEYUNT_REASON_TYPE_EX", COMMENTS: "新的重点单位设立原因代码 支持多个原因", DATA_TYPE: "VARCHAR2"}
    //CALIS_COD: ""  "CA证书号",

    private UUID uunit;    //新平台对应的Unit ID;

    private String fail;    //同步报错？
}
