package org.fjsei.yewu.entity.incp;

import lombok.Getter;
import lombok.Setter;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

@Getter
@Setter
@Entity
public class JcAuthor {

        @Id
        @GeneratedValue
        private Long id;

        @Column(unique = true, nullable = false)
        private String nickname;


}

