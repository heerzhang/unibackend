package org.fjsei.yewu.entity.fjtj;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.fjsei.yewu.pojo.SliceSyncRes;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

/* 监察 部门太多了随意设置？  检验 部门有基本废弃不用。
JC_UNT_SECUDEPT =26680;   TB_UNT_SECUDEPT =35196;
JC_UNT_DEPT =115720;   TB_UNT_DEPT 748;
 维保部门：要以检验为准。检验没有数据的情形：SECUDEPT可以采用监察，但是DEPT就要忽视掉。
* */

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "TB_UNT_DEPT" )
public class UntDept implements SliceSyncRes {
    @Id
    @Column(name = "ID", insertable=false, updatable=false)
    protected Long id;

    private String name;
    //名义地址
    private String DEPT_ADDR;    //基本上是为空的。   DEPT_ADDR  实际没有用
    private String  DEPT_AREA_COD;      // NUMBER(8),

    private String LKMEN;
    private String PHONE;
    private String MOBILE;      //优先使用

    //JPA需要改名： private Long  UNT_ID; Long untId;
    private Long  untId;

    //同步数据环节的处理结果
    private String fail;
}

