package org.fjsei.yewu.entity.incp;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.fjsei.yewu.pojo.SliceSyncRes;

import jakarta.persistence.*;

/* 监察 部门太多了随意设置？  检验 部门有基本废弃不用。
JC_UNT_SECUDEPT =26680;   TB_UNT_SECUDEPT =35196;
JC_UNT_DEPT =115720条;   TB_UNT_DEPT 748;
  部门：要以检验为准。检验没有数据的情形：SECUDEPT可以采用监察，但是DEPT就要忽视掉。
过滤后不到3万： length(PHONE)>6 OR (length(MOBILE)>6 AND MOBILE<>'13599000001') 厦门居多
【提升性能】加索引untId之后，维护模块VF.cD作业函数性能提升8倍，主要jcuntDeptRepository.countAllByUntId()耗时#
* */

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "JC_UNT_DEPT",indexes={@Index(columnList = "untId",unique=false), } )
public class JcuntDept implements SliceSyncRes {
    @Id
    @Column(name = "ID", insertable=false, updatable=false)
    protected Long id;      //部门的id
    //太多报错；分解2步骤： id 71584=福州市台江区义洲街道红旗村民委员会
    private String name;
    //名义地址
    private String DEPT_ADDR;    //基本上是为空的。   DEPT_ADDR  实际没有用
   //JY: private String  DEPT_AREA_COD;      // NUMBER(8),
    private String  DEPT_AREA;    //"DEPT_AREA", COMMENTS: "所在区域", DATA_TYPE: "NUMBER"}

    private String LKMEN;
    private String PHONE;
    private String MOBILE;      //优先使用

    /*这个单位ID 关联于 JC_UNT_MGE表！
     * */
   // @Column(name = "UNT_ID")  JcuntDeptRepository.countAllByJcuntid 带来的 :No property UNT found for type JcuntDept!
   // private Long  jcuntid;   不能用！！
    //private Long  UNT_ID;
    //JPA需要改名： private Long  UNT_ID; Long untId;
    private Long  untId;        //单位id


    //同步数据环节的处理结果
    private String fail;
}
