package org.fjsei.yewu.entity.fjtj;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;


/**旧的平台 现成的行政区划代码表; 照抄(极少的重名，报错！ 需人工修改)：
 * ! 不是最新的？ 权威资源网站 收集的区划表。
 * 比较权威的 区划代码库 http://www.axunxun.com/daima/daima-sheng.php?sd=%E6%96%B0%E7%96%86
 * */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "TB_DICT_AREA" )
public class DictArea {
    @Id
    private Long id;
    private Long FAU_TYPE_PARENT_CODE;      //父区划的id;
    private String FAU_TYPE_NAME;       //区划名字
    //不是正常9位数字？的 35082312 :福建省龙岩市上杭县县属其它
    private String FAU_TYPE_CODE;       //编码- 乡镇级别;
    private String FAU_TYPE_DETAIL;     //附带说明标签
}

