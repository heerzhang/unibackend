package org.fjsei.yewu.entity.fjtj;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.*;
import java.util.UUID;

//旧平台 部门表。

//@Table(name = "TB_USER_INFO",  schema="HRUSER",   HRUSER.TB_DEPT_INFO

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "TB_DEPT_INFO",  schema="HRUSER",
        uniqueConstraints={@UniqueConstraint(columnNames={"DEPT_ID"})} )
public class HrDeptinfo {
    @Id
    @Column(name = "DEPT_ID", insertable=false, updatable=false)
    private Long  DEPT_ID;

    private String  DEPT_NAME;
    private String  AREA_COD;
    private Short   JC_AREA_COD;
    private Byte  USE_STA;
    private Byte  DEPT_TYPE;
    private Boolean  IF_LHCHK;
    private String  DEPT_ADD;
    private String  ORGCOD;

    private String  REP_COD_TAG;
    private String  DEPT_TEL;
    private String  DEPT_PRINC;
    //跨数据库了！！ 不能做关联，不能直接上不是本数据库的 Entity模型，JPA只能针对单一个数据库等于CRDB:Cluster底下某个特定的Database概念。
    private UUID division;

   // private Division division;     //本平台的Unit底下的部门ID
/*    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn
    private Division  dep;*/

}

