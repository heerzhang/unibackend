package org.fjsei.yewu.entity.incp;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface JispUnitRepository extends JpaRepository<JispUnit, Long>, JpaSpecificationExecutor<JispUnit>, QuerydslPredicateExecutor<JispUnit> {

    JispUnit findByIspuntId(Long uid);
}
