package org.fjsei.yewu.entity.incp;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.fjsei.yewu.pojo.SliceSyncRes;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.UUID;

/* 监察 单位还分出好多表，同样叫单位ID的所关联的表 竟然吃屎还不同的多个表。
这个单位表目的？是 为了设计单位等等单位是很可能消亡的单位，保留历史单位信息看似快照用途。
* */

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "JC_PMT_UNT" )
public class JcPermtUnt implements SliceSyncRes {
    @Id
    protected Long id;
    //单位名 来定位，不能重复的
    //该表的单位：竟然有40%比例无法在普通表中找出，无法直接定位到Unit 就只能是在该表查名字了。太随意了单位管理！？历史遗留数据
    //【太随意】 随意输入的名称 !差错太多!
    private String name;

    private String ORG_COD;      //"组织机构代码"
    private String MAKE_NATION;      // "制造国"
    //不算很多的 被删除；
    private Boolean IS_DEL;      // "是否删除", DATA_TYPE: "NUMBER"

    private String UNT_BAND;

    //找到： Unit表的id
    private UUID uunit;    //映射到新平台的Unit实体。

    //同步数据环节的处理结果
    private String fail;
}

