package org.fjsei.yewu.entity.incp;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface JcdunitRepository extends JpaRepository<Jcdunit, Long>, JpaSpecificationExecutor<Jcdunit>, QuerydslPredicateExecutor<Jcdunit> {

    Jcdunit findByJcuntId(Long jcuid);
}
