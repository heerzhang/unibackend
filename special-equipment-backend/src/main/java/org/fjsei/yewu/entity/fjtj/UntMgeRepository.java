package org.fjsei.yewu.entity.fjtj;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface UntMgeRepository extends JpaRepository<UntMge, Long>, JpaSpecificationExecutor<UntMge>, QuerydslPredicateExecutor<UntMge> {

}
