package org.fjsei.yewu.entity.incp;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.UUID;

/**监察- 检验机构
 * JC_ISPUNT
 * */

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "JC_ISPUNT")
public class JispUnit {
    @Id
    private Long ispuntId;   //"ISPUNT_ID", COMMENTS: "检验机构ID", DATA_TYPE: "NUMBER"
    private String untName; //: "仙游县市场监督管理局"
    private String untOrgCod; //: "003719862"
    private String untPhone;
    private String untLkmen;

    //private Long  supJcuntId;  //上级单位
    private String untAddr; //: "仙游县鲤南镇新园街196号"
    private String untAreaCod; // : "350322"

   private String  INDUSTRY_PROP_COD;       //行业性质
   private String  UNT_PROP_COD;    //"单位性质代码", DATA_TYPE: "NUMBER"
   private String  ECONM_TYPE_COD;  //: ""  "经济类型代码", DATA_TYPE: "NUMBER"  字典定义表 TB_DICT_ECONMTYPECOD；

   private String  APPR_SETUP_UNT;  //"批准成立机关"
   private String  REG_UNT_NAME;   //"营业执照登记机构",
   private String  leader;
    private Integer  ispMenNum;     //"检验人员数"
    private String  ispuntSteel; //"ISPUNT_STEEL", COMMENTS: "检验机构钢印号",
    //"IF_LEGAL_ENTITY", COMMENTS: "是否独立法人", DATA_TYPE: "NUMBER"}
    private Boolean  ifLegalEntity;
    private Boolean  ifOutIspunt;  //"IF_OUT_ISPUNT", COMMENTS: "是否外地检验机构", DATA_TYPE: "NUMBER"

    //本类来维护1：1缺省的字段关联名字；@JoinColumn(name = "townID我这一边的关联字段不一定是id", referencedColumnName = "ID是对方的ＩＤ")
    /*    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="unitId", referencedColumnName = "ID")
    private Unit unit; ？JPA hibernate 多数据库关联！ 运行错误 references an unknown entity:*/

    private UUID uunit;    //新平台对应的Unit ID;
}

