package org.fjsei.yewu.entity.fjtj;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.fjsei.yewu.pojo.SliceSyncRes;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.UUID;

/** 旧检验表的 所有单位； 反而有 addDate 用于同步，监察没有实际上设置日期？
 *  监察的也有单位表 JC_UNT_MGE
 * */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "TB_UNT_MGE"
   //   ,uniqueConstraints={@UniqueConstraint(columnNames={"UNT_ID"}),
   //           @UniqueConstraint(columnNames={"JC_UNT_ID"}) }
        )
public class UntMge implements SliceSyncRes {
  //  @Id
  //  @GeneratedValue(strategy = GenerationType.IDENTITY)
  //  private Long id;
    /*  @Id
        @GenericGenerator(name="jpa-uuid",strategy = "uuid")
        @GeneratedValue(generator = "jpa-uuid")
        private String id;*/
    //主键由多个字段联合组成，每个表只能定义一个主键。报错Composite-id class must implement Serializable
    //【稀奇了】id== -1 和 0 都用上了
    //实际数据UNT_ID是唯一的。
    @Id
    private Long  UNT_ID;    //关键！ 目前UNT_ID最大=278293 ;本表合计194194条，

    //穿越时空隧道, 另外一个平台数据库的ID;
    //JC_UNT_ID重复的很多。
    private Long  JC_UNT_ID;      //监察对应单位ID

    private String  UNT_NAME;
    private Integer  UNT_STATE;  //: "1"

    private String UNT_ADDR;      //'单位详细地址'  填写很乱，最终门栋地址。
    //个人的 也算=单位？
    private String UNT_ORG_COD;      //组织机构代码
    //居民身份证号就是你的社会信用代码，组织统一社会信用代码  18位+验证码；
    private String UNT_LKMEN;      //单位联系人
    private String UNT_MOBILE;      //单位联系人手机
    private String UNT_PHONE;   //: "85287568/13906994526/13805919406"

    private String POST_COD;      //单位邮编
    //需要牵涉到的字段： "Z99"
    private String  INDUSTRY_PROP_COD;   //行业性质 'Z01'=个人; 字典表：TB_DICT_INDUSTRYPROPCOD；
    private String  UNT_PROP_COD;    //"单位性质代码", DATA_TYPE: "NUMBER" @没啥用，很多没填！！
    private String  ECONM_TYPE_COD;  //: "经济类型代码", DATA_TYPE: "NUMBER" @没啥用，很多没填！！

    private String  UNT_AREA_COD;

    private String LEADER; //: "无" 法定代表人或负责人
    private String LEADER_PHONE; //: ""
    private Long  MANAGE_UNT; //: ""
    //UNT_LAT,　UNT_LONG, 大多都没有数据的   == 干脆放弃引用; ?地址吞并问题？ 地址同名错用的。
    private Double  UNT_LAT; //: "24.711565"
    private Double  UNT_LONG; //: "118.626611"
    //CALIS_COD: ""  "CA证书号",

    private UUID uunit;    //新平台对应的Unit ID;  MySql时期类型是Long的。

    private String fail;    //同步报错？
    //监察JC_UNT_MGE有"ZMQ_COD", COMMENTS: "所属自贸区 0 非自贸区 1 福州片区 2 厦门片区 3 平潭片区", DATA_TYPE: "NUMBER"}
}

