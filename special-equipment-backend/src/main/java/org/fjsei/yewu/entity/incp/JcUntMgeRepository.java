package org.fjsei.yewu.entity.incp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface JcUntMgeRepository extends JpaRepository<JcUntMge, Long>, JpaSpecificationExecutor<JcUntMge>, QuerydslPredicateExecutor<JcUntMge> {

}
