package org.fjsei.yewu.config;

//import org.elasticsearch.client.RestHighLevelClient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.elc.ElasticsearchConfiguration;
//import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;
//import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;


/**
 * 不能再用Rest Client和Transport Client了，推荐使用Java Client的API包。这里RestHighLevelClient也是淘汰中：ES8无法再用的。
 * ES对内存和CPU需求都比较大的。
 * 需要看spring-data-elasticsearch适配版本号，以及Elasticsearch自己也有适配版本号，综合考虑来升级：boot ES Java 版本；
 * 【诸多包不适配，暂无法升级 ES8.1.2版本】 new Elasticsearch client forces users to switch from using javax.json.spi.JsonProvider to jakarta.json.spi.JsonProvider. Spring Data Elasticsearch cannot enforce this switch; Spring Boot will switch to jakarta with version 3；
 * ES8对比ES7的代码变动将会非常大。JSON对象映射库，functional builders->函数式,和应用程序类的自动映射; https://juejin.cn/post/7080726607043756045
 * ES启动：代替配置文件
 * spring:
 *   data:
 *     elasticsearch:
 *       #cluster-name: docker-cluster
 *       #cluster-nodes: locahost:9300,192.168.171.3:9300
 *       #默认是http://localhost:9200
  版本兼容性  https://docs.spring.io/spring-data/elasticsearch/docs/current/reference/html/
 【篡改starter适配Elasticsearch的版本】spring-boot-starter-data-elasticsearch；  https://blog.csdn.net/qq_28772075/article/details/122240942
 boot 2.6.6当前默认Elasticsearc 7.15.2；     ES8版要求java>=17版。
 ES8版本： RestHighLevelClient被抛弃，替换成ElasticsearchClient，而旧的RestClient可以继续保留。ES8版本多个JacksonJsonpMapper();
 Enables compatibility mode that allows HLRC 7.17 to work with Elasticsearch 8.x. 遗留兼容 https://www.elastic.co/guide/en/elasticsearch/client/java-api-client/current/migrate-hlrc.html
 当前spring-data-elasticsearch-4.3.3#ElasticsearchRestTemplate目前实际用了RestHighLevelClient的，看来此版本变动巨大！。
 7.17.2版本告警：WARN  org.elasticsearch.client.RestClient    company/_refresh?ignore_throttled=false&ignore_unavailable=false&expand_wildcards=open&allow_no_indices=true] :
     [299 Elasticsearch-7.17.2- "[ignore_throttled] parameter is deprecated because frozen indices have been deprecated. Consider cold or frozen tiers in place of frozen indices."]
*/

@Configuration
public class ElasticsearchRestClientConfig extends ElasticsearchConfiguration {
    //密码在配置文件中；生产系统和调试环境将要对接的是不一样的ES服务端主机和集群。
    @Value("${sei.ESauthSecret:null}")
    private String esPassword;

    public ClientConfiguration clientConfiguration() {
        return ClientConfiguration.builder().connectedTo("localhost:9200")
                .withBasicAuth("elastic",esPassword).build();
    }

//    @Override
//    @Bean
//    public RestHighLevelClient elasticsearchClient() {
//         不兼容8.1.2版本的；    https://www.elastic.co/guide/en/elasticsearch/reference/8.1/rest-api-compatibility.html
//         Java REST Client (deprecated) [7.17]？淘汰啊 ，新8.0版本需要用：Java Client [8.1]；
//         .usingSsl()传入SSLContext不验证域名和证书是否匹配    http://t.zoukankan.com/zihunqingxin-p-14575139.html
//         【过渡办法】 7.9版本的API https://docs.spring.io/spring-data/elasticsearch/docs/current/reference/html/#preface.requirements
//        *
//
//        //HttpHeaders defaultHeaders = new HttpHeaders();
//        final ClientConfiguration clientConfiguration = ClientConfiguration.builder()
//                .connectedTo("localhost:9200").withBasicAuth("elastic",esPassword)
//                .withDefaultHeaders(defaultHeaders)
//                .withHeaders(() -> {
//                    HttpHeaders headers = new HttpHeaders();
//                    headers.add(HttpHeaders.ACCEPT,"application/vnd.elasticsearch+json;compatible-with=7");
//                    headers.add(HttpHeaders.CONTENT_TYPE,"application/vnd.elasticsearch+json;compatible-with=7");
//                    return headers;
//                })
//                .build();
//
//        return RestClients.create(clientConfiguration).rest();
//    }

    //添加原因，出错IspController required a bean of type 'org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate' that could not be found.
    //自己定义就能提前装入，避免Controller报错说没有Bean；但是，延伸问题是两个同名Bean冲突，二选一啊？  @Qualifier
/*    @Bean
    @Primary
    public ElasticsearchRestTemplate elasticsearchRestTemplate(RestHighLevelClient elasticsearchClient){
        return new ElasticsearchRestTemplate(elasticsearchClient);
    }*/


    //终于可以正常注入了 IspController.{ esTemplate }
//    @Override
//    @Bean(name = { "elasticsearchOperations", "elasticsearchTemplate" })
//    public ElasticsearchRestTemplate elasticsearchOperations(ElasticsearchConverter elasticsearchConverter,
//                                                           RestHighLevelClient elasticsearchClient) {
//
//        ElasticsearchRestTemplate template = new ElasticsearchRestTemplate(elasticsearchClient, elasticsearchConverter);
//        template.setRefreshPolicy(refreshPolicy());
//
//        return template;
//    }
}




/*
ES对物理内存需求很大,改jvm.options;jvm.options.d/ 全都没用java15版本=自动调jvm堆内存。 https://www.elastic.co/guide/cn/elasticsearch/guide/current/heap-sizing.html
Elasticsearch Java API客户端是一个全新的客户端库，与旧的高级Rest客户端RestHighLevelClient（HLRC）无关。
Kibana ? elasticsearch-head 部署elasticsearch8.1.1   https://blog.csdn.net/xutengfei999/article/details/123684334
用 Docker compose 来一键部署ES集群；  https://juejin.cn/post/7082735047824015397
专门定制云版本：Elastic Cloud on Kubernetes通过ECK部署，安装在Kubernetes集群内; 自签名证书https;  https://blog.51cto.com/u_14783669/2558785
Kubernetes 搭建 Elasticsearch 7.6 集群, ES普通版的k8s集群搭建：  https://ld246.com/article/1586757433779
*/
