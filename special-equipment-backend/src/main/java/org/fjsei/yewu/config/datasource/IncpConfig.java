package org.fjsei.yewu.config.datasource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.config.BootstrapMode;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import jakarta.annotation.Resource;
import jakarta.persistence.EntityManager;
import javax.sql.DataSource;
import java.util.Map;


//旧平台　对接的监察平台库。


@Configuration
@EnableJpaRepositories(
    entityManagerFactoryRef = "entityManagerFactoryIncp",
    //transactionManagerRef = "transactionManager",
    bootstrapMode= BootstrapMode.LAZY,
    //repositoryFactoryBeanClass = CustomRepositoryFactoryBean.class,
    basePackages = {"org.fjsei.yewu.entity.incp"})
public class IncpConfig {

    /* 旧的库； 監察的。
     */

    @Bean(name = "dataSourcePropertiesIncp")
    @Qualifier("dataSourcePropertiesIncp")
    @ConfigurationProperties(prefix="app.datasource.incp")
    public DataSourceProperties dataSourcePropertiesIncp() {
        return new DataSourceProperties();
    }

    @Bean(name = "incpDataSource")
    @Qualifier("incpDataSource")
    //@ConfigurationProperties(prefix="app.datasource.incp")
    public DataSource incpDataSource(@Qualifier("dataSourcePropertiesIncp") DataSourceProperties dataSourcePropertiesIncp) {
        return dataSourcePropertiesIncp.initializeDataSourceBuilder().build();
    }

    @Bean(name = "incpJdbcTemplate")
    @Qualifier("incpJdbcTemplate")
    public JdbcTemplate incpJdbcTemplate(@Qualifier("incpDataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }



    @Bean(name = "entityManagerIncp")
    public EntityManager entityManager(EntityManagerFactoryBuilder builder,
            @Qualifier("entityManagerFactoryIncp")  LocalContainerEntityManagerFactoryBean entityManagerFactoryIncp
    ) {
        return entityManagerFactoryIncp.getObject().createEntityManager();
    }

    @Resource
    private JpaProperties jpaProperties;

    private Map<String, Object> getVendorProperties() {
        return null; //jpaProperties.getHibernateProperties(new HibernateSettings());
    }

    /**
     * 设置实体类所在位置
     */

    //报红EntityManagerFactoryBuilder实际有注入
    @Bean(name = "entityManagerFactoryIncp")
    //@DependsOn("transactionManager")
    //@DependsOn("transactionManager")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryIncp(EntityManagerFactoryBuilder builder,
           @Qualifier("incpDataSource") DataSource incpDataSource) {
        return builder
            .dataSource(incpDataSource)
            .packages("org.fjsei.yewu.entity.incp")
            .persistenceUnit("incpPersistenceUnit")
        //    .properties(getVendorProperties())
            .build();
    }

    /*
    JPA使用的
    //
    @Bean(name = "transactionManagerBar")
    public PlatformTransactionManager transactionManagerFoo(EntityManagerFactoryBuilder builder) {
        return new JpaTransactionManager(entityManagerFactoryFoo(builder).getObject());
    }
    */

}
