package org.fjsei.yewu.config.datasource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.config.BootstrapMode;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import jakarta.annotation.Resource;
import jakarta.persistence.EntityManager;
import javax.sql.DataSource;
import java.util.Map;


//解决了问题：@ManyToMany或@OneToMany的Many多的那一方，一定用Set容器来存放，而不能用List集合。

//旧平台的主库(测试时接入测试平台，但正式转换后是接入旧平台生产环境的正式库)

//允许把@Configuration注释掉，关闭数据源
@Configuration
@EnableJpaRepositories(
        entityManagerFactoryRef = "entityManagerFactoryFjtj",
        //transactionManagerRef = "transactionManager",
        bootstrapMode= BootstrapMode.LAZY,
        //repositoryFactoryBeanClass = CustomRepositoryFactoryBean.class,
        basePackages = {"org.fjsei.yewu.entity.fjtj"})
public class FjtjConfig {

    /* 旧的平台运行库;  app.datasource.fjtj是配置文件的某项。
     */

    @Bean(name = "dataSourcePropertiesFjtj")
    @Qualifier("dataSourcePropertiesFjtj")
    @ConfigurationProperties(prefix="app.datasource.fjtj")
    public DataSourceProperties dataSourcePropertiesFjtj() {
        return new DataSourceProperties();
    }

    @Bean(name = "fjtjDataSource")
    @Qualifier("fjtjDataSource")
    //@ConfigurationProperties(prefix="app.datasource.fjtj")
    public DataSource fjtjDataSource(@Qualifier("dataSourcePropertiesFjtj") DataSourceProperties dataSourcePropertiesFjtj) {
        return dataSourcePropertiesFjtj.initializeDataSourceBuilder().build();
    }


    @Bean(name = "fjtjJdbcTemplate")
    @Qualifier("fjtjJdbcTemplate")
    public JdbcTemplate fjtjJdbcTemplate(@Qualifier("fjtjDataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    //最后一步执行的：当前配置有连接数据库。
    @Primary
    @Bean(name = "entityManagerFjtj")
    public EntityManager entityManager(EntityManagerFactoryBuilder builder,
                                       @Qualifier("entityManagerFactoryFjtj")  LocalContainerEntityManagerFactoryBean entityManagerFactoryFjtj) {
        return entityManagerFactoryFjtj.getObject().createEntityManager();
    }

    @Resource
    private JpaProperties jpaProperties;

    private Map<String, Object> getVendorProperties() {
        return null; //jpaProperties.getHibernateProperties(new HibernateSettings());
    }

    /**
     * 设置实体类所在位置
     */

    //报红EntityManagerFactoryBuilder实际有注入
    //@Primary 注意只能一个数据源注解@Primary的。
    @Bean(name = "entityManagerFactoryFjtj")
    //@DependsOn("transactionManager")
    //@DependsOn("transactionManager")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryFjtj(EntityManagerFactoryBuilder builder,
                   @Qualifier("fjtjDataSource") DataSource fjtjDataSource ) {
        return builder
                .dataSource(fjtjDataSource)
                .packages("org.fjsei.yewu.entity.fjtj")
                .persistenceUnit("fjtjPersistenceUnit")
                .build();

        //必须在@EnableJpaRepositories里头注解  "org.fjsei.yewu.entity.fjtj"
    }

}
