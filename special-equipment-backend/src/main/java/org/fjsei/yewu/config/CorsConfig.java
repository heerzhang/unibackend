package org.fjsei.yewu.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**这个可能是 无状态模式 seesionLess 走的路子？
 * 和WebConfig有关联吗
 * */
@Configuration
public class CorsConfig {
    @Bean
    public CorsFilter corsFilter(){
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOriginPattern("*");          //还是放开，依据登录用户权限验证。
        //config.addAllowedOrigin("http://localhost:3765");     详细配置个别前端Web服务器的。
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");

//      source.registerCorsConfiguration("/api/**", config);
        source.registerCorsConfiguration("/graphql/**", config);

        return new CorsFilter(source);
    }
}
