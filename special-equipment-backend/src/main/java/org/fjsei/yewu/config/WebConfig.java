package org.fjsei.yewu.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**这个可能是 有状态模式 seesion MVC才走的路子？
 * 和CorsConfig有关联吗
 * */
@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
//        registry.addMapping("/**");
//            // .allowedOrigins("http://192.168.171.3:3765")
//        //后面运行org/springframework/boot/autoconfigure/graphql/servlet/GraphQlWebMvcAutoConfiguration.java:153

        registry.addMapping("/**")
//                .allowedOrigins("http://192.168.1.102:3765/")
                .allowCredentials(true)
                .allowedOriginPatterns("*")
                .allowedHeaders("*")
                .allowedMethods("GET", "POST", "PUT", "DELETE", "OPTION");
//                .maxAge(3600);
    }

}

