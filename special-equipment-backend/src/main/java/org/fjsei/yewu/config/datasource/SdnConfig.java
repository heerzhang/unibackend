package org.fjsei.yewu.config.datasource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.config.BootstrapMode;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import jakarta.annotation.Resource;
import jakarta.persistence.EntityManager;
import javax.sql.DataSource;
import java.util.Map;


//旧平台　对接报检平台的库。

/**JPA实体映射才需要的：
 * 对于比如camunda服务专用数据库那种的就不需要额外做定义了。
 * */

//暂时关 @Configuration     //没有Bean牵涉到的可以彻底关
@EnableJpaRepositories(
    entityManagerFactoryRef = "entityManagerFactorySdn",
    //transactionManagerRef = "transactionManager",
    bootstrapMode= BootstrapMode.LAZY,
    //repositoryFactoryBeanClass = CustomRepositoryFactoryBean.class,
    basePackages = {"org.fjsei.yewu.entity.sdn"})
public class SdnConfig {

    /** SDN库
     * */
    @Bean(name = "dataSourcePropertiesSdn")
    @Qualifier("dataSourcePropertiesSdn")
    @ConfigurationProperties(prefix="app.datasource.sdn")
    public DataSourceProperties dataSourcePropertiesSdn() {
        return new DataSourceProperties();
    }

    @Bean(name = "sdnDataSource")
    @Qualifier("sdnDataSource")
    //@ConfigurationProperties(prefix="app.datasource.sdn")
    public DataSource sdnDataSource() {
        return dataSourcePropertiesSdn().initializeDataSourceBuilder().build();
    }


    @Bean(name = "sdnJdbcTemplate")
    @Qualifier("sdnJdbcTemplate")
    public JdbcTemplate sdnJdbcTemplate(@Qualifier("sdnDataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }


    @Bean(name = "entityManagerSdn")
    public EntityManager entityManager(EntityManagerFactoryBuilder builder) {
        return entityManagerFactorySdn(builder).getObject().createEntityManager();
    }

    @Resource
    private JpaProperties jpaProperties;

    private Map<String, Object> getVendorProperties() {
        return null; //jpaProperties.getHibernateProperties(new HibernateSettings());
    }

    @Bean(name = "entityManagerFactorySdn")
   // @DependsOn("transactionManager")  transactionManagerSdn
   // @DependsOn("transactionManager")
    public LocalContainerEntityManagerFactoryBean entityManagerFactorySdn(EntityManagerFactoryBuilder builder) {
        return builder
            .dataSource(sdnDataSource())
            .packages("org.fjsei.yewu.entity.sdn")
            .persistenceUnit("sdnPersistenceUnit")
      //      .properties(getVendorProperties())
            .build();
    }

    /*
    JPA使用的
    @Bean(name = "transactionManagerSdn")
    PlatformTransactionManager transactionManagerSdn(EntityManagerFactoryBuilder builder) {
        return new JpaTransactionManager(entityManagerFactorySdn(builder).getObject());
    }
    */

}
