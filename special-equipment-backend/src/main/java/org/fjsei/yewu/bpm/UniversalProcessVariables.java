package org.fjsei.yewu.bpm;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**注入给zeebe流程引擎： 引擎有逻辑代码和变量配置的，拼凑一部分伪代码运行。
 * 建模zeeebe的流程变量：尽量通用的， 报告流转和通用审批事务都能满足的。
 * 报错java.time.LocalDateTime` not supported by default: add Module "com.fasterxml.jackson.datatype:jackson-datatype-jsr310" to enable 无法建立流程variable
* 【全局变量】 流程变量实际遗传上一个节点的取值， 每个节点需要适当注意清理和设置新数值。
 * 感觉zeebe服务：就是割裂的代码，又无法保持独立性，流程图体现的逻辑代码调试有时容易混淆； 解释性代码 javascript引擎类比的；zeebe自己的解析器和代码调度。
 * */

@Getter
@Setter
public class UniversalProcessVariables {
    /**创建者：流程启动者 ， 发起人 */
    private String  author;

    //【特殊的】并行多实例子流程的BPMN默认就有 result字段 缺省吗？
    //private Boolean result;
    //private String tweet;

    /**前端给用户的跳转URL页面，具体操作实际在转入页面
     * 相对路径 :8371/  端口号前面都不要
     * */
    private String  uri;
    private String  ent;      //挂接的申请单或报告的GlobalID
    /**本流程实例实际针对挂接的 申请单或报告的 主题标识： 汉语描述；
     * */
    private String  topic;

    /*多个环节都可共享变量；
     * 【注意】传递和需修改赋值。
     * 在BPMN图上面看到的变量不见得一定也要在这里体现出的。默认变量result就没。
     * */
   //private Boolean  approved;  //若用boolean  approved;可能导致流程停留在某个节点的，流程变量可能还是null的。也没有报错啊【奇怪】

    //private List<String> candidateGroups;        //序列化【】可识别吗
    //  [bossg,tomens]
    //private String bossg;
    //用户组 ？？
    //private List<String> assignGroups;
    //@Deprecated
    //private String  creationDate;

    /**通用的截止日期，超时不处理的触发;
     * 整个流程上多个节点Flownode 都会共享使用的变量; 【因此】每个节点UserTask可能修改的。
     * 对应流程图定义当中的“expirationDate”变量，是在流程实例启动时配置的。
     * */
    private String  expirationDate;         //若改用Date也不行！表达式null
    /**审批环节超时了回退给审核人员后的 在审核人员处置的允许超期期限*/
    private String  fallbackExpDate;

    private boolean  overtimed;
    /**签字环节：多人并行会签的，几个都都要一起同意才算过关的，时间没有顺序，即不是顺序上串行的审批
     * */
    private List<String>  ispMens;
    //会签签名结果
    private List<String>  allsigned;

    /**报告审核节点的审核人： 责任工程师；
     * 单个人审核，
     * 通常含义的： 针对两级审批控制场景的第一位次的主管审批人。
     * */
    private String  reviewer;

    /**通常所说的：审批人，最终的审批人。
     * */
    private String  approver;

    /**分项，子报告， 为适应分支而预制的先决条件：分项报告吗？
     * 每个业务流程都可能不同的分叉前置条件变量的： 这里是Boolean  subitem，代码适应性：难道每种流程都要自己再整个*ProcessVariables以及接口函数？
     * subitem： 针对报告流转流程的意思= “是否是分项的”；
     * */
    private Boolean  subitem;



    public UniversalProcessVariables setUri(String uri) {
        this.uri = uri;
        return this;
    }
    public UniversalProcessVariables setEnt(String ent) {
        this.ent = ent;
        return this;
    }
    public UniversalProcessVariables setTopic(String topic) {
        this.topic = topic;
        return this;
    }

    public UniversalProcessVariables setAuthor(String author) {
        this.author = author;
        return this;
    }
    public UniversalProcessVariables setReviewer(String reviewer) {
        this.reviewer = reviewer;
        return this;
    }
    public UniversalProcessVariables setApprover(String approver) {
        this.approver = approver;
        return this;
    }

//    public UniversalProcessVariables setApproved(boolean approved) {
//        this.approved = approved;
//        return this;
//    }

}
