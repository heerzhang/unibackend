package org.fjsei.yewu.bpm;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**通用的申请单
 * 前面几关若有保留意见的也只能点击"通过流转下一关"，只有最后一关才是不同意意见的直接结束流程的，非最后一关的有不同意意见但是也需要流转下一关同时审核的就不能直接点击"否决"，应当用”流转下一关“+否定的意见说明模式！
 *  */
@Getter
@Setter
@Builder(toBuilder=true)
public class RequestProcessVariables {
    //前面4个是通用的全局变量名字。 每个流程Process都应当具备基本4个变量，方便前端查询显示在一起，这样"待办事项"页面统一的字段。
    /**本流程实例实际针对挂接的 申请单或报告的 主题标识： 汉语描述；
     * 某某请假，设备号xx停机，xx单位变更名字
     * */
    private String  topic;
    /**前端给用户的跳转URL页面，具体操作实际在转入页面
     * 相对路径 :8371/  端口号前面都不要
     * */
    private String  uri;
    /**挂接的申请单或报告的GlobalID*/
    private String  ent;
    /**代办列表通常会显示的 关键 前面那个人说明。后退理由。
     * 每个UserTask环节可以留下一句话给下面流程转接去的UserTask节点处理人:
     * 说明理由，否决原因。
     * 从data{ memo: "" }提取的部门意见。
     * */
    private String  say;

    //下面这些参数都是跟随流程图bpmn变动的，不固定的名字。

    /**创建者：申请单主人， 发起人 ==实体表crman字段。
     * 协议申请单创建者，检验客户 ： ？若窗口人员代为编制申请的？
     * */
    private String  author;

    /**直接领导签注 : 文员？受理人员环节； SDN客户针对某个检验机构的前台接待人员。
     * */
    private String  director;

    /**主管部门领导签注， 两阶段两个级别审批批准模式的，需要设置前置审核人。
     * */
    private String  manager;

    /**谁负责最后一关口的审核人，最后批准人。
     * */
    private String  auditor;

    /*第几个级别 几个层次的 审批几个关，该参数特殊含义代表：（通用语义，从前端感觉来讲语义的，正常最多3个人审核）
     * ”3“： 当前总共才提供一级或一关审批人，最简单的模式。
     * ”2“： 当前需要两关口的审批。
     * ”1“： 当前要求至少3个人来审批的，每个都设立关卡一共三道审批的。
     * 【用途】仅作为流转的说明性参数，表示流转给谁的具体哪一个关卡审批角色的，创建流程才需要用，启动以后根据BPMN意图就能够判定角色的就没用了。
     * */
//    private Integer  level; 失去意义了。

}

