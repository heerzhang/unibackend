package org.fjsei.yewu.bpm;

/**ZeebeBpmnError  Error 主要看 Code="duplicateMessage" 设置和BPMN文件对应上的。而Name和reference不重要；
 * 类似的 ZeebeBpmnError("DOESNT_WORK", "");  Code="DOESNT_WORK"
 * */
public class DuplicateReportException extends Exception {
    public DuplicateReportException(String message) {
        super(message);
    }

    public DuplicateReportException(String message, Throwable cause) {
        super(message, cause);
    }
}
