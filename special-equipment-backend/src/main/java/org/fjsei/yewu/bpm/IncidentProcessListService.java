package org.fjsei.yewu.bpm;

import io.camunda.operate.CamundaOperateClient;
import io.camunda.operate.auth.SimpleAuthentication;
//不能用 同名的 import io.camunda.tasklist.auth.SimpleAuthentication;
import io.camunda.operate.dto.Incident;
import io.camunda.operate.exception.OperateException;
import io.camunda.operate.search.*;
import md.cm.geography.AdminunitRepository;
import org.springframework.stereotype.Service;
import jakarta.annotation.PostConstruct;
import java.util.Date;
import java.util.List;

/**实际上又套了一层：我这通过camunda-operate-client-java包来对接Operate服务器提供的API接口，最后Operate的存储实际挖掘来自ES索引存储zeebe_record，
 * 而zeebe_record索引是由流程引擎服zeebe主动导出的, zeebe/Broker.bat它实际内部也有自己管理的最原始存储的。倒腾2遍的流程数据啊。
   Zeebe内部流程数据 => 主动导出到ES索引zeebe-record*存储 => 再次由camunda-operate服务端启动后会收集整理为ES索引operate-*存储。
 *【目的】实际Operate服务器启动的用处是为了查询Incident列表的，方便解决内部隐藏的流程问题。而且Tasklist服务器和Operate真没有依赖关联的。
 * [有问题]有这可能性，zeebe流程引擎集群自身的流程存储的有些流程实例会遗留下，客户端也一直无法知晓这些流程实例的存在的，无法自动清除被遗忘的引擎存储数据。
 * Elastic exporter如何可以把zeebe流程引擎的内部存在的历史流程实例数据全量导出？ 【思路】在关系数据库保留流程创建的历史记录，依赖外部数据库确保zeebe历史流程的清理完整性。
 * */
@Service
public class IncidentProcessListService {
    SimpleAuthentication sa;
    CamundaOperateClient  client;
    //简单独立部署环境用的：To use if Tasklist is not configured with Identity and Keycloak情况用的。
    public IncidentProcessListService(AdminunitRepository adminunitRepository) {
        sa = new SimpleAuthentication("herzhang", "testuko", "http://localhost:8271");
    }
    @PostConstruct       //启动时间Operate服务器重建索引啊，耗时间
    public void initialize() throws OperateException {
        try {
            client = new CamundaOperateClient.Builder().operateUrl("http://localhost:8271").authentication(sa).build();
        } catch (OperateException e) {
            e.printStackTrace();
        }
    }
    /**只保留故障列表的功能：Operate服务器所能提供的其它功能能直接用zeebe_client接口替代。
     * */
    public List<Incident> getIncidents(Integer pageSize) {
        if(null!=client) {
            try {
                IncidentFilter incidentFilter = new IncidentFilter.Builder().creationTime(new DateFilter(new Date(), DateFilterRange.YEAR)).build();
                SearchQuery incidentQuery = new SearchQuery.Builder().withFilter(incidentFilter).withSize(pageSize).withSort(new Sort("state", SortOrder.ASC)).build();
                List<Incident> incidents = client.searchIncidents(incidentQuery);    //若连接服务端是camunda-tasklist的这里才报错的;要配对服务端。
                int size = incidents.size();
                if(size>0) {
                    Incident incident = client.getIncident(incidents.get(0).getKey());
                }
                return incidents;
            } catch (OperateException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}

