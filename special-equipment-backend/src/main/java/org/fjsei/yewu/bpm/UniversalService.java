package org.fjsei.yewu.bpm;


/**
 * Publish content on Twitter.
 */
public interface UniversalService {

  void goUri(String content) throws DuplicateReportException;

}
