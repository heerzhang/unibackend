package org.fjsei.yewu.bpm;


import io.camunda.zeebe.spring.client.annotation.VariablesAsType;
import io.camunda.zeebe.spring.client.annotation.JobWorker;
import md.cm.flow.ApprovalStm;
import md.log.Opinion_Enum;
import md.specialEqp.inspect.Isp;
import md.system.User;
import org.fjsei.yewu.filter.FlowStat;

import java.util.List;
import java.util.Set;

public interface ApprovalStmService {
    /**开启一个新的流程；
     * processNode参数：流程定义 zeebe模板的名字。
     * 普通的审批事项单子，有可能与这个报告流转的接口参数需求并不会一致的，？？只能多设置类似的函数+参数不同的分开代码逻辑。
     * */
    public String startFlow(String bpmn,String topic,String entId, String uri, ApprovalStm stm, Opinion_Enum opinion, String  memo,
                            Integer days, List<User> authr, Boolean subitem);

   // @Deprecated
   // public Iterable<Isp> findAllISPfilter(Long taskId);


    /**流程节点是UserTask的，需要用户审批的操作，往前走或后退
     * Map<String, Object> variables流程变量：Collections.singletonMap("approved", true) #有可能多个变量，时间期限，同意，回退理由,转给多人多用户组。
     * 每个流程flowNode所考察的 variables 不见得名字一致的？ 统一化的映射zeebe的模型变量名。
     * stm参数：报告流转历史 ApprovalStm 关系数据库存储当前流程状态。其它申请单也能挂接。
     * entId： 报告ID，或者 其他的实体模型以及申请单id;
     * due: 到期时间 预期支持精度某一天凌晨24点截止。String 适配：格式可变，精度可调。
     * */

    String flowTo(String userTaskId, FlowStat ent, Opinion_Enum opinion, String memo, Integer days, Set<User> mens, String uri);

    //审批单子：参数要求不一样的话？

}
