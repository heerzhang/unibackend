package org.fjsei.yewu.index;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

/** ES对象 ID+name 映射需要；ES嵌套Object对象
 * 和ES索引的实际字段名字对应的上。    ?.id  ?.name;
 * 就没必要配套Village去定义一个VillageEs吧
 * */
@Data
@NoArgsConstructor
public class NameIdEs {
    protected UUID id;
    /**有个名字字段的；
     *实体上有 类似 @FullTextField
     *       private String  name;
    * */
    private String name;
}

