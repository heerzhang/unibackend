对照spring-data-elasticsearch生成的company的模型ES映射。
{
  "company": {
    "aliases": {},
    "mappings": {
      "properties": {
        "_class": {
          "type": "keyword",
          "index": false,
          "doc_values": false
        },
        "address": {
          "type": "text",
          "analyzer": "ik_max_word",
          "search_analyzer": "ik_smart"
        },
        "id": {
          "type": "keyword"
        },
        "linkMen": {
          "type": "keyword"
        },
        "name": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 256
            }
          }
        },
        "no": {
          "type": "keyword"
        },
        "phone": {
          "type": "keyword"
        }
      }
    },
    "settings": {
      "index": {
        "routing": {
          "allocation": {
            "include": {
              "_tier_preference": "data_content"
            }
          }
        },
        "refresh_interval": "1s",
        "number_of_shards": "1",
        "provided_name": "company",
        "creation_date": "1649722371401",
        "store": {
          "type": "fs"
        },
        "number_of_replicas": "1",
        "uuid": "hrV-p6-1SwGZEUS9bl_2RA",
        "version": {
          "created": "7170299"
        }
      }
    }
  }
}
对照旧的Eqp模型ES映射。
{
  "eqp_0906": {
    "aliases": {},
    "mappings": {
      "properties": {
        "_class": {
          "type": "keyword",
          "index": false,
          "doc_values": false
        },
        "ad": {
          "properties": {
            "_class": {
              "type": "keyword",
              "index": false,
              "doc_values": false
            },
            "cityId": {
              "type": "keyword"
            },
            "countryId": {
              "type": "keyword"
            },
            "countyId": {
              "type": "keyword"
            },
            "id": {
              "type": "keyword"
            },
            "provinceId": {
              "type": "keyword"
            },
            "townId": {
              "type": "keyword"
            }
          }
        },
        "address": {
          "type": "text",
          "analyzer": "standard"
        },
        "cand": {
          "type": "date",
          "format": "date"
        },
        "cerd": {
          "type": "date",
          "format": "date"
        },
        "cert": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 40
            }
          },
          "analyzer": "ngram_analyzer"
        },
        "cnam": {
          "type": "keyword"
        },
        "cod": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 32
            }
          },
          "analyzer": "ngram_analyzer"
        },
        "cpa": {
          "type": "byte"
        },
        "cping": {
          "type": "boolean"
        },
        "dense": {
          "type": "boolean"
        },
        "expire": {
          "type": "date",
          "format": "date"
        },
        "fno": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 150
            }
          },
          "analyzer": "ngram_analyzer"
        },
        "id": {
          "type": "keyword"
        },
        "impt": {
          "type": "keyword"
        },
        "insd": {
          "type": "date",
          "format": "date"
        },
        "insuId": {
          "type": "keyword"
        },
        "ispuId": {
          "type": "keyword"
        },
        "ispudId": {
          "type": "keyword"
        },
        "level": {
          "type": "keyword"
        },
        "makeuId": {
          "type": "keyword"
        },
        "mkd": {
          "type": "date",
          "format": "date"
        },
        "model": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 50
            }
          }
        },
        "money": {
          "type": "scaled_float",
          "scaling_factor": 1.0
        },
        "move": {
          "type": "boolean"
        },
        "mtuId": {
          "type": "keyword"
        },
        "nxtd1": {
          "type": "date",
          "format": "date"
        },
        "nxtd2": {
          "type": "date",
          "format": "date"
        },
        "nxttd": {
          "type": "date",
          "format": "date"
        },
        "ocat": {
          "type": "boolean"
        },
        "oid": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 32
            }
          },
          "analyzer": "ngram_analyzer"
        },
        "ownerId": {
          "type": "keyword"
        },
        "plat": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 20
            }
          },
          "analyzer": "ngram_analyzer"
        },
        "plcls": {
          "type": "keyword"
        },
        "plno": {
          "type": "text",
          "analyzer": "standard"
        },
        "pt": {
          "type": "geo_point"
        },
        "rcod": {
          "type": "keyword"
        },
        "reg": {
          "type": "keyword"
        },
        "regd": {
          "type": "date",
          "format": "date"
        },
        "reguId": {
          "type": "keyword"
        },
        "reguLare": {
          "properties": {
            "_class": {
              "type": "keyword",
              "index": false,
              "doc_values": false
            },
            "cityId": {
              "type": "keyword"
            },
            "countryId": {
              "type": "keyword"
            },
            "countyId": {
              "type": "keyword"
            },
            "id": {
              "type": "keyword"
            },
            "provinceId": {
              "type": "keyword"
            },
            "townId": {
              "type": "keyword"
            }
          }
        },
        "remuId": {
          "type": "keyword"
        },
        "repuId": {
          "type": "keyword"
        },
        "rnam": {
          "type": "keyword"
        },
        "seimp": {
          "type": "boolean"
        },
        "sno": {
          "type": "keyword"
        },
        "sort": {
          "type": "keyword"
        },
        "subv": {
          "type": "keyword"
        },
        "svuId": {
          "type": "keyword"
        },
        "svuLare": {
          "properties": {
            "_class": {
              "type": "keyword",
              "index": false,
              "doc_values": false
            },
            "cityId": {
              "type": "keyword"
            },
            "countryId": {
              "type": "keyword"
            },
            "countyId": {
              "type": "keyword"
            },
            "id": {
              "type": "keyword"
            },
            "provinceId": {
              "type": "keyword"
            },
            "townId": {
              "type": "keyword"
            }
          }
        },
        "titl": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 80
            }
          }
        },
        "type": {
          "type": "keyword"
        },
        "unqf1": {
          "type": "boolean"
        },
        "unqf2": {
          "type": "boolean"
        },
        "uscd": {
          "type": "date",
          "format": "date"
        },
        "used": {
          "type": "date",
          "format": "date"
        },
        "useu": {
          "properties": {
            "_class": {
              "type": "keyword",
              "index": false,
              "doc_values": false
            },
            "cancel": {
              "type": "boolean"
            },
            "id": {
              "type": "keyword"
            },
            "indCod": {
              "type": "keyword"
            }
          }
        },
        "ust": {
          "type": "keyword"
        },
        "usudId": {
          "type": "keyword"
        },
        "vart": {
          "type": "keyword"
        },
        "vital": {
          "type": "boolean"
        },
        "vlgId": {
          "type": "keyword"
        },
        "vlgName": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 30
            }
          }
        }
      }
    },
    "settings": {
      "index": {
        "max_ngram_diff": "1",
        "routing": {
          "allocation": {
            "include": {
              "_tier_preference": "data_content"
            }
          }
        },
        "number_of_shards": "1",
        "provided_name": "eqp_0906",
        "creation_date": "1649668121133",
        "analysis": {
          "analyzer": {
            "ngram_analyzer": {
              "tokenizer": "ngram_tokenizer"
            }
          },
          "tokenizer": {
            "ngram_tokenizer": {
              "token_chars": [
                "letter",
                "digit"
              ],
              "min_gram": "3",
              "type": "ngram",
              "max_gram": "3"
            }
          }
        },
        "number_of_replicas": "1",
        "uuid": "Umk4ljYzTSeRi7w3uWAOhA",
        "version": {
          "created": "7170299"
        }
      }
    }
  }
}
