package org.fjsei.yewu.index;

import graphql.schema.DataFetchingEnvironment;
import lombok.Data;
import lombok.NoArgsConstructor;
import md.specialEqp.RegState_Enum;
import md.specialEqp.UseState_Enum;
import md.specialEqp.inspect.Detail;
import md.specialEqp.type.Pipeline;
import org.fjsei.yewu.util.Tool;
import org.springframework.data.elasticsearch.annotations.Document;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;


/** 目前本ES索引还未被使用！！ 类似的getAllEqpEsFilter()??， 只有hibernate-search 、HS做索引存储保存（后台人工 批处理作业处理）。
 * 前端页面还没用到这个？
 * 【用处是】管道单元PipingUnit搜索索引的：spring-data-elasticsearch工具提供的--只读搜索模型。
 * */
@Document(indexName="pipingunit-read", createIndex=false) //报错屏蔽！
@Data
@NoArgsConstructor
public class PipingUnitEs {
    protected UUID id;
    //这里主要都是需要 过滤的字段， 不需要过滤字段假设在前端轮播概要版本ListItem列表所需要展示的那些关键定位识别字段的也需要放在这里：避免二次查询走关系数据库。
    private Pipeline pipe;
    private String  code;

    /*从多对一 修改成 多对多之后，ES索引定义似乎没有太大修改; 自定义@PropertyBinding(binder = @PropertyBinderRef(type = NodeIdBinder.class))不能照搬：输入是List<>输出似乎一致?
     "det": {
          "dynamic": "strict",
          "properties": {
            "id": {
              "type": "keyword"
            }
          }
        },
     "dets": {
          "dynamic": "strict",
          "properties": {
            "id": {
              "type": "keyword"
            }
          }
        },
    * */
    private  List<Detail> dets;       //只需 [ID!]
    //private Detail  det;

    private String  rno;
    private String  start;
    private String  stop;
    private UseState_Enum ust;
    private RegState_Enum reg;
    private String level;

    private LocalDate nxtd1;
    private LocalDate nxtd2;

    public String getId(DataFetchingEnvironment env) {
        return Tool.toGlobalId(this.getClass().getSimpleName(), this.id);
    }
}

