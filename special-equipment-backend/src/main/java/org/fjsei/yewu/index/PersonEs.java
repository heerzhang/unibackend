package org.fjsei.yewu.index;

import graphql.schema.DataFetchingEnvironment;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.fjsei.yewu.util.Tool;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.UUID;

//保持模块独立性，减少关联依赖，就不在PersonEs底下添加一个业务映射ID，也就是unit_ID字段。
//PersonEs设计成这样｛id, name, pos{Address.id}, extadr扩展地址文本串[门牌号]｝pos可提前搜并选定id过滤！随后进一步extadr是搜索；
//@Document(indexName = "person")  旧的spring-data-elasticsearch模型;

/**公民库： 原来考虑到独立性，并没有把Unit unit字段纳入Person实体模型的，PersonEs更没考虑存储Unit的id的；只能回到关系数据库去查单位。
 * PersonEs和CompanyEs实际完全可能脱离于本平台存在啊。要是多后端多系统共享一套的PersonEs和CompanyEs存储ES库，那么关系数据库同步？
 * ES库和关系数据库相关的。多个后端多系统每个都有自己单独独立运作的关系数据库啊，关联表啊。？数据的分发和更新同步。
 * Person和Company有专门特殊独立的数据维护者的，同步发布数据更新。 本后端本系统只负责被动更新保持同步。
 * 所以Unit unit还是不纳入 PersonEs 索引库。
* */
@Document(indexName="person-read", createIndex=false)
@Data
@NoArgsConstructor
public class PersonEs {
    protected UUID id;

    private String name;        //人名。
    private String no;       //身份证号码
    /**可快递的地址文本全部。  仅支持模糊搜索
     * 有了这个字段就给性能带来很大压力！
     * */
    private String address;     //详细住址，首要办公地点，楼盘地址。

    /*当前归属 地理 区域:ad实际6个ID
     * */
    private AdminunitEs ad;

    private String phone;       //手机号
    //旧的用id()；不能再用了。
    //替换掉public String getId(); 把函数getId()归还给java部分来使用，避免影响扩大。而graphQL可自动获取id()
    public String getId(DataFetchingEnvironment env) {
        return Tool.toGlobalId(this.getClass().getSimpleName(), this.id);
    }
}

