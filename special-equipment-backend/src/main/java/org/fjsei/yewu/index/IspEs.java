package org.fjsei.yewu.index;

import graphql.schema.DataFetchingEnvironment;
import lombok.Data;
import lombok.NoArgsConstructor;
import md.cm.unit.Unit;
import md.specialEqp.BusinessCat_Enum;
import md.specialEqp.Eqp;
import md.specialEqp.inspect.Detail;
import org.fjsei.yewu.util.Tool;
import org.springframework.data.elasticsearch.annotations.Document;

import java.time.LocalDate;
import java.util.UUID;


/** Isp搜索索引的：通过使用 spring-data-elasticsearch 这个包做的： 只读搜索模型。
 * 因为不涉及映射，生成更新索引。所以有大部分注解省略掉了。 该部分功能让HibernateSearch包替换掉同样功能了。
 * 记录个数上可能是最多的ES库表了：上亿条可能。
 * 实际就是外部的登记号码报告号映射为内部的数据库UUID检验Isp.id过程。替代传统关系数据库或CRDB的按照no索引搜索定位Isp的功能 #索引替换#。
 * */
@Document(indexName="isp-read", createIndex=false)
@Data
@NoArgsConstructor
public class IspEs {
    protected UUID id;

    private Detail  bus;
    //private UunodeEs  bus;  前端报错
    //private Report report; 没有

    private BusinessCat_Enum bsType;
    private Boolean  entrust;
    private Eqp  dev;

    private String  no;
    private String  no_sort;

    /*【问题】 这个类型Unit不是正常的JPA实体Unit；借用的带来混淆的。数据库和ES索引的数据都用一个类型?。
     ES搜索结果特殊性：
     直接套用Unit带来问题：却又不想改id从ID！改为ID； @破坏规矩：正常graphQL的Type就必须提供ID! GlobalID! !
     前端的代码不能用servu{id name company{name} person{name}}后端搜索IspES实际提供的company{person{没有ID字段的。会报错!
     ES索引又不想存储 company{person{id 字段。
     @只能人工协调前后端了。    否则另外定义类型？ 关联对象graphQL模型改不同名称，#嵌套#啊太麻烦：类型interface的数量增加太多！。
        type IspEs {
            servu: Unit         改Unit成别的类型interface ？;
        }
        type Unit？ { company: Company }          改成 "？
        type Company？ { id:ID!  name: String!}      改成 "id:ID？
    * */
    private Unit  servu;

    private LocalDate ispDate;
    private Unit  ispu;     //缺省 =福建省特检院。

    //最终给监察平台看的再做设置；报告未终结以前是用 主报告Report.stm流转状态机的检验人员列表;长期保存档案使用的。 本后端不能用这个字段，
    //List<User> ispMen; 集合：多对多； @ ES引擎还不好处置啊  @。
    //User checkMen;审核人。

    //给前端看的 IspEs 直接改为用 Isp
    public String getId(DataFetchingEnvironment env) {
        return Tool.toGlobalId("Isp", this.id);
    }
}

