package org.fjsei.yewu.index;

import lombok.Data;
import lombok.NoArgsConstructor;

import jakarta.persistence.Id;
import java.util.UUID;


/**Hibernate Search实际并不需要这个类定义！本类是要配套Spring Data Elasticsearch做查询而带来！前者负责生成同步ES索引库，后者用在搜索读取场合。
 * 以上两个中间件，我这都用的，配合和优缺点互补。
 * Elasticsearch【非独立索引】；仅作为AddressEs内部对象FieldType.Object类型。AddressEs内引用到的；
 * 实际上每一个AddressEs所关联到的即使是相同id的AdminunitEs，在存储上都是单独的，也就是每条AddressEs都复制一份AdminunitEs，修改AdminunitEs需要额外同步数据。
 最大5级行政； 首先找到行政分解的最小单元Adminunit 得到id，用来过滤地址库。
 最小一级行政级别 townId； Town 一一对应Adminunit；
 */

@Data
@NoArgsConstructor
public class AdminunitEs {
    //保留Adminunit.id可以和数据库直接对接，定位Adminunit;
    /**相应的Entity数据库实体类的ID类型从Long改成UUID
     * */
    @Id
    protected UUID id;
    //没必要设置之这个字段，搜索目的用途：
    //private String  prefix;  //ES复制的存储空间很大，没有意义？
    //支持这种搜索行为：若Adminunit只是选择了一部分，而不是街道最小行政级别Town的id如何办：？行政区划4个等级+1的；

    //假如一个街道乡镇的名字？Long ID, 做了修改了，也要额外处理同步问题？
    //？？节省ES反向索引的存储空间需求。没必要@OneToOne关系，搜索选定了街道了，直接选出Adminunit.id就是了。
    private UunodeEs town;
    private UunodeEs county;
    private UunodeEs city;
    private UunodeEs province;
    private UunodeEs country;


    /*基本的 一对函数重写
     * */
    /*@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long sumid= countryId+provinceId+cityId+countyId+townId+id;
        result = prime * result + (int)(sumid^(sumid>>>32));
        return result;
    }
    //hashCode必须和equals一起被重载的！
    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj)   return true;
        if (!(obj instanceof AdminunitEs))   return false;
        AdminunitEs that = (AdminunitEs) obj;
        //判断两个Long类型是否相等，Long==Long不好使！
        return (id.equals(that.id) && townId.equals(that.townId) && countyId.equals(that.countyId) && cityId.equals(that.cityId)
                && provinceId.equals(that.provinceId) && countryId.equals(that.countryId) );
    }*/
}



//搜索模式：如果指明了ad.id那么直接name里面搜，如果ad.id没有选的那么全地址搜索,搜索出匹配程度最大的，多个省份的类似地址。
