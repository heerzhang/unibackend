package org.fjsei.yewu.index;

import lombok.Data;
import lombok.NoArgsConstructor;
import md.cm.geography.Adminunit;

import java.util.UUID;

/*
 graphql模型文件定义union UnitEs= CompanyEs | PersonEs，但这union和java定义我这里UnitEs似乎都没有半点相关的，仅仅同名!!
 */

//实际上等价于JSON字符串？替代id{java关联对象}对比Text{json};
/** 确实可以查询得多个索引的数据的，
 * company和person两个模型的前端搜索输入和呈现字段的差异：但是可能要求按照name无法分辨到底公司还是个人的一起搜索需求。UnitEs不能应对两种graphQL模型需求一种Union一种Type。
 * 其它思路：前端呈现一种搜索不适合可直接跳转：点击可直接跳转搜索另外一个模型；方便操作即可。
 * 所以还是免去这个做法：用@Document(indexName="company-read,person-read",createIndex=false) class UnitEs{ }来构建统一搜索对象的思路。采用老模型。
 * graphQL还有用 union UnitEs= CompanyEs | PersonEs，但是和ES索引毫无关系啊。
 *  */

//@Document(indexName="company-read,person-read", createIndex=false) 注释掉：spring-data-es就不能搜。
@Data
@NoArgsConstructor
@Deprecated
public class UnitEs {
    protected UUID id;    //union UnitEs= CompanyEs | PersonEs，是xxxES.id, 不是单位Unit.ID;
    /**单位已经注销，但是还需要显示名称啊；已经无效的单位，仅剩下名字有用。
     * */
    private Boolean cancel;  //单位已经被注销；cancelled; [企业解散？,个人死], 资格问题？

/*    //没有必要直接用单位名称来搜索出设备，要分两步走，首先定位出单位，再找设备; ？ ES嵌套很深的？ 扁平化，反规范化的。
    @MultiField(mainField= @Field(type=FieldType.Text),
            otherFields={ @InnerField(suffix="keyword",type=FieldType.Keyword, ignoreAbove=180)
            }
    )
    private String name;
    //被管辖地区　码
    @Field(type = FieldType.Text, analyzer = "ik_max_word", searchAnalyzer = "ik_smart")
    private String address;*/

    /**行业性质: 树 INDUSTRY_PROP_COD：  单位细分种类；个人，行业
     */
    private String  indCod;

    /**因为Eqp实体注解了@IndexedEmbedded(includePaths = {"id","company.name","person.name"} )所以也能用name直接搜索设备Eqp;
    Eqp延伸下Unit再到Company|Person实体定义模型找到@FullTextField name是全文搜索。【一个定义ES类型】两个物理模型的索引Eqp+{Company|}都能用上。
    * */
    private String name;        //企业或机构名。行政机关名字。

    //仅仅支持精确匹配
    private String no;       //统一社会信用代码
    /**有了该字段极大地降低ES实体保存地速度，该字段消耗磁盘性能，对存储速度要求大。
     * */
    //不做注解的话String默认生成mapping是Text底下再嵌套Keyword的类型;字符串将默认被同时映射成text和keyword类型.
    //仅仅支持模糊搜索
    private String address;     //首要办公地点，楼盘地址。

    private Adminunit ad;

    /*PersonEs专有的*/
    private String phone;       //手机号

/*    public boolean equalOf(Unit unit) {
        if(null==unit)  return false;
        return ( unit.getId().equals(id)  &&  unit.getCancel()==cancel
                && (null==unit.getIndCod() && null==indCod || null!=unit.getIndCod() && unit.getIndCod().equals(indCod) )
          );
    }*/
}

