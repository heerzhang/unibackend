package org.fjsei.yewu.index;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

/** ES对象ID 映射需要
 * */
@Data
@NoArgsConstructor
public class UunodeEs {
    protected UUID id;
}

