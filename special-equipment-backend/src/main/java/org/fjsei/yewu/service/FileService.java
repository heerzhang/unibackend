package org.fjsei.yewu.service;

//REST的做法与graphQL的做法，实际上差别还是很大的。
//service目录的原型就是来自 RESTful的处理框架模式的常见目录结构。DAO, DTO, controller, service 等REST目录结构。
/**
 * @program: spring-boot-example
 * @description: 相当于REST的接口处理等相关的目录功能部分，
 * @author:
 * @create: 2018-05-02 10:02
 **/

public interface FileService {

    String  addFile(String url);
    String  getFileOfID(String id);
}

