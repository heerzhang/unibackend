package org.fjsei.yewu.service;


import org.fjsei.yewu.repository.Teacher;

import java.util.List;
import java.util.UUID;

/*
 * @program: spring-boot-example
 * @description:
 * @author:
 * @create: 2018-05-02 11:12
 */

///@Transactional
public interface JpaService {
   // Student findByName(String name);
    Teacher getTeacher(String name);
    List<Teacher> getAllTeacher();

    public Teacher addTeacher(Teacher topic);

    void setTeacherAge(UUID userId, String age);
    Teacher getTeacherById(Long id);
    //测试 CRDB
  void upsert();
  void upsertAll(int numberOfRecords);
    void testIt();
}

