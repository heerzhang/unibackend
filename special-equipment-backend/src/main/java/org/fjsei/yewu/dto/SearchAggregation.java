package org.fjsei.yewu.dto;
@Deprecated
public class SearchAggregation {

    public final String value;
    public final Long count;

    public SearchAggregation(String value, Long count) {
        this.value = value;
        this.count = count;
    }
    
}
