package org.fjsei.yewu.dto;

import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
@Deprecated
public class SearchRequest {

    public final String keyword;

    public final String abbreviation;

    public final LocalDate date;

    public final Pageable page;


    public SearchRequest(String keyword, String abbreviation, LocalDate date, Pageable page) {
        this.keyword = keyword;
        this.abbreviation = abbreviation;
        this.date = date;
        this.page = page;
    }

}
