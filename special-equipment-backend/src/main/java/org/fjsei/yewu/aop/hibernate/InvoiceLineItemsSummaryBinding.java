package org.fjsei.yewu.aop.hibernate;

import org.hibernate.search.mapper.pojo.mapping.definition.annotation.processing.PropertyMapping;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.processing.PropertyMappingAnnotationProcessor;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.processing.PropertyMappingAnnotationProcessorContext;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.processing.PropertyMappingAnnotationProcessorRef;
import org.hibernate.search.mapper.pojo.mapping.definition.programmatic.PropertyMappingStep;

import java.lang.annotation.*;

/** #没用到 @
 * 桥接例子， hibernateSearch 注解 @InvoiceLineItemsSummaryBinding
 * 没必要用，直接上@IndexedEmbedded(includePaths = {"id","cod","ad.city"} )
 * 通常没必要用的。
 * */
@Deprecated
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})
@PropertyMapping(processor = @PropertyMappingAnnotationProcessorRef(
        type = InvoiceLineItemsSummaryBinding.Processor.class
))
@Documented
public @interface InvoiceLineItemsSummaryBinding {

    String fieldName() default "";

    class Processor implements PropertyMappingAnnotationProcessor<InvoiceLineItemsSummaryBinding> {
        @Override
        public void process(PropertyMappingStep mapping, InvoiceLineItemsSummaryBinding annotation,
                            PropertyMappingAnnotationProcessorContext context) {
            NodeIdBinder binder = new NodeIdBinder();
            String anFiled=context.annotatedElement().name();
          //  if (!annotation.fieldName().isEmpty()) {       }
            binder.fieldName(anFiled);
            mapping.binder(binder);
        }
    }
}

