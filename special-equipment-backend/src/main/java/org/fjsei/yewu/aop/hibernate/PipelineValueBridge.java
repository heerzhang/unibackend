package org.fjsei.yewu.aop.hibernate;

import md.specialEqp.type.Pipeline;
import org.hibernate.search.mapper.pojo.bridge.ValueBridge;
import org.hibernate.search.mapper.pojo.bridge.runtime.ValueBridgeToIndexedValueContext;

/** # 没用到 @
 * 变值桥接 hibernateSearch 例子，
 * 通常没必要用的。
 * */
@Deprecated
public class PipelineValueBridge implements ValueBridge<Pipeline, String> {
    @Override
    public String toIndexedValue(Pipeline value, ValueBridgeToIndexedValueContext context) {
        return value == null ? null : value.getId().toString();
    }
    //正常不需要fromIndexedValue() 相反地：为projection操作 直接读取实体对应类型的值。
}

