package org.fjsei.yewu.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
public class MetricsLoggerAspect {
    @Around("@annotation(MetricsLogger)")
    public Object proceed(ProceedingJoinPoint joinPoint) throws Throwable {
        final long startTime = System.currentTimeMillis();
        Object result = joinPoint.proceed();
        final long timeTaken = System.currentTimeMillis() - startTime;
        log.info("[衡量] class={}, method={}, 花费时间={}ms",joinPoint.getSignature().getDeclaringTypeName(),joinPoint.getSignature().getName(),timeTaken);
        return result;
    }
}

