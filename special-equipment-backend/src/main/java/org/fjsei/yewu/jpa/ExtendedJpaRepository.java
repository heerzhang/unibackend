package org.fjsei.yewu.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

/**
补充JPA接口，定做;  利于配套Cockroach DB，
提高性能。CRDB小强数据库的充分利用upsert、upsertAll优化；
 UPSERT vs. INSERT ON CONFLICT不一样： https://www.cockroachlabs.com/blog/sql-upsert/
【注意】限制条件：1，若存在非主键的唯一性约束的字段，若出现唯一性冲突可能无法执行成功。
        条件：2，存在其它非主键的索引会影响性能的提升，需要多读一次索引。
     3: 实体类代码依赖于注解@Table @Column。nativeQuery 执行原生sql语句。
用法：仓库接口定义 interface XxxRepository extends JpaRepository<StoreSync, UUID>,
 可直接用 ExtendedJpaRepository 来替换正常的 JpaRepository ；
 随后仓库用户可以用upsert upsertAll替换掉正常的save saveAll；
by ：  herzhang ， 2022/04/22
 旧的是QueryDslPredicateExecutor 改名字了？QueryDslPredicateExecutor 现在是 SpringBoot 2 中的 QuerydslPredicateExecutor
 [局限性]只能在特殊场景才能发挥用处。
*/

@NoRepositoryBean
public interface ExtendedJpaRepository<T,ID>  extends JpaRepository<T,ID> {
    //@Transactional
    //[啥！]For tables with secondary indexes, there is no performance difference between UPSERT and INSERT ON CONFLICT.没多大实际意义?
    @Modifying
    T upsert(T entity);

    //@Transactional
    @Modifying
    List<T> upsertAll(List<T> entities);

   // Page<T> findAll(Predicate predicate, Pageable pageable);

    /**顺带改了 abstract class ExtendedJpaRepositoryImpl
     * */
  //  @Override
  //  <T> Slice<T>  readAllBy(Pageable pageable, Class<T> type);

   //没法子做！ <T> Slice<T> findAll(Pageable pageable); 编译问题；
}

