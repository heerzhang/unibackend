package org.fjsei.yewu.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;


/**普遍 JPA的 xxxRepository都用的：
* */
@NoRepositoryBean
public interface NormalExecutor<T,ID> extends QuerydslNcExecutor<T>, ProjectionRepository<T, ID>,
                                        JpaRepository<T, ID>, JpaSpecificationExecutor<T>
{
    /*如果定义  Streamable<T> findAllBy(.querydsl.core.types.Predicate predicate,Pageable pageable);
     启动出错：.data.repository.query.QueryCreationException: Method has to have one of the following return types[List,Page,Slice]
    */
}
