package org.fjsei.yewu.repository;

import lombok.*;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.security.access.prepost.PreAuthorize;

import jakarta.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;


//@SequenceGenerator(name = "repnoSeq", initialValue = 1001, allocationSize = 1, sequenceName = "sequence_repno")
//@Table(name = "teacher3", schema = "flow", catalog="fjtj" ) ，无法设置catalog没用#
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder=true)
@Entity
@Table(name = "teacher", schema = "flow")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL, region = "Fast")
public class Teacher {
    /**分布式数据库不能用Long做ID，需要UUID;
     *    @GeneratedValue(strategy = GenerationType.IDENTITY)
     *    private Long id;
     * */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

/*    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "commonSeq")
    @SequenceGenerator(name = "commonSeq", initialValue = 1, allocationSize = 1, sequenceName = "SEQUENCE_COMMON")*/

    private String name;
    /**不能加 @Column( columnDefinition="TEXT (2000)") 无法初始化建表：还不会启动就可报错
     * */
    @Lob
    @Basic(fetch= FetchType.LAZY)
    private String age;

    private Byte course;
    private LocalDateTime regd;
    /**有些编码需要 递增的： 插入新的ROW就自动生成编号
     * 注意@Id注解 若是多个 必须是组合的组件，IDClass需自定义的，要implements Serializable；
     * 不好用@Column(updatable =false, columnDefinition ="SERIAL4 DEFAULT nextval('sequence_repno')" ) 前提条件是sequence_repno必须另外创建。
     * 第一种是单独定义一个@Generated()列来接收seqNO做法：
     @Generated(GenerationTime.INSERT)
     @Column(updatable =false, columnDefinition ="bigint default nextval('sequence_repno')" )
     注解方式必须额外创建sequence后才能用。  SERIAL类型； sno不可以加index Unique/KEY会报错,普通index可以不报错。
     * 另外一种做法：查询nextval('sequence_repno')直接利用seq取值，不用独立保存seqNO。
     * */
    private Long sno;       //当前事务中new Teacher().save()就能立刻读取到新的sno编号；
    //正式报告编码
    private String repno;


    public Teacher(String name, String age, Byte course) {
        this.name = name;
        this.age = age;
        this.course = course;
    }
    @Override
    public String toString() {
        return "Teacher{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", age='" + age + '\'' +
            ", course='" + course + '\'' +
            '}';
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAge() {
        return age;
    }
    public void setAge(String age) {
        this.age = age;
    }
    /**导致REST RespResult<Teacher> getTeacher 报错，无法序列化
     * */
    @PreAuthorize("hasRole('ADMIN')")
    public Byte getCourse() {
        return course;
    }
    @PreAuthorize("hasRole('ADMIN')")
    public void setCourse(Byte course) {
        this.course = course;
    }
}
