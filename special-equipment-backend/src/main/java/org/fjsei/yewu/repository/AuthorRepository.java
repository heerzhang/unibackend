package org.fjsei.yewu.repository;


import org.fjsei.yewu.jpa.ExtendedJpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface AuthorRepository extends ExtendedJpaRepository<Author, UUID> {

}
