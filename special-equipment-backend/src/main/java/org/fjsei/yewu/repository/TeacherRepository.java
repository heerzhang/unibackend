package org.fjsei.yewu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;
import java.util.UUID;

/** TeacherDao 改名：
 * 无法创建表：@Repository注解不能加在这里！
* */

//只是读的 没必要@Transactional
public interface TeacherRepository extends JpaRepository<Teacher, UUID>, JpaSpecificationExecutor<Teacher>, QuerydslPredicateExecutor<Teacher> {
    Teacher findByName(String name);

    @Query(value = "from Teacher t")
    List<Teacher> findAll();

    Teacher getById(UUID id);

    @Query(value ="SELECT max(sno) FROM Teacher")
    Long maxSno();


}


//CREATE SEQUENCE customer_seq_cached CACHE 10; improved performance 缓存是数据库端的，漏号更多了。
