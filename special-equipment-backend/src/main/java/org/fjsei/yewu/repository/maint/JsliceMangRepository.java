package org.fjsei.yewu.repository.maint;

import org.fjsei.yewu.jpa.ExtendedJpaRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.UUID;

public interface JsliceMangRepository extends ExtendedJpaRepository<JsliceMang, UUID>, JpaRepository<JsliceMang, UUID>,
        JpaSpecificationExecutor<JsliceMang>, QuerydslPredicateExecutor<JsliceMang> {

    JsliceMang findByName(String nmid);
}
