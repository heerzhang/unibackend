package org.fjsei.yewu.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.fjsei.yewu.pojo.SliceSyncRes;

import jakarta.persistence.*;
import java.util.UUID;

//同步多个平台的总的数据源关键部分 - 设备部分。
/**数据集中在：3电梯 2容器 4起重; 电梯再按区域SUBSTR(EQP_AREA_COD,1,4)划分前3个："3501","3502","3505"地市。
 * 拆分成7大块的： 电梯其他地市"99439条", "3501","3502","3505"电梯；2容器 4起重; 其他设备类型。【7个同步大块的数据集合汇总】
 * 中间过渡形式，执行一轮的同步时刻，初始化要清空掉，同步完成不再新增加。
 * 用upsertAll替换saveAll的情况才需要添加多余的注解@Table @Column；确保能兼容saveAll,并且builder时:new(Entity)其@Id需要人工生成的。
 * 多出name = "StoreSync" 数据库都变成小写的。 多出注解@Column(name = "xx")才能浪字段对upsert生效。
 * AK之后需剔除无效的设备代码组合。{mysql加索引,否则很慢!}
 * 但是执行AK初始化时不要添加索引；索引应该等待所有数据准备完成后，在给下一步TZ过滤和查询的环节之前才去手动添加的。插入数据不要索引，查询数据要求添上索引。
 * CRDB非enterprise license版 PARTITION BY无法做: Zone Multi-region功能。
 * SHOW PARTITIONS FROM TABLE StoreSync;
 * ALTER TABLE StoreSync PARTITION BY RANGE (id)
 *         (PARTITION graduated VALUES FROM (MINVALUE) TO ('83c6b7cf-d091-4b4a-9eb6-e1e66b6bd4a7'::uuid),
 *         PARTITION current VALUES FROM ('83c6b7cf-d091-4b4a-9eb6-e1e66b6bd4a7'::uuid) TO (MAXVALUE));
 *非全表删除，CRDB数据库删除数据性能考虑： 过滤字段必须建立索引。可重做的小事务分批次受限大小的记录数删除。最好排序获取ID范围，例子如下：
 DELETE  from public.storesync where fail='OK' AND id<='20dbf4b0-a0d5-4be1-bd85-43dc4c4fbb2f'
 ORDER BY id DESC LIMIT 5000  RETURNING id,cod,oid,fail;  修改id<=''取值，多次执行删除，最后检查是否条件满足所有的记录都已删除。
 * */

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "StoreSync")
public class StoreSync implements SliceSyncRes {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private UUID id;
    /*mySQL 时期用的
    @GenericGenerator(name="jpa-uuid",strategy = "uuid")
    @GeneratedValue(generator = "jpa-uuid")
    private String id; */

    //用JPA高级功能时会报错，JPA Repository规范要求的字段名：必须是小写头的，也不能用_号分割的。字段名尽量连续小写的变量。
    @Column(name = "cod")
    private String  cod;
    /**不会存在 oid +cod 同时为空记录。
     * */
    @Column(name = "oid")
    private String  oid;

    //private Long    oldId;  //现在都没有cod=null'' && oid=null''的数据！
    //不能用 字段名：from
    //private Byte  frdb;     //0=JY, 1=JC, 2=JC_TEMP; 3=new add();

    /**
     * TZ过滤过后：可以全部删除本轮产生fail!=null数据=全部都清空。
     * 删了再alter table storesync add fail varchar(2048)
     * 报错或提示，未能实际有效完成同步作业。
     * */
    @Column(name = "fail")
    private String  fail;
}


//本机8GB内存的mysql配置参数  https://www.it610.com/article/1174760213530599424.htm
//set global innodb_buffer_pool_size = 2097152; //缓冲池字节大小，单位kb，如果不设置，mysql安装后默认为128M的！！
//你的MySQL服务器与其它应用共享资源，那么上面80%的经验就不那么适用了。
