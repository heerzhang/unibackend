package org.fjsei.yewu.repository;


import org.fjsei.yewu.jpa.ExtendedJpaRepository;
import org.fjsei.yewu.jpa.ProjectionRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
/**在引入findAllByIdBetween代替StoreSyncRepository.findAll之后,OD作业在并发多通道作业时表现良好，3通道竟然可3倍加速，解决事务争用相互拆台问题。
 * */
//Long 后改了uuid  String;  UUID
@Repository
public interface StoreSyncRepository extends ExtendedJpaRepository<StoreSync, UUID>,
        JpaSpecificationExecutor<StoreSync>, QuerydslPredicateExecutor<StoreSync>, ProjectionRepository<StoreSync, UUID> {
    //数据全部生成以后：对cod oid两个字段 手动添加索引，给后面过滤查询用。
    List<StoreSync>  findAllByCod(String cod);
    List<StoreSync>  findAllByOid(String oid);

    //这是专门针对着ProjectionRepository.：来实现其未做明确实现的接口的
    <P> Slice<P> readAllBy(Pageable pageable,Class<P> type);

    //【提升性能】添加上主键的排序范围搜索比默认做的findAll(page)更快！Id排序 范围 搜索[分两段] 可依据其他的有索引字段来过滤和拆分分区。
    //【考虑】尽量避免深度翻页，Pageable的offset越大就越慢！所以才需要过滤器和多通道拆分成多个作业！findAll没用where太多了必然会导致深度翻页，性能就差很多。
    <P> Slice<P> findAllByIdBetween(UUID id1,UUID id2,Pageable pageable,Class<P> type);

}


/*[分两段] 00000591-c96e-4c3a-bf95-236a16d0cf6b   '83c6b7cf-d091-4b4a-9eb6-e1e66b6bd4a7'::uuid   fffff502-308d-4514-bb2b-88686fa024c5
* */