package org.fjsei.yewu.pojo.jsn;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**本来只是给前端看的：
* */
@SuperBuilder(toBuilder=true)
@Getter
@Setter
@NoArgsConstructor
public class VesselPam extends DeviceComnPam {
    private String  工作介;
    private Float   充重;     //数字范围太大，
    private String  工作压;    //壳程/管程：1.0/0.4
    private String  工作温;    //壳体/夹套：-196/50
    private String  筒料;
    private String  封料;
    private String  衬料;
    private String  夹料;
    private String  壳用压;     //—0.1～0.25
    private Float  壳高压;      //数据太少， 都能转 float;
    private String  壳用温;    //入口/出口：150/35
    private String  管用压;    //上：≤0.8 下：≤0.3
    private Float   管高压;
    private String  管用温;    //上：-15 下：≤145
    private String  夹用压;     //"真空";  1.2(上)/0.4(下)
    private Float   夹高压;
    private String  夹用温;    //-15~164
    private String  筒腐裕;    //0(内筒)/1.0(夹套)
    private String  头腐裕;    // 壳程/管程：0.6/1.0
    private Float  壳体重;
    private Float  内件重;
    private Float  舱用压;
    private Float  舱高压;
    private String  空调式;
    private String  照明;
    private String  测氧;
    private String  空调机;    //"磁耦合"
    private String  表量程;
    private String  表精度;
    private String  医疗登记;
    private String  联合国号;   //没数据
    private Float  罐高压;
    private Float  罐试压;
    private Float  功率;
    private String  侧稳定角;
    private Float  车形长;    //没数据
    private Float  车形宽;
    private Float  车形高;
    private Float  轴荷前;     //没数据
    private Float  轴荷后;
    private Float  轴荷中;
    private String  充系数;
    private Float  充重量;
    private String  腐裕;     //内筒：0 外筒：1.0
    private String  保温料;
    private String  热处;
    private Float  耐试压;
    private String  气试压;    //没数据
    private String  铭牌位;    //没数据
    private String  罐色;     //没数据
    private String  运行态;     //？?"租赁"  "自用，生产，长期使用""医用" "自用/生活/备用"
    private String  安全;    //'5级" "三级"
    private String  装卸位;    //没数据
    private String  装卸式;
    private Byte   危险介;    //只有3条数据！：{给后端是数字的}危险介质分类 1== 第一组； 2== 第二组
    //常压的类型 R000:
    private String  介质;
    private String  铭牌;
    private String  运输证;
    private String  委托书;
    private String  外尺寸;
    private String  额定压;
    private Float  价格;
    private String  桶数;
    private String  桶规格;
    private String  修改人;    //user ID?
}

