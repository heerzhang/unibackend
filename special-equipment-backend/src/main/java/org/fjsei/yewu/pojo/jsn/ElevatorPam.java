package org.fjsei.yewu.pojo.jsn;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**本来只是给前端看的：
* */
@SuperBuilder(toBuilder=true)
@Getter
@Setter
@NoArgsConstructor
public class ElevatorPam extends DeviceComnPam {
    private String  钳型号;
    private String  钳编号;
    //JY:才有字段:
    private String  层门型号;
    private Float   底坑深度;
    private String  补偿方式;    //数据很少的！
    private String  对限速号;
    private String  对限速型;
    private String  额定电流;    //"18.1/13.3"=上下 两个合体的？
    private String  缓编号;
    private String  缓型号;
    private String  缓厂家;
    private String  上限电速;
    private String  上限机速;
    private String  下限电速;
    private String  下限机速;
    private String  移护型;
    private String  移护号;
    private String  装修;     //JY:
    private String  锁型号;

    private String  上护编号;
    private Boolean   手机信;   //"IF_SCANMOBILE", COMMENTS: "是否手机信号覆盖", DATA_TYPE: "VARCHAR2"}
    private String  限速器号;
    private Float   限绳直径;
    private String  爆炸物质;
    private Float  上行额速;
    private Float  下额定速;
    private String  限机械速;
    private String  泵编号;
    private String  泵流量;
    private String  泵型号;
    private String  泵转速;
    private String  液油型号;
    private String  防爆证号;

}

