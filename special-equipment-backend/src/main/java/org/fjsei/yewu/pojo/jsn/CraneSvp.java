package org.fjsei.yewu.pojo.jsn;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**本来只是给前端看的：
* */

@SuperBuilder(toBuilder=true)
@Getter
@Setter
@NoArgsConstructor
public class CraneSvp extends DeviceComnSvp {
    private String  臂构式;
    private String  臂类;
    private String  变幅施;
    private String  节联接;
    private String  搭载式;
    private String  底架型;
    private String  吊臂型;
    private String  钩时额量;
    private String  钩时幅;
    private Float  斗时额量;
    private String  斗时幅;
    private Float  专时额量;
    private String  专时幅;
    private String  额起升速;    //"0-28.8/57.6/72"
    private String  制动载荷;
    private String  防爆;     //"ExdⅡCT4"
    private String  防爆级;    //"dIIBT4"
    private String  附装名;    //监察没有该字段！   "钢丝绳电动葫芦"
    private String  附装品;    //监察没有字段    "升降机/电动葫芦桥式起重机"
    private Float  工半径;    //没数据
    private String  工环境;
    private Float  轨长;
    private String  轨长桥;        //"环形10'
    private Float  轨距;
    private String  回转;     //监察没有字段  "上回转"
    private String  架桥承;     //监察没有字段!   "900"
    private String  监检式;     //没数据!
    private String  门跨度;    //范围大
    private String  门结构;    //监察没有字段!   数据很少！
    private String  平衡重;    //没数据!
    private Float  副钩高;    //监察没有字段!
    private String  起升速;
    private Float  副钩速;
    //【奇了怪】
    private String  覆力矩;     //[检验平台]没数据!  ？监察技术表反而有 6条数据的？？ 可能填甜不填的
    private String  设型式;     //监察没有字段   "整跨架设式"
    private String  设规范;    //[检验平台]没数据!  ？监察技术表反而有 140条数据的  "GB/T3811-2018"
    private String  伸展;
    private String  生产率;    //3000t/h
    private String  升降速;
    private Boolean  是监;
    private Boolean  有附装;    //监察没有字段
    private Boolean  有监控;    //监察没有字段
    private Boolean  专导梁;   //监察没有字段
    private String  塔顶型;
    private String  塔身型;
    private String  出入口;    //监察没有字段
    private String  桅杆;     //"桁架式"
    private String  悬长;     //"36/26"
    private Float   悬2长;
    private Float  悬跨度;
   // private Boolean  有副臂;  因为是收费需要的参数所以从svp移入到DB结构化字段：
   // private Boolean  有副钩;
    private String  车方法;    //"刷卡或手动按钮开关"
    private String  支腿;     //"钢丝绳"
    private String  造规;     //没数据!
    private String  梁结构;    //监察没有字段!   "箱形"
    private String  梁数;     //监察没有字段  "单主梁" "其它"
    private String  主吊具;
    private String  主悬件;    //监察没有字段
    private String  主升机构;   //监察没有字段
    private String  主体式;
    private String  主腿构;    //监察没有字段
    private Float   自重;     //监察没有字段
    private String  组装型;
    private String  最幅起;    // "1.57/1.5"
    private Float   最工幅;
    private String  层站数;
    private String  架设式;
    private Float  架设跨;

    //升降机添加的：
    //private String  节联接;
    private String  层门型;
    private String  层门式;
    private String  传动;
    private String  葫芦号;
    private String  葫芦型;
    private String  电动机类;
    private String  电机号;
    private String  电机型;
    //private String  笼行程;
    private String  对绳径;     //"KXS-X-280"
    private String  额定载人;    //没数据
    private String  坠保护;    //"防坠器"
    private String  附属种;     //没数据
    private String  工作式;
    private String  缓冲器;
    private Short   笼数;
    private String  对重块数;    //"整块水泥"
    private String  限速器型;
    private String  门式;
    private String  门驱动;
    private String  门方向;
    //private String  伸展;
    private Boolean  是防爆;
    private Boolean  绝缘;
    //private String  有附装;
    private String  拖动;
    private Float   绳直径;
    private String  绳数;      //"CD2-6D"
    //private String  最工幅;
    private Float   最大幅;

}

