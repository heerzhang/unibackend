package org.fjsei.yewu.pojo.jsn;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**本来只是给前端看的：
* */

@SuperBuilder(toBuilder=true)
@Getter
@Setter
@NoArgsConstructor
public class FactoryVehicleSvp extends DeviceComnSvp {
    private String  牌型;      //检验没数据；  监察有，可是不正常  "CPCD30"
    private String  防爆级;    //监察没有字段！ "EX s d ib ⅡBT4 Gb/DIP A21 TA，T4"
    private String  车类型;
    private Short   厢数;      //监察没有字段！  =2  || =3, 没啥数据
    private String  传动;
    private String  底盘号;
    private String  电机号;
    private String  机编号;
    private String  机功率;       //"2×12.5"
    private String  机转速;    //"2400~2500"
    private Float   后轮距;    //监察没有字段
    private Integer   驾员;    //? ?  "100000",
    private Float   升最高;    //监察没有字段！
    private Float   前轮距;    //监察没有字段
    private String  燃料;
    private String  设规范;
    private Float   电压;
    private String  行装置;
    private String  制规范;
    private Float   最坡度;    //监察没有字段
    private Float   时速;

}

