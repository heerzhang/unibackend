package org.fjsei.yewu.pojo.jsn;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import md.cm.base.Company;
import md.cm.unit.Unit;

import java.util.UUID;


/**Json中存储的Unit简易版
 * */

//@Builder
@Getter
@Setter
@NoArgsConstructor
public class AjsonUnit {
    private UUID id;
    private String  name;
    //只保留了  名称 和 id； 历史快照信息， 以后修改了名字怎么办？ 单位被注销了消失了？
    public AjsonUnit(Unit unit) {
        Company company= unit.getCompany();
        this.name= (null!=company)? company.getName(): unit.getPerson().getName();
        this.id = unit.getId();
    }
    public AjsonUnit(String name,UUID  id){
        this.name=name;
        this.id =id;
    }
}
