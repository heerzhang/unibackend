package org.fjsei.yewu.pojo.jsn;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**本来只是给前端看的：
* */
@SuperBuilder(toBuilder=true)
@Getter
@Setter
@NoArgsConstructor
public class CranePam extends DeviceComnPam {
    private String  锁编号;
    private String  锁型号;
    private String  爆区;
    private String  爆物;
    private String  承索数;
    private String  馈电式;
    private Float  导支跨度;
    private String  吊具型;    //没有数据!
    private String  笼行程;     //升降机才有数据  "(37.7m/min)0.63"
    private Float  顶升速;     //没有数据!
    private String  动力系统;
    private Float  副钩1重;
    private Float  副钩2重;
    private String  电方轻;
    private String  回转角;    //"主钩：±60;副钩：360"
    private Float  基距;
    private Float  集前伸;     //监察没有该字段！
    private Float  副臂长;     //监察没有该字段！
    private String   副钩倍;     //监察没有该字段！
    private Float  副额起;     //监察没有该字段！
    private Short   附着;
    private String  检跨度;
    private Float  架铰高;
    private String  所在地;     //监察没有该字段！ 最新流动地址？
    private Float  主臂长;     //监察没有该字段！
    private String  主钩倍;
    private String  检吊具;
    private Float  工作幅;
    private String  小车数;    //监察没字段  "多小车" "其它"
    private String  起重臂;    //"箱形"
    private Float  总重;
    private String  取物;
    private String  所爆级;
    private String  标高差;
    private String  提力矩;    //没数据
    private Float   提绳径;    //升降机才有数据
    private String  停车式;    //'纵向"
    private String  深度;
    private String  降速;
    private Float  驶速度;
    private String  走范围;    //监察没字段
    private String  走速度;    //"25/15"
    private String  移动型;    //监察没字段
    private String  易爆物;    //监察没字段 ,  就2条有数据
    private Float   效半径;
    private Float  效重量;
    private String  车运行时;    //"升降:28s/横移:24s"
    private String  轨道;
    private Float  总功;     //没数据
    private Float  钩1重;     //监察没字段 ,
    private Float  钩2重;     //监察没字段 ,
    private String  余冲程;     //区区1条有效数据
    private Float  自由高;     // ?? "SSD60?á60"
    private String  纵移速;    //"3.7/3.7"
    private Float   独立高;    //监察没字段
    private Float   外伸距;
    private String  最小幅监;    //"17/20"
    private Float   最小幅;
    //private AjsonUnit  监系单;
    private String  监系单;      //监察没有字段!; ？没必要纳入单位管理；可以随意点输入 企业名称。
    private String  监系号;
    private String  监系型;

    //升降机添加的：
    private String  钳编号;
    private String  钳型号;
    private String  电机功率;       //"11KW×2"  "15/3.4"
    private String  电机转速;    //"1395/1680"
    private Float   顶层高度;
    private Float   对重轨距;
    private String  额定电流;    //"16.9/21.2"
    private String  防爆标志;     //没数据
    private Short   笼数检;
    private String  对块数检;
    private String  坠安号;
    private String  坠安型;
    private String  缓编号;
    private String  缓型号;
    private String  缓厂家;
    private Float   轿厢轨距;
    private Float   轨架高;
    private Float   附墙架;
    //?? "铭牌未标识":
    private String  下限电速;    //"未标识"
    private String  下限机速;    //"1.30~1.34"
    private String  限速器号;
    private Float   门数;
    private String  环境;
    private String  速比;     //"61:1"
    //private String  提绳径;
    private String  限机械速;   //"1.2;↵1.03"
    private Float   限绳直径;
    private String  曳引比;    //"2: 1"

    private String  型试样;     //: "否": "是"
    private String  轮节径;       //"2:1" ??
    //private String  自由高;
    private String  用途;     //'人货两用"
    //private String  独立高;
    private String  作业人;      //没数据！

}

