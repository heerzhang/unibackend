package org.fjsei.yewu.pojo.jsn;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

/**本来只是给前端看的：
 * 非结构化json 存储的设备选择，关联Eqp;
* */

@SuperBuilder(toBuilder=true)
@Getter
@Setter
@NoArgsConstructor
public class EqpDat {
    //除了ID: Relay ID 要求特殊外，其他字段可直接映射;
    private String  id;    //实际存储是globalID类型的，导致不能直接List<Eqp> devs替代的，json存储并非uuid的!
    private String cod;
    private String oid;
    private LocalDate crDate;       //graphQL前端反过来存储给非结构化json变成String
}

