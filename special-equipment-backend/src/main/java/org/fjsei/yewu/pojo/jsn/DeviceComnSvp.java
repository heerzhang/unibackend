package org.fjsei.yewu.pojo.jsn;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

/**本来只是给前端看的：json{}
 * */

@SuperBuilder(toBuilder=true)
@Getter
@Setter
@NoArgsConstructor
public class DeviceComnSvp {
    private String  年限;   //有效数据少，改成Float的意义不大，有的'年限'需要附带说明;
    private Boolean  重监控;
    private String  制造国;
    private String  固资值;    //忽略
    private Integer  大修周期;
    private Integer  维周期;
    private LocalDate   设计日期;
    private AjsonUnit   造监检单;   //复杂一点的
    private LocalDate   竣验日;
    private AjsonUnit   设计单位;
    private String  产品标准;
    private String  设计图号;
     //private LocalDate   安竣日; 改成 Eqp.insd
    private Double  总重量;
    private String  装项负责;
    private String  装联电;
    private String  型试报告;
    private String  设鉴单位;
    //简化模式的关联单位 json{id , name}
    private AjsonUnit  型试单位;
}

