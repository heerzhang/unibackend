package org.fjsei.yewu.pojo.jsn;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**本来只是给前端看的：
 * */
@SuperBuilder(toBuilder=true)
@Getter
@Setter
@NoArgsConstructor
public class DeviceComnPam {
    private String  急救电;
    private String  维保电;
    private String  急救人;
    private String  联系电;    //"USE_PHONE", COMMENTS: "联系人电话",
    private String  联系人;    //"USE_LKMEN", COMMENTS: "设备操作人员/联系人",
}

