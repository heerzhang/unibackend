package org.fjsei.yewu.pojo.jsn;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

/**本来只是给前端看的：
* */
@SuperBuilder(toBuilder=true)
@Getter
@Setter
@NoArgsConstructor
public class BoilerPam extends DeviceComnPam {
    private Float   饱和温度;
    private String  补给水;
    private String  出口温;     //"中压228/低压215"
    private String  出口压;    //"17.12/4.18"
    private String  出热量;       //就1条数据?
    private Float   出水温度;       //"-10000.00" ??
    private String  出渣;
    private String  除氧;
    private Short   司炉数;
    private String  电站情况;      //没数据
    private Float   额定出力;
    private String  给水温;     // "高/中/低156.7/155.3/55"
    private String  给水压;    //"中压2.4/低压0.7"
    private String  工作温度;    //"≤230"
    private String  筒工作压;    //"2.16/0.6"
    private String  过热温;     //"高压310/低压180"
    private String  过热压;    //"高压1.35/低压0.3"
    private String  回水温;    //"≤260"
    private String  介出温;    //"≤170.3"
    private LocalDate  介验日;    //监察没有该字段
    private String  试验介质;
    private LocalDate  压验日;    //监察没有该字段  "28-9月 -19"
    private Float   试压力;
    private String  能效测标;    //监察没有该字段 "余热锅炉"
    private String  能效评价;    //没有数据;     监察没有该字段
    private String  省煤构;
    private Float   蒸发量;     //监察没有该字段
    private Float   使用年限;   //没有数据;   监察没有该字段
    private String  使用压力;    //监察没有该字段   "9.8/2.15"
    private Boolean  有过热器;
    private String  受热布置;    //没有数据;   监察没有该字段
    private String  水处模式;
    private LocalDate   水压试日;   //监察没有该字段 ; 检验就2条数据 "18-5月 -21"
    private String  水压试力;     //"2.6/0.83"
    private String  水油联话;    //没有数据;
    private String  水油联人;    //没有数据;   监察没有该字段
    private Short   水员数;      //就1条数据；
    private String  液验介;     //没有数据;
    private LocalDate  液验日;     //有些是，倒序的： ?"22-10月-19"  "29-3月 -16"
    private String  液验压;       //"气密0.7/油压1.05"
    private Float   再热出温;
    private Float   再热入温;
    private Float   再热出压;
    private Float   再热入压;    //??问题数据：  数字范围变动大！！
    private Float   再汽流;
    private Float   直启动流;
    private Float   直启动压;     //没有数据;
    private String  最连蒸;     //"4t/h"  "高压263.5"
    //旧 检验的Eqp的字段
    //private Boolean   微压炉;      //'IF_WYL'在旧平台都已经算作废了。

}

