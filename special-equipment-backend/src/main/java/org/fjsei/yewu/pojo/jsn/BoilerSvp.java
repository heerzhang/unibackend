package org.fjsei.yewu.pojo.jsn;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**本来只是给前端看的：
* */

@SuperBuilder(toBuilder=true)
@Getter
@Setter
@NoArgsConstructor
public class BoilerSvp extends DeviceComnSvp {
    private String  加热方式;
    private String  水循环;
    //private String  装置型式;   //实际数据较少
    private String  工作介质;
    private String  安装况;     //"移装"
    private String  炉房型;
    private String  汽水分离;
    private String  用途;
    private String  主体材料;    //"SA 106 Gr.B、SA 234 WPB"
    private String  过热调温;
    private String  介名;
    private String  介牌号;
    private Float   介许温;
    private String  予热器构;
    private String  燃器布置;
    private String  烧设备;     //检验平台没数据  ；  监察平台反而有10条数据： "燃天然气燃烧器"
    private String  设标准;
    private String  设出口温;    //"270/170"
    private String  设出口压;    //"1.96/0.55"
    private String  设规范;
    private String  设挥发;     //"5-6%"
    private String  设热效;     //数字范围太大  "1400"

    private Float   设低热;
    private String  设低热位;    //配套？ 检验没数据!；   监察烂数据  "大卡" "MJ/kg"

    private String  状态;      //?? eqp.ust 重复了
    private String  水处式;
    private String  水处设型;
    private String  制水能力;    //'220×3'
    private String  水处造单;    //监察没有这个字段！ "Pentair" "美国" "润新" "厦门神禹水处理设备有限公司"
    private String  水源种;     //没有数据！
    private String  烟尘式;
    private String  许用压;     //"≤1.25"  "过热器/再热器10.49/3.44"
    private String  有载牌号;
    private String  许工温;     //"中压270/低压170"
    private String  再热调温;    //无有效数据
    private String  蒸汽用途;    //监察没这字段
    private String  制造范;     //检验就1条数据  监察6条数据;  "蒸汽锅炉安全技术监察规程"

}

