package org.fjsei.yewu.pojo.jsn;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

/**本来只是给前端看的：
 * 非结构化json 存储的本平台某个对象模型的ID选择，关联某个UUID,globalID;
 * 发送给第三方应用程序接口的有可能需要附加字段:String addon?:来关联反馈的标签定位？ json:非结构化类比IdCrDate做法。
* */

@SuperBuilder(toBuilder=true)
@Getter
@Setter
@NoArgsConstructor
public class IdCrDate {
    //除了ID: Relay ID 要求特殊外，其他字段可直接映射;
    private String  id;      //实际存储是globalID类型的
//    private String  name;
    private LocalDate crDate;       //graphQL前端反过来存储给非结构化json变成String
}

