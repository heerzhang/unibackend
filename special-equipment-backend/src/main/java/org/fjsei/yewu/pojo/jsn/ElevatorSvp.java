package org.fjsei.yewu.pojo.jsn;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**本来只是给前端看的：
* */

@SuperBuilder(toBuilder=true)
@Getter
@Setter
@NoArgsConstructor
public class ElevatorSvp extends DeviceComnSvp {
    private Short 电梯站数;     // '电梯站数'
    //类型不对； private int 电梯门数;     数据库可能为null?
    private Short 电梯门数;       // is '电梯门数'
    private String  屏号;
    private String  曳引号;
    private String  主机号;
    private Float   电机功率;    //原本旧数据定义是String 实际数据支持 强制转换成 float 不会有太大影响的;
    private String  电机转速;
    private Float   顶层高度;
    private Float   对重轨距;    //强制String转换成 float 实际数据:不会有太大影响的;
    private String  对重块数;    //实际数据 不是绝对数字的; 大块+小块，高度描述。
    private String  额定载人;
    private String  轿厢轨距;
    private String  上护装置;
    private String  速比;
    private String  拖动;
    private String  曳引比;
    private Float   轮节径;
    private String  绳数;

    private Boolean  是钢带;    //our? 本平台设置：优化处理；同步时刻  ？不好自动化转换！
    //不能显示出来 private String  绳直径;     //"φ 10" （mm） 单位的 数字 ;DRAG_DIA;
    private Float   绳直径;
    private String  钢带规格;   //【"钢带 30×2.5"】新 特别分解 from if(绳直径)?:  ,导入出：双方向映射转换的？

    private String  电动机类;
    private String  顶升形式;
    private String  导轨型式;     //数据极少！
    private Float   轿厢高;
    private Float  轿厢宽;
    private Float  轿厢深;
    private String  区域防爆;
    private String  驱动方式;
    private Boolean  船梯;
    private Boolean  公共交通;
    private Boolean  汽车电梯;  //Boolean类型 =null或者 false的字段，在生成JSON时刻应该直接抛弃不用生成 Key/Value。
    private Float  梯级宽度;
    private String  悬挂绳数;
    private Float  悬挂绳径;
    private Float   油缸数;
    private String  油缸形式;
    private String  防爆标志;
    private Float  倾斜角度;
    //Float 数字类型，更麻烦？ 直接上String类型：导致前端编辑器是number输入框的情形，就无法显示出来。
    private Float  泵功率;
    private String  上护型号;
}

