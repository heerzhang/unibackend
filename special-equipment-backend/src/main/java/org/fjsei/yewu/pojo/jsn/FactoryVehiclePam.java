package org.fjsei.yewu.pojo.jsn;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**本来只是给前端看的：
* */
@SuperBuilder(toBuilder=true)
@Getter
@Setter
@NoArgsConstructor
public class FactoryVehiclePam extends DeviceComnPam {
    private String  环境;     //"易燃易爆场所"  比例很少的；
    private String  用区域;
    private String  车空重;
    private String  胎型;
    private Short   头座位;
    private Short   厢座位;
    private String  引力;     //"18.1KN"  单位不统一， 数字范围大;
    private String  驱动;
    private String  油类;
    private String  场防爆级;
    private String  区域型;     //很多不是预定义的<Select/> : "矿区" "施工场地"
    private String  颜色;
    private Boolean   有拖;
    private Float   速度;
    private Float   载心距;     //监察没有该字段

}

