package org.fjsei.yewu.pojo.jsn;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**本来只是给前端看的：
* */
@SuperBuilder(toBuilder=true)
@Getter
@Setter
@NoArgsConstructor
public class PipingUnitPam  {
    private String  工压;    //"1.2～1.3"  "≦0.3"
    private String  试压;     //"耐压：1.73↵泄漏：1.5"  "0.4/0.23"
    private String  工温;    //"420" "-18～-10"  "环境温度"
    private String  防腐材;    //"环氧煤沥青" "红丹防锈漆"
    private String  沥青级;    //"3PE加强级"
    private String  防腐厚;    //"≥150Цm"    "架空≥0.32↵埋地≥1.7"mm
    private String  工介;    //"氨" "回收醋酸"
    private String  实用时;    //有效数据很少的！   "16年7个月"  "2015-01-09"
    private String  绝材;    //"聚氨酯" "岩棉" "泡沫玻璃"
    private String  绝厚;    //"120/100" "180/160/140"
    private String  保数;    //数据很少的！     "安全阀1个，↵压力表2个"
    private String  备注;     //数据很少的     "2018年大修检验新增"
    private String  图号;     //监察竟然没有该字段    "1M1826-01-GD-S60"
}

