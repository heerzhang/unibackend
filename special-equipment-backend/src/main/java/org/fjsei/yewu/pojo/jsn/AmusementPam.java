package org.fjsei.yewu.pojo.jsn;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**本来只是给前端看的：
* */
@SuperBuilder(toBuilder=true)
@Getter
@Setter
@NoArgsConstructor
public class AmusementPam extends DeviceComnPam {
    private String  车数;     //"13+2(备用)"
    private String  电动机转速;    //没数据
    private Float   副功率;
    private Float   轨矩长;
    private String  回收装;
    private Float   主功率;
    private Float   滑梯高;
    private Float   平距;
    private Float   线速度;    //【特殊】检验没数据！  反而监察有点数据的= B/C级别的有14条，A级别的有19条。 单位问题=数据范围变化大
    private Float   圆速度;    //检验没数据！   监察有点数据 10条的
    private Float   直半径;     //检验没数据！   监察有点 6条的数据

}

