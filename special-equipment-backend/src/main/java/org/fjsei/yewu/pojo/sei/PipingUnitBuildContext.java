package org.fjsei.yewu.pojo.sei;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import md.cm.geography.Adminunit;
import md.cm.unit.Unit;
import md.specialEqp.inspect.Isp;
import md.specialEqp.type.Pipeline;

import java.util.HashMap;
import java.util.Map;

/**同步管道单元 集合，构造数据的相似性，关联模型可以做缓存，
 * 目的是加快生成速度。多次的管道单元构建时刻短暂时刻之内：共享缓存的 数据库当前查询到的数据。
 * */
@Getter
@Setter
@NoArgsConstructor
public class PipingUnitBuildContext {
    /**ad 行政区划; 缓存
     * */
    private Map<String, Adminunit>  madm= new HashMap<>();
    /**desu设计单位 ;  insu安装单位; 缓存
     * 单位Name没有能够映射Unit的警告信息 缓存
     * <K / V> Unit=?null：表示没能映射Unit的信息警告
     * */
    private Map<String, Unit>  munit= new HashMap<>();
    /**isp1 2; ispsv; Isp 缓存
     * */
    private Map<String, Isp>  misp= new HashMap<>();
    /**管道装置对象：Eqp
     * */
    private Pipeline  pipe;
}

