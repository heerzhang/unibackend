package org.fjsei.yewu.pojo.jsn;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**本来只是给前端看的：
* */

@SuperBuilder(toBuilder=true)
@Getter
@Setter
@NoArgsConstructor
public class AmusementSvp extends DeviceComnSvp {
    private Float   半径;
    private String  蹦极;
    private Float   场面积;
    private Float   电压;
    private String  额载荷;     //"6人/筏/次"  "1人/道/次"
    private Float   副速度;
    private Float   绳直径;
    private Float   高差;
    private Float   轨长;
    private Float   轨高;
    private Short   轨数;
    private String  轨矩;     //范围变动大； mm 还是 m 做的单位??
    private String  提送;
    private String  道种类;
    private String  道材索根;    //"双索"
    private Short   索数;
    private String  回收式;     //数据 就2条   "下回收"
    private String  驱动式;
    private Float   设备高;     //数据 就8条
    private Float   深度;
    private String  设规范;
    private Boolean  小蹦;     //监察没有该字段！
    private String  池型;
    private Float   池深;
    private String  制规范;
    private Short   舱数;    //检验就1条数据；   监察有点数据:  ？A级别不属于普通特检院来检验。
    private Float   舱高;

}

