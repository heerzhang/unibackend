package org.fjsei.yewu.pojo.jsn;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**本来只是给前端看的：
* */

@SuperBuilder(toBuilder=true)
@Getter
@Setter
@NoArgsConstructor
public class PipelineSvp extends DeviceComnSvp {
    private String  保温式;
    private String  腐材料;
    private String  腐裕量;     //"YG01:0,其他:1.0"
    private String  规许可证;    //监察没有字段！ "建字第350521202000020"  较少有数据的；
    private String  工作压;
    private String  工作温;
    private String  公直径;     //mm  都是数字?
    private String  公壁厚;     //'9.5/9.27/7.11"
    private String  衬厚;      //没有数据！
    private String  起点;     //"锅炉主汽阀至车间分汽缸"
    private String  止点;     //"储罐"   "安全地点放空"
    private String  设证号;    //监察没有字段！   "TS1810314-2021"
    private String  最高温;     //"≤50"
    private String  最高压;     //"≤0.7"
    private String  管道设备名;      //主表?Eqp父类  EQP_NAME 字段
    private Boolean   附属设;      //同步数据来自Eqp主表;  监察没有该字段！

}

