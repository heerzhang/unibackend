package org.fjsei.yewu.pojo.jsn;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

/**本来只是给前端看的：
* */

@SuperBuilder(toBuilder=true)
@Getter
@Setter
@NoArgsConstructor
public class PipingUnitSvp  {
    private String  规格;
    private String  设压;     //"0~0.49" "0.75/FV" "1.8/1.0"  "0.4/-0.08"
    private String  设温;    //"—45/160"  "-196℃～50℃"  "400"
    private String  焊数;    //"PE管2个↵钢管2个"  "PE管4个"  "7/81"  "3+1（1口Ф89）"
    //管子材料牌号； 字段PIPELINE_DATUM_SIGN  检验的 数据较少！！ 在监察平台也是少有数据，该字段实际应该被 PIPELINE_MEDIUM 所替换掉；
    //private String  管料;    //"20#" "20↵GB/T 8163-2008"
    //直接删除“ ” .管料(bsp.getPipelineDatumSign())
    private String  材料;     //字段=PIPELINE_MEDIUM管道材质； 不是"V_PIPELINE_MEDIUM"字段,

    //监检INCP_NEXT_ISP_DATE; 监检报告下检日期
    private LocalDate  监下;
    //腐蚀裕量在化工环保工程设计中的基本含义是，在设备（容器、管道、法兰、阀门及泵等）正常寿命期，因环境介质的腐蚀作用而导致设备失效时的最大允许腐蚀深度。
    private String  腐蚀;     //"1.5/3"
    private String  直径;     //原始字段NOMINAL_DIA，  计费使用另外字段V_PIPELINE_DIA。
}

