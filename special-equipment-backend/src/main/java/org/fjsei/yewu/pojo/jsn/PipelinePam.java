package org.fjsei.yewu.pojo.jsn;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**本来只是给前端看的：
* */
@SuperBuilder(toBuilder=true)
@Getter
@Setter
@NoArgsConstructor
public class PipelinePam extends DeviceComnPam {
    private String  保装数;     //监察没有字段；
    private String  安全级;    //"未进行全面检验"
    private String  腐施单;    //监察没有字段；
    private String  根数;     //监察没有字段； "1344米" ?
    private Double    总长;    //累计 太长了
    private String  管规;     //"φ219×13/φ168×11/φ114×8.5"  "Φ57×3.5" "见单线图" "Φ168.3×7.11"
    private String  敷设;
    private String  管厚;     //"4.5,4.0"
    private String  管材牌;    //"20（GB/T8163-1999）"  "0Cr18Ni10Ti、1Cr5Mo、20"
    private String  焊口数;    //"55、49、100"
    private String  绝热材;
    private String  绝热厚;    // "管径＞100：120；＜100:50"
    private String  衬料;
    private String  实用时;    //监察没有字段；"8年6个月、2年1个月"  "65个月" "17年5个月"  "2003年3月1日"
    private String  试压;     //监察没有字段    "1.32/0.99/0.6"
    private String  监检号;    //监察没有字段  "TS7110237-2022" "GDJ2020-2020-G1026-2020" "市局授权"
    private String  输送介;

    private String  探伤比;    //就6条数据：   "5%"  "100%"
    private String  保温;     //就3条数据：监察没有数据;   "有" "无" ? ?[]

}

