package org.fjsei.yewu.pojo.jsn;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**本来只是给前端看的：
* */

@SuperBuilder(toBuilder=true)
@Getter
@Setter
@NoArgsConstructor
public class VesselSvp extends DeviceComnSvp {
    private String  结构;
    private String  支座;
    private String  内径;     //语义上弱化成了 非结构化字段： 诸如： "480/600"
    private String  许工作压;       //壳程/管程：1.25/1.85
    private String  许工作温;    //壳程/夹套：-196～50/-20～50
    private String  许工作介;
    private String  设计介;
    private String  壳设压;     //吸气腔/排气腔：0.4/1.4
    private String  壳设温;    //90（工况1）/-46（工况2）
    private String  壳介;
    private Float   管设压;
    private String  管设温;        // '常温'
    private String  管介;
    private String  夹设压;     //-0.1/0.66
    private String  夹设温;    //-40～62.5(上)/95(下)
    private String  筒厚;     //面/底侧:20.0/18.0
    private String  头厚;     //"8.0/10.0"
    private String  衬厚;     //"0.8～2.0"
    private String  夹厚;     //筒体8.0/封头10.0
    private String  人均容;    //AVG_AREA 22条； 治疗舱4.1m3/过渡舱4.31m3
    private String  人均;     //CAPABLIMIT_EVERY 66条； 主舱3.9/副舱5.0
    private String  舱容限;    // '治疗舱12+抢救舱10+过渡舱4'
    private Boolean  有保温;   // '1' '保温层'
    private Float   舱设压;
    private String  加压式;
    private String  规格;
    private Float  车空重;
    private String  罐材内;
    private String  罐材外;
    private String  罐材头;
    private Float  罐设压;
    private Float  罐设温;
    private String  底盘号;     //LKMUM183XCM000063
    private Float  罐厚筒;
    private Float  罐厚头;    //无数据
    private String  罐厚外筒;    // "3.5/3.5,4"
    private String  罐厚外头;    //"7.0/8.0"
    private String  底盘型;
    private Float  时速;
    private Float  时速弯;
    private String  罐容积;    //取值范围大
    private String  人孔位;    //无数据
    private Float  罐外内径;
    private Float  罐外壁厚;
    private Float  盖厚;
    private Float  罐外长;
    private String  盖材料;
    private String  盖形式;
    private String  筒料球;
    private String  壁厚;     //"筒体：2.5，封头：8"
    private String  设规范;    //设计规范有两个字段啊 DESIGN_LAW 替代 DESIGNCRITERION；
    private String  力类别;
    private String  封头型;
    private Boolean  是快开;     //？选择困难：默认自含义所占实际比例反而更小点？。 "21279"条, "是", "11122"条, "否"; ""null空的也有可能代表某些品种没有该参数！
    private String  监检式;    //无数据!
    //是换热面积? "50702",  "容积"} "1202",   "换热面积"}
    private Boolean  是换热;       //缺省="容积"; Boolean必须明确恰当的默认含义，前端只能设置2个状态值=0,1。
    private String  制规范;     //"GB150-1998"
    //常压的类型 R000:
    private String  车辆类;
    private String  车架;
    private String  发动机;
}

