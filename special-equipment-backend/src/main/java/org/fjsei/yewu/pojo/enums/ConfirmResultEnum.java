package org.fjsei.yewu.pojo.enums;

/**
 * 确认结果枚举
 *
 * @author luohq
 * @date 2022/01/30
 */
public enum ConfirmResultEnum {
    NOT_CONFIRM(0, "未确认"),
    CONFIRM_PAY(1, "同意支付"),
    CONFIRM_NOT_PAY(2, "不同意支付");

    private Integer code;
    private String message;

    ConfirmResultEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * code是否相等
     *
     * @param code
     * @return
     */
    public Boolean equalCode(Integer code) {
        for (ConfirmResultEnum curEnum : values()) {
            if (curEnum.getCode().equals(code)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 根据code转换为枚举类
     *
     * @param code
     * @return
     */
    public static ConfirmResultEnum ofCode(String code) {
        for (ConfirmResultEnum curEnum : values()) {
            if (curEnum.getCode().equals(code)) {
                return curEnum;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
