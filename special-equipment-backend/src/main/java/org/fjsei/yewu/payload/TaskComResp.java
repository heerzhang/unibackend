package org.fjsei.yewu.payload;

import lombok.Getter;
import lombok.Setter;
import md.specialEqp.inspect.Task;

/**通用的变更接口应答：给Task领域的。
 * */
@Getter
@Setter
public class TaskComResp {
    /*成功与否
     * */
    //private boolean  ok;
    //删除收费项也走这里： 删除成功warn='' 收费项删除失败='提示消息'
    /**成功=null, 但有提示消息的不代表一定是失败的！
     * 删除的成功与否要以fee==null?作为首要依据。
     * */
    private String  warn;

    /**把最新的对象模型暴露给接口，前端按需领取吧。前端不做内省就不会带来负载的，那样的话自然就不影响性能。
     * */
    private Task  task;

    /**第二条任务： 划转给这个任务的。
     * */
    private Task  newTask;
}

