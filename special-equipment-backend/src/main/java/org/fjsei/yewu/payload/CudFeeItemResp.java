package org.fjsei.yewu.payload;

import lombok.Getter;
import lombok.Setter;
import md.specialEqp.fee.Charging;
import md.specialEqp.inspect.Detail;

/**添加修改收费项，后端反馈给前端的信息：
 * */
@Getter
@Setter
public class CudFeeItemResp {
    //删除收费项也走这里： 删除成功warn='' 收费项删除失败='提示消息'
    /**成功=null, 但有提示消息的不代表一定是失败的！
     * 删除的成功与否要以fee==null?作为首要依据。
     * */
    private String warn;
    /**增加或修改的那一条 实体记录
     * 删除成功的直接给null，注意！若删除时不成功的保留fee=之前的状态 fee!=null;
     * */
    private Charging fee;
    //返回最新的Detail, 前端采用内省方式可以直接获取最新数据，假如前端没有内省该属性字段的也没什么损失。
    //该字段 不做强制要求；Detail有提供的就可以让前端省点事，免去手动更新。
    /**允许给前端留个路：直接更新；
     * 但如果前端没有做 内省，那么graphQL中间件这一层也不会提取数据，当然也没有消耗性能。
     * */
    private Detail  bus;
}
