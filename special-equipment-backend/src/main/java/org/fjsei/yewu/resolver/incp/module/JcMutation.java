package org.fjsei.yewu.resolver.incp.module;

import org.fjsei.yewu.entity.incp.JcAuthor;
import org.fjsei.yewu.entity.incp.JcAuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;


//实际相当于controller;

@Component
public class JcMutation  {
    @Autowired
    private JcAuthorRepository authorRepository;
    @PersistenceContext(unitName = "entityManagerFactoryIncp")
    private EntityManager emIncp;

/**
 *不是主数据库的其他数据库：需要+ @Transactional 再配套+ emIncp.joinTransaction() 才能真的保存成功！
 * */

    //@Transactional
    public JcAuthor newJcAuthor(String nickname) {
/*
        if(!emIncp.isJoinedToTransaction())      System.out.println("没达到 emIncp.isJoinedToTransaction()");
        else System.out.println("到 emIncp.isJoinedToTransaction()");
        if(!emIncp.isJoinedToTransaction())      emIncp.joinTransaction();
        Assert.isTrue(emIncp.isJoinedToTransaction(),"没emIncpisJoinedToTransaction");
*/

        JcAuthor author = new JcAuthor();
        author.setNickname(nickname);
        authorRepository.save(author);
        return author;
    }

}
