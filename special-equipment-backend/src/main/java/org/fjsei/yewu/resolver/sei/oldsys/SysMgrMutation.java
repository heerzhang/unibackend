package org.fjsei.yewu.resolver.sei.oldsys;

import md.cm.unit.Units;
import md.computer.FileRepository;
import md.specialEqp.Equipments;
import md.specialEqp.ReportRepository;
import md.specialEqp.inspect.IspRepository;
import md.specialEqp.inspect.TaskRepository;
import md.system.AuthorityRepository;
import md.system.UserRepository;
import org.fjsei.yewu.security.JwtUserDetailsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;


@Component
public class SysMgrMutation {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;
    @Autowired
    private Equipments eQPRepository;
    @Autowired
    private IspRepository iSPRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private ReportRepository reportRepository;
    @Autowired
    private Units units;
    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private FileRepository fileRepository;


    @PersistenceContext(unitName = "entityManagerFactorySei")
    private EntityManager emSei;                //EntityManager相当于hibernate.Session：

//    @Autowired
//    private final JwtTokenUtil jwtTokenUtil=new JwtTokenUtil();


}
