package org.fjsei.yewu.resolver.sei.fee;

import md.cm.flow.ApprovalStmRepository;
import md.cm.unit.Unit;
import md.cm.unit.Units;
import md.specialEqp.BusinessCat_Enum;
import md.specialEqp.Equipments;
import md.specialEqp.ReportRepository;
import md.specialEqp.fee.Charging;
import md.specialEqp.fee.ChargingRepository;
import md.specialEqp.inspect.*;
import md.specialEqp.type.*;
import md.system.AuthorityRepository;
import md.system.Ifop_Enu;
import md.system.UserRepository;
import org.fjsei.yewu.input.FeeItemInput;
import org.fjsei.yewu.payload.CalcFeeResp;
import org.fjsei.yewu.payload.CudFeeItemResp;
import org.fjsei.yewu.resolver.Comngrql;
import org.fjsei.yewu.security.JwtUserDetailsService;
import org.fjsei.yewu.util.Tool;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static md.specialEqp.BusinessCat_Enum.*;

/**收费 发票相关的。 MutationResolver不同于QueryResolver;变更是事务同步，查询是并发异步。
 *把 @Component 改成 @Controller之后问题：chargeCalc容器((Vessel) isp.getDev(),类型转换报错！
 */
@Controller
public class FeeMgrMutation extends Comngrql {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;
    @Autowired
    private Equipments eQPRepository;
    @Autowired
    private IspRepository iSPRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private ReportRepository reportRepository;
    @Autowired
    private Units units;
    @Autowired
    private AuthorityRepository authorityRepository;
    @Autowired private ApprovalStmRepository approvalStmRepository;
    @Autowired private ChargingRepository chargingRepository;
    @Autowired private DetailRepository detailRepository;

    @PersistenceContext(unitName = "entityManagerFactorySei")
    private EntityManager emSei;
    static final int [ ][ ] 容器设计收费={{20,50,100,200,350,500},{30,60,100,150,200,300},{40,60,75,150,200,250},{30,50,60,100,120,200}};
    static final int [ ][ ] 锅炉设计收费={{20,30,45,60,80,100,120,150},{35,45,60,75,100,130,150,200},{200,400,500,600,800,1000,1300,1800},
            {80,160,300,500,650,800,1000,1500},{40,80,150,180,200,300,500,1000},{30,50,100,150,180,250,400,800},
            {600,900,1200,1400,1700,2100,2700,3200}};
    static final Map<String, Integer> 金相试验收费= Arrays.stream(new Object[][]{
            {"L110", 50},{"L120", 50},{"L131", 30},{"L132", 40},{"L133", 100},{"L134", 40},{"L135", 50},
            {"L210", 15},{"L220", 20},{"L300", 120},{"L400", 120},{"L500", 300},{"L600", 400}
    }).collect(Collectors.toMap(kv -> (String) kv[0], kv -> (Integer) kv[1]));
    //有小数点的62.5会默认认定是 double,报错。舍弃首字母:"TOFD检测"  FEE_COD":"WA00"对应这里的{"A00", 2400}
    static final Map<String, Float> 无损检测收费= Arrays.stream(new Object[][]{
            {"110", 60},{"120", 55},{"210", 50},{"211", 62.5f},{"212", 75},{"220", 70},{"230", 120},{"310", 45},
            {"311", 90},{"320", 60},{"321", 120},{"330", 30},{"331", 60},{"410", 60},{"420", 80},{"431", 20},
            {"432", 30},{"433", 40},{"434", 50},{"435", 60},{"500", 60},{"611", 30},{"612", 50},
            {"621", 60},{"622", 80},{"700", 5},{"800", 8},{"910", 15},{"920", 30},{"A00", 2400}
    }).collect(Collectors.toMap(kv -> (String) kv[0],  kv ->{
        //必须分别转换，报错！ 不然的话,需统一都加 f结尾，一起都转浮点。
        return  kv[1] instanceof Integer ?   ((Integer) kv[1]).floatValue() : (Float) kv[1];
    } ) );
    //重新计算Isp收费：自动计算部分规则选择参数，后端已经设置的参数，前端要求强制插入条件选择参数，允许当即修改的参数。除了人工选择部分规则。
    //前后端协调：参数认定来源，采信谁；参数选择搞定后，自动计算，人工选择部分规则可允许在前面就加，或者自动计算后面再加,再一次确认最终金额。显示收费依据明细。
    @MutationMapping
    @Transactional
    public CalcFeeResp reckonIspFee(@Argument String id)
    {
        Detail bus = entityOf(id,Detail.class);
        Assert.notNull(bus,"未找到Detail:"+id);
        CalcFeeResp calcFeeResp=new CalcFeeResp();   //同时作为输出的+Context用途：
        //首先删除旧的非人工选择的收费项目。 管道收费特别：关联单元详细列表一起要删除。
        //改成“,orphanRemoval=true)”以后会报错：更新该字段时不能使用setFees()，需要使用List类的clear()或者addAll方法进行更新！
        List<Charging>  oldfees=bus.getFees().stream().filter(charging->{
            if(charging.getManual())    return true;
            else {
                chargingRepository.delete(charging);      //关联charging.pipus单元详细列表@ManyToMany可同时自动删除
                return false;
            }
        }).collect(Collectors.toList());
        bus.getFees().clear();
        bus.getFees().addAll(oldfees);      //会报错bus.setFees( );
        Isp isp =bus.getIsp();
        BusinessCat_Enum bstp=bus.getTask().getBsType();   //虽然isp.getBsType()也有，但避免用，Isp是长期状态字段，优先用短期状态字段。
        String  type,  sort;
        if(null!=isp.getDev()) {        //==null的水质其它报告？
            type = isp.getDev().getType();   //绝对不为空的，type<>""的。
            sort = isp.getDev().getSort();
            String vart = isp.getDev().getVart();
            String subv = isp.getDev().getSubv();       //可能=null
            //把 @Component 改成 @Controller之后才有的Cast error。
            Object unproxyEnt = Hibernate.unproxy(isp.getDev());        //Hibernate代理类 instanceof Elevator
            if ("4".equals(type)) chargeCalc起重((Crane) unproxyEnt, bus, bstp, sort, vart, calcFeeResp);
            else if ("3".equals(type)) chargeCalc电梯((Elevator) unproxyEnt, bus, bstp, sort, vart, calcFeeResp);
            else if ("5".equals(type))  chargeCalc厂车((FactoryVehicle) unproxyEnt, bus, bstp, sort, vart, subv, calcFeeResp);
            else if ("6".equals(type)) chargeCalc游乐((Amusement) unproxyEnt, bus, bstp, sort, vart, subv, calcFeeResp);
            else if ("1".equals(type)) chargeCalc锅炉((Boiler) unproxyEnt, bus, bstp, sort, vart, subv, calcFeeResp);
            else if ("2".equals(type)) chargeCalc容器((Vessel) unproxyEnt, bus, bstp, sort, vart, subv, calcFeeResp);
            else if ("8".equals(type)) chargeCalc管道((Pipeline) unproxyEnt, bus, bstp, sort, vart, calcFeeResp);
            else if ("R".equals(type)) chargeCalc常压容器((Vessel) unproxyEnt, bus, bstp, sort, vart, calcFeeResp);
        }
        else{       //单独报告，没有设备台账的业务。
            type =bus.getType();
            sort="";
            if ("F".equals(type))   chargeCalc安全阀(bus, bstp, calcFeeResp);
            else if ("Z".equals(type))  chargeCalc水质(bus, bstp, calcFeeResp);
            else if ("7".equals(type))  chargeCalc管道元件(bus, bstp, calcFeeResp);
        }

        //todo:"单项管道检验总长度超过10公里，不足30公里，收费标准下浮10%","单项管道检验总长度超过30公里，收费标准下浮25%" ?放在Task里面多个Isp
        //Task归并派工限制：同一个Task管道检验业务若有多个Detail那么必须都是管道，不可能管道和锅炉/容器什么的合并在同一个Task中。

        chargeCalc所有业务设备公用(isp, calcFeeResp);
        calcFeeResp.setBus(bus);
        chargeCalcSumMod5Floor(bstp,type,sort,calcFeeResp);         //收费最终结果计算： 包含人工选择部分。
        if(calcFeeResp.getParms().size()>0)   //代表自动计算需要的参数有些无效，不能保证完整性，只好作废。
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        else  calcFeeResp.getFees().forEach(a -> {
                a.setDetail(bus);
                //管道收费特别：关联列表的！
                chargingRepository.save(a);
                //必须加上这条：否则本该？总共3条收费项，graphQL却仅返回一条给前端?直接依赖内省机制取得返回的isp.bus.fees[]都是旧的；Transactional还未结束呢。只能重新走Query查询。
                bus.getFees().add(a);    //关联，立刻更新！
            });
        bus.setFeeOk(false);
        //【手动添加项目】：【人工选择-分开】+ %@[加收减免条款] @所有业务 所有设备的：手选部分；+ @特别定制条款： 代码修改"B107"
        //fee_mod1 全部是自动计算的，允许没有mod1收费项目，允许多于1条的mod1收费项”
        //?"电站锅炉水汽质量检验时，如果两台同时进行检验的，按收费总额的90%收取" "电站锅炉水汽质量检验时，三台及以上同时进行检验的，按收费总额的80%收取","EXTRAFEE_ID":"98",？改成人工修正;
        //Task归并派工限制：同一个Task电站锅炉水汽质量检验业务若有多个Detail那么必须都是电站锅炉水汽质量检验，不可能和其它业务设备的合并在同一个Task中。
        //'1'=type && subv="1002": "电站锅炉" && OPE_TYPE":"11"; 所有Detail同时都满足才算的。
        return calcFeeResp;
    }

/**起重机械 自动计算收费; fee_mod=1,8,9等部分
 * */
    private void chargeCalc起重(Crane eqp, Detail bus, BusinessCat_Enum bstp, String sort, String vart, CalcFeeResp rsp)
    {
        Float gn=eqp.getRtlf();   //基本分叉参数：CHAENGLOAMAIN =rtlnf =gn
        if( (!"48".equals(sort) && !"4D".equals(sort)) && (null==gn || gn<1) )
            rsp.getParms().add("rtlf");    //各种类型起重机：最明显的参数设置异常排除！ gn==mod1最基础的分叉变量 ??每类别起重不一样的;
        double eqcost=0;      //工程造价 OPE_TYPE":"2,14,15"
        if(INSTA.equals(bstp) || REFORM.equals(bstp) || REPAIR.equals(bstp) )    //OPE_TYPE":"2,14,15"
             eqcost=rsp.校验造价(eqp,bus,2000,1000);
        //【第1大块】基础收费fee_mod=1
        if(REGUL.equals(bstp)){     //OPE_TYPE":"3"｛ 定期检验=3;
            if("417".equals(vart) && null!=gn){   //电动单梁（悬挂）起重机定期检验" FEE_COD":"4110"
                    if(gn<5)    rsp.useBase("4111", 500);    //"3≤Gn＜5" FEE_COD":"4111"
                    else if(gn<10)  rsp.useBase("4112", 550);    //"5≤Gn＜10" ,2 , RS_EXPR":"550"
                    else if(gn<16)  rsp.useBase("4113", 600);
                    else if(gn<25)  rsp.useBase("4114", 650);
                    else rsp.useBase("4115", 700);
            }
            else if("427".equals(vart) && null!=gn){  //电动葫芦门式起重机定期检验" FEE_COD":"4180"
                if(gn<5)    rsp.useBase("4181", 420);
                else if(gn<10)  rsp.useBase("4182", 455);
                else if(gn<16)  rsp.useBase("4183", 560);
                else if(gn<25)  rsp.useBase("4184", 630);
                else if(gn<40)  rsp.useBase("4185", 700);
                else if(gn<60)  rsp.useBase("4186", 770);
                else rsp.useBase("4187", 840);
            }
            else if(("41".equals(sort) || "42".equals(sort) || "49".equals(sort) || "445".equals(vart) )
                    && !"417".equals(vart) && !"427".equals(vart) && null!=gn){   //"桥门式、铁路、缆索起重机定期检验" FEE_COD":"4120"
                if(gn<5)    rsp.useBase("4131", 400);
                else if(gn<10)  rsp.useBase("4132", 450);
                else if(gn<16)  rsp.useBase("4133", 500);
                else if(gn<25)  rsp.useBase("4134", 550);
                else rsp.useBase("4135", 600);
            }
            else if("48".equals(sort))   //升降机定期检验" FEE_COD":"4140"
                rsp.useBase("4140", 600);
            else if("4D".equals(sort)) {  //机械式停车设备定期检验" FEE_COD":"4170"
                if(null==eqp.getPnum() )    rsp.getParms().add("pnum");     //可认定为非法参数值
                else if(eqp.getPnum()<20)   rsp.addFeeItem("4171", (double) eqp.getPnum()*160,1, (double) eqp.getPnum());
                else {  //RS_EXPR":"(BERNUM-19)*80+3040"
                    rsp.addFeeItem("4172", (double) (3040 + (eqp.getPnum() - 19) * 80), 1, (double) (eqp.getPnum() - 19));
                }
            }
            else if("44".equals(sort) && !"445".equals(vart) ){  //流动式起重机定期检验" FEE_COD":"4210"
                rsp.useBase("4210", 750);
            }
            else if("47".equals(sort) ){  //门座式起重机定期检验" FEE_COD":"4220"
                rsp.useBase("4220", 1000);
            }
            else  if("4A".equals(sort) ){  //桅杆式起重机定期检验" FEE_COD":"4240"
                rsp.useBase("4240", 600);
            }
            else if("43".equals(sort) ){  //塔式起重机定期检验" FEE_COD":"4250"
                rsp.useBase("4250", 900);
            }
        }
        if(FIRST.equals(bstp)){    //OPE_TYPE":"16"｛ 首检 不需要 安装监检:"2"
            if(("41".equals(sort) || "42".equals(sort) || "49".equals(sort) || "445".equals(vart) )
                    && !"417".equals(vart) && !"427".equals(vart) && null!=gn ){     //"桥门式、铁路起重机首检" FEE_COD":"4410"
                if(gn<5)     rsp.useBase("4411", 780);
                else if(gn>=5 && gn<10)      rsp.useBase("4412", 845);
                else if(gn>=10 && gn<16)     rsp.useBase("4413", 1040);
                else if(gn>=16 && gn<25)     rsp.useBase("4414", 1170);
                else if(gn>=25 && gn<40)     rsp.useBase("4415", 1300);
                else if(gn>=40 && gn<60)     rsp.useBase("4416", 1430);
                else    rsp.useBase("4417", 1560);
            }
            else if("487".equals(vart)){     //升降机首检" FEE_COD":"4420"
                rsp.useBase("4420", 780);
            }
            else if("417".equals(vart) && null!=gn){     //电动单梁（悬挂）起重机首次检验" FEE_COD":"4430"
                if(gn<5)     rsp.useBase("4431", 650);
                else if(gn>=5 && gn<10)      rsp.useBase("4432", 715);
                else if(gn>=10 && gn<16)     rsp.useBase("4433", 780);
                else if(gn>=16 && gn<25)     rsp.useBase("4434", 845);
                else   rsp.useBase("4435", 910);
            }
            else if("441".equals(vart) || "444".equals(vart)){     //流动式起重机首检" FEE_COD":"4510"
                rsp.useBase("4510", 975);
            }
            else if("471".equals(vart) || "476".equals(vart)){     //门座式起重机首检" FEE_COD":"4520"
                rsp.useBase("4520", 1300);
            }
            else if("4A".equals(sort) ){     //桅杆式起重机首检" "FEE_COD":"4530"
                rsp.useBase("4530", 780);
            }
            else if("442".equals(vart) ){     //履带起重机首检" FEE_COD":"4540"
                rsp.useBase("4540", 1125);
            }
        }
        if(INSTA.equals(bstp) || REFORM.equals(bstp) || REPAIR.equals(bstp) ){    //OPE_TYPE":"2,14,15"｛
            if("417".equals(vart) && null!=gn && eqcost>0) {
                if (gn <= 5) {   //"5吨（含）以下的电动单梁（悬挂）起重机安装、改造、修理过程监督检验" FEE_COD":"4370"
                    if (eqcost <= 500000) {     //"按X（起重机械单台工程造价）的2.0%收取，但不低于1000元/台"
                        rsp.useBase("4371", eqcost * 0.02,1000);
                    } else if (eqcost <= 2000000) rsp.useBase("4372", eqcost * 0.015);
                    else if (eqcost <= 5000000) rsp.useBase("4373", eqcost * 0.01);
                    else rsp.useBase("4374", eqcost * 0.007);
                } else {   //"5吨（不含）以上的电动单梁（悬挂）起重机安装、改造、修理过程监督检验" FEE_COD":"4390"
                    if (eqcost <= 500000) {  //但不低于1500元/台
                        rsp.useBase("4391", eqcost * 0.02,1500);
                    } else if (eqcost <= 2000000) rsp.useBase("4392", eqcost * 0.015);
                    else if (eqcost <= 5000000) rsp.useBase("4393", eqcost * 0.01);
                    else rsp.useBase("4394", eqcost * 0.007);
                }
            }
            else if("4D".equals(sort)) {  //只有2个车位的机械式停车设备安装、改造、修理过程监督检验" FEE_COD":"4360"
                if(null==eqp.getPnum() )    rsp.getParms().add("pnum");
                else if(eqp.getPnum()<=2)  {
                    if (eqcost <= 500000)   rsp.useBase("4361", eqcost * 0.02);
                    else if (eqcost <= 2000000) rsp.useBase("4362", eqcost * 0.015);
                    else if (eqcost <= 5000000) rsp.useBase("4363", eqcost * 0.01);
                    else rsp.useBase("4364", eqcost * 0.007);
                }
                else {      //"2个（不含）车位以上的机械式停车设备安装、改造、修理过程监督检验" FEE_COD":"4380"
                    if (eqcost <= 500000) {   //但不低于1500元/台
                        rsp.useBase("4381", eqcost * 0.02,1500);
                    } else if (eqcost <= 2000000) rsp.useBase("4382", eqcost * 0.015);
                    else if (eqcost <= 5000000) rsp.useBase("4383", eqcost * 0.01);
                    else rsp.useBase("4384", eqcost * 0.007);
                }
            }
        }
        if(REFORM.equals(bstp) || REPAIR.equals(bstp) ){    //OPE_TYPE":"14,15"｛
            if(!"417".equals(vart) && !"4D".equals(sort) ) {  //起重机械改造、修理过程监督检验" FEE_COD":"4320"
                if (eqcost <= 500000) {  //但不低于1500元/台
                    rsp.useBase("4321", eqcost * 0.02,1500);
                } else if (eqcost <= 2000000) rsp.useBase("4322", eqcost * 0.015);
                else if (eqcost <= 5000000) rsp.useBase("4323", eqcost * 0.01);
                else rsp.useBase("4324", eqcost * 0.007);
            }
        }
        if(INSTA.equals(bstp) ){    //OPE_TYPE":"2"｛
            //因为OPE_TYPE":"2,14,15"包含了OPE_TYPE":"2":所以eqcost=rsp.校验造价()放在里面就可能跑两趟。
            if(!"417".equals(vart) && !"4D".equals(sort) ) {  //起重机械安装过程监督检验 FEE_COD":"4310"
                if (eqcost <= 500000) {  //但不低于1500元/台
                    rsp.useBase("4311", eqcost * 0.02,1500);
                } else if (eqcost <= 2000000) rsp.useBase("4312", eqcost * 0.015);
                else if (eqcost <= 5000000) rsp.useBase("4313", eqcost * 0.01);
                else rsp.useBase("4314", eqcost * 0.007);
            }
        }
        if(OTHER.equals(bstp) ){    //OPE_TYPE":"13"｛目前"13"实际都是=安全监控管理系统 检验？ ｝
            if(Boolean.TRUE.equals(bus.isCheap() )) {  //起重机械安全监控管理系统定期（首次）检验" FEE_COD":"4610"
                rsp.useBase("4610", 600);
            }else {     //起重机械安全监控管理系统监督检验" "FEE_COD":"4620"  @改成单独的Isp/task任务独立做派工收费模式：OPE_TYPE":"13"，单独计费，可关联同一张发票
                rsp.useBase("4620", 1500);
            }
        }
        if(SAFETYINS.equals(bstp) ){    //OPE_TYPE":"4"; {}
            if(null==eqp.getMoney() || eqp.getMoney()<5000)  rsp.getParms().add("money");
            else {   //进口起重机械安全性能监督检验" FEE_COD":"4330"
                rsp.useBase("4330", eqp.getMoney() * 0.009);
            }
        }
        Double mod1fee =rsp.addUpMod1fee();    //合计fee_mod 1:
        //【第2大块】进入加收收费fee_mod=8：还需提供mod1的金额
        if(REGUL.equals(bstp) || FIRST.equals(bstp)){    //OPE_TYPE":"3,16" ,
            if(3==eqp.getImpt()){  //EXTRAFEE_ID":"33" IMPORT_TYPE==整机进口",RS_EXPR":"(srcPrice*20)/100"
                rsp.addFeeItem("A33", mod1fee*0.2 ,8);
            }
            if("413".equals(vart)||"422".equals(vart)){  //EXTRAFEE_ID":"26",
                rsp.addFeeItem("A26", mod1fee ,8);
            }
            if("415".equals(vart)){  //EXTRAFEE_ID":"65",
                rsp.addFeeItem("A65", mod1fee*0.3 ,8);
            }
            if("417".equals(vart) && gn>40){  //EXTRAFEE_ID":"16",EQP_VART==4170&&CHAENGLOAMAIN>=40.0",RS_EXPR":"100",RS_TIME":"(CHAENGLOAMAIN-40)/10",
                int times=(int)Math.ceil((gn-40)/10.0);
                rsp.addFeeItem("A16", (double) (100*times),8,(double)times);
            }
            if("43".equals(sort) && Boolean.TRUE.equals(eqp.getWalk()) ){  //EXTRAFEE_ID":"41",&&IF_XZS==是", RS_EXPR":"(srcPrice*20)/100",
                rsp.addFeeItem("A41", mod1fee*0.2 ,8);
            }
            if("43".equals(sort) ) {  //EXTRAFEE_ID":"40",&&ELEHEIGHTMAIN>10.0", RS_EXPR":"(srcPrice*10)/100",  RS_TIME":"(ELEHEIGHTMAIN-10)/2",
                if(null==eqp.getHlfm() || eqp.getHlfm()<2)
                    rsp.getParms().add("hlfm");     //可认定为非法参数值
                else if(eqp.getHlfm()>10) {
                    int times = (int) Math.ceil((eqp.getHlfm() - 10) / 2.0);   //17.5=18;  7.2也是当做=8;
                    rsp.addFeeItem("A40", mod1fee * 0.1 * times, 8, (double) times);
                }
            }
            if("48".equals(sort) && Boolean.TRUE.equals(eqp.getTwoc()) )
            {  //EXTRAFEE_ID":"32",&&IF_TWO_HOIS==是",RS_EXPR":"(srcPrice*30)/100"
                rsp.addFeeItem("A32", mod1fee*0.3 ,8);
            }
            if("48".equals(sort) ){  //EXTRAFEE_ID":"31",&&UP_HIGH>20.0", RS_EXPR":"50",RS_TIME":"(UP_HIGH-20)/3",
                if(null==eqp.getHlf() || eqp.getHlf()<3)
                    rsp.getParms().add("hlf");
                else if(eqp.getHlf()>20) {
                    int times = (int) Math.ceil((eqp.getHlf() - 20) / 3);
                    rsp.addFeeItem("A31", (double) (50 * times), 8, (double) times);
                }
            }
            if(("43".equals(sort)||"44".equals(sort)||"47".equals(sort)||"4A".equals(sort)) && !"445".equals(vart)){
                //EQP_SORT==4300||EQP_SORT==4400||EQP_SORT==4700||EQP_SORT==4A00<&&>EQP_VART!=4450
                Float  maxmo=Tool.castFloat(eqp.getMom());     //检查明显不正常的配置参数！
                if(null==maxmo || maxmo<1)    rsp.getParms().add("mom");
                else if(maxmo>50 && maxmo<=10000){
                    //EXTRAFEE_ID":"34" &CHAADVMOM>50.0&&CHAADVMOM<=10000", RS_EXPR":"(srcPrice*5)/100",RS_TIME":"(CHAADVMOM-50)/20",
                    int times=(int)Math.ceil((maxmo-50)/20);
                    rsp.addFeeItem("A34", mod1fee*0.05*times ,8, (double)times);
                }else if(maxmo>10000){
                    //EXTRAFEE_ID":"121"+"122",&&CHAADVMOM>10000", RS_EXPR":"(srcPrice*2.5)/100",RS_TIME":"(CHAADVMOM-10000)/50", ?合并@
                    Double 固定基准=mod1fee*0.05*497.5;      //前端解释容易推算：mod1fee明显的；mod1fee*24.875="50吨米至10000吨米的部分，每20吨米加收5%"
                    int times=(int)Math.ceil((maxmo-10000)/50);      //特例：amount !== 项目标准/单位 * mnum;  公倍数; mnum指出了大于10000吨米的那一部分
                    rsp.addFeeItem("A121", 固定基准 + mod1fee*0.025*times ,8, (double)times);
                }
                if(Boolean.TRUE.equals(eqp.getCotr() )){   //EXTRAFEE_ID":"37", IF_CONTAIN_H==是", RS_EXPR":"(srcPrice*10)/100",
                    rsp.addFeeItem("A37", mod1fee*0.1 ,8);
                }
                if(Boolean.TRUE.equals(eqp.getGrab() )){  //EXTRAFEE_ID":"35",&IF_GRAB_B==是", RS_EXPR":"(srcPrice*10)/100",
                    rsp.addFeeItem("A35", mod1fee*0.1 ,8);
                }
                if(Boolean.TRUE.equals(eqp.getAuxh() )){
                    //EXTRAFEE_ID":"38",&IF_VICE_LOAD==是", RS_EXPR":"(srcPrice*20)/100",
                    rsp.addFeeItem("A38", mod1fee*0.2 ,8);
                }
                if(Boolean.TRUE.equals(eqp.getWjib() ))
                {  //EXTRAFEE_ID":"39",&IF_VICE_ARM==是", RS_EXPR":"(srcPrice*20)/100",
                    rsp.addFeeItem("A39", mod1fee*0.2 ,8);
                }
                if(Boolean.TRUE.equals(eqp.getSuck() )){    //EXTRAFEE_ID":"36",&IF_SUCTORIAL==是", RS_EXPR":"(srcPrice*10)/100",
                    rsp.addFeeItem("A36", mod1fee*0.1 ,8);
                }
            }
            //EQP_SORT==4100||EQP_SORT==4200||EQP_SORT==4900||EQP_VART==4450
            if("41".equals(sort)||"42".equals(sort)||"49".equals(sort)||"445".equals(vart)){
                //EQP_SORT==4100||EQP_SORT==4200||EQP_SORT==4900||EQP_VART==4450<&&>EQP_VART!=4170
                if(!"417".equals(vart) && gn>100){
                    //EXTRAFEE_ID":"17", &&CHAENGLOAMAIN>=100.0", RS_EXPR":"100",  RS_TIME":"(CHAENGLOAMAIN-100)/10",
                    int times=(int)Math.ceil((gn-100)/10.0);
                    rsp.addFeeItem("A17", (double) (100*times),8, (double)times);   //amount= 项目标准/单位100 * mnum;
                }
                if(Boolean.TRUE.equals(eqp.getTwo() )){  //EXTRAFEE_ID":"19",<&&>IF_TWO_CAB==是",RS_EXPR":"(srcPrice*10)/100",
                    rsp.addFeeItem("A19", mod1fee*0.1 ,8);
                }
                if(Boolean.TRUE.equals(eqp.getTwoc() )){  //EXTRAFEE_ID":"18",&&>IF_TWO_LCAR==是",RS_EXPR":"(srcPrice*10)/100",
                    rsp.addFeeItem("A18", mod1fee*0.1 ,8);
                }
                if(null==eqp.getHlfm() || eqp.getHlfm()<1)
                    rsp.getParms().add("hlfm");
                else if(eqp.getHlfm()>6){  //EXTRAFEE_ID":"25" &&>ELEHEIGHTMAIN>6.0",RS_EXPR":"(srcPrice*5)/100",RS_TIME":"(ELEHEIGHTMAIN-6)/3",
                    int times=(int)Math.ceil( (eqp.getHlfm()-6)/3);
                    rsp.addFeeItem("A25", mod1fee*0.05*times ,8, (double)times);
                }
                if(Boolean.TRUE.equals(eqp.getNnor() )) {  //"EXTRAFEE_ID":"28",&&>QZ_IF_UNNORMAL==是",RS_EXPR":"(srcPrice*20)/100"
                    rsp.addFeeItem("A28", mod1fee*0.2 ,8);
                }
                if(Boolean.TRUE.equals(eqp.getGrab() )) {  //EXTRAFEE_ID":"21",&&>IF_GRAB_B==是", RS_EXPR":"(srcPrice*10)/100",
                    rsp.addFeeItem("A21", mod1fee*0.1 ,8);
                }
                if(Boolean.TRUE.equals(eqp.getSuck() )) {  //EXTRAFEE_ID":"22",&&>IF_SUCTORIAL==是", RS_EXPR":"(srcPrice*10)/100",
                    rsp.addFeeItem("A22", mod1fee*0.1 ,8);
                }
                if(null==eqp.getSpan() || eqp.getSpan()<1)
                    rsp.getParms().add("span");    //可能？某些种类起重根本没有关注: 跨度这个概念?
                else if(eqp.getSpan()>22.5){  //EXTRAFEE_ID":"24",&&>SPAN>=22.5", RS_EXPR":"(srcPrice*10)/100",  RS_TIME":"((2*SPAN)-45)/12",
                    int times=(int)Math.ceil( (eqp.getSpan()-22.5)/6);
                    rsp.addFeeItem("A24", mod1fee*0.1*times ,8, (double)times);
                }
                if(Boolean.TRUE.equals(eqp.getCotr() )) {  //EXTRAFEE_ID":"23",&&>IF_CONTAIN_H==是", RS_EXPR":"(srcPrice*10)/100",
                    rsp.addFeeItem("A23", mod1fee*0.1 ,8);
                }
                if(Boolean.TRUE.equals(eqp.getAuxh() )) {  //EXTRAFEE_ID":"20",&&>IF_VICE_LOAD==是", RS_EXPR":"(srcPrice*10)/100",
                    rsp.addFeeItem("A20", mod1fee*0.1 ,8);
                }
            }
        }
        if(REGUL.equals(bstp) ){  //OPE_TYPE":"3"
            if("428".equals(vart)||"429".equals(vart)||"424".equals(vart)||"426".equals(vart)){
                //EXTRAFEE_ID":"30", EQP_VART==4280||EQP_VART==4290||EQP_VART==4240||EQP_VART==4260",RS_EXPR":"(srcPrice*80)/100",
                rsp.addFeeItem("A30", mod1fee*0.8 ,8);
            }
        }
        if(FIRST.equals(bstp) ){
            if("423".equals(vart)||"425".equals(vart)||"428".equals(vart)){
              //EXTRAFEE_ID":"29" EQP_VART==4230||EQP_VART==4250||EQP_VART==4280", RS_EXPR":"(srcPrice*150)/100",
                rsp.addFeeItem("A29", mod1fee*1.5 ,8);
            }
        }
        if(INSTA.equals(bstp) || REFORM.equals(bstp) || REPAIR.equals(bstp) || FIRST.equals(bstp)){    //OPE_TYPE":"2,14,15,16" ；
            //Detail bus：里面的参数针对每一次Isp检验的，前端:都要要求确认输入。
            if(Boolean.TRUE.equals(bus.isTest() )) {  //EXTRAFEE_ID":"94", IF_HOLD_TEST==是", RS_EXPR":"300", 询问检验情况：？参数附加
                rsp.addFeeItem("A94", (double)300,8);
            }
        }
       //【第3大块】进入加收减免条款fee_mod=9的自动计算部分： mod=9是乘数计算关系; mod9也可能自动计算非选择。
        if(INSTA.equals(bstp) || REFORM.equals(bstp) || REPAIR.equals(bstp) || FIRST.equals(bstp)){  //OPE_TYPE":"2,14,15,16" ；
            if("47".equals(sort) && Boolean.TRUE.equals(eqp.getWhole()) ){  //EXTRAFEE_ID":"127","IF_ZJCC==是&&EQP_SORT==4700", RS_EXPR":"-(tmpCalPrice*50)/100",
                rsp.addFeeItem("B127", 0.5,9);      //@不是负数，mod9_折扣=系数的乘数;
            }
        }
        //【人工选择-分开前端搞】# "理化、金相试验收费标准"   # "无损检测和其他检测项目收费标准（现场检测）"
    }

    private void chargeCalc所有业务设备公用(Isp isp, CalcFeeResp rsp)
    {
        Unit objUnit;
        if(null!=isp.getDev()){
            objUnit=isp.getDev().getUseu();
            //使用单位 eqp.useu;
        }else{
            //服务对象单位 isp.servu可能不等于使用单位;
            objUnit=isp.getServu();
        }
        Assert.isTrue(null!=objUnit,"单位空");
        //-@可自动计算的： @所有业务 所有设备的：
        String ic=objUnit.getIndCod();
        //"INDUSTRY_PROP_COD==O82||INDUSTRY_PROP_COD==O821||INDUSTRY_PROP_COD==O822||INDUSTRY_PROP_COD==821||INDUSTRY_PROP_COD==822||INDUSTRY_PROP_COD==826||INDUSTRY_PROP_COD==827||INDUSTRY_PROP_COD==828",
        if("O82".equals(ic) || "O821".equals(ic) || "O822".equals(ic) || "821".equals(ic) || "822".equals(ic)
                || "826".equals(ic) || "827".equals(ic) || "828".equals(ic) )  {
            rsp.addFeeItem("B49", 0.5,9);   //EXTRAFEE_ID":"49", RS_EXPR":"-(tmpCalPrice*50)/100",
        }
        if(isp.getBus().getRecnt()>0) {     //复检业务
            if (Boolean.TRUE.equals(isp.getBus().getNsite()))   //EXTRAFEE_ID":"91",复检无需现场复检确认; RS_EXPR":"-tmpCalPrice"
                rsp.addFeeItem("B91", (double) 0,9);
            else if (isp.getBus().getRecnt() > 1)   //EXTRAFEE_ID":"93",   ?%复检>=2次数
                rsp.addFeeItem("B93", 0.6,9);
            else        //EXTRAFEE_ID":"92",  需要到现场进行复检确认的
                rsp.addFeeItem("B92", 0.3,9);
        }
    }
    /**电梯 自动计算收费;
     * */
    private void chargeCalc电梯(Elevator eqp, Detail bus, BusinessCat_Enum bstp, String sort, String vart, CalcFeeResp rsp)
    {
    //【第1大块】基础收费fee_mod=1
        if(REGUL.equals(bstp)) {     //OPE_TYPE":"3"｛ 定期检验=3;
            if ("343".equals(vart) ) {   //杂物电梯定期检验" FEE_COD":"3120"
                rsp.useBase("3120", 310);
            }
            else if("331".equals(vart) || "332".equals(vart) ){     //自动扶梯、自动人行道定期检验" "FEE_COD":"3130"
                rsp.useBase("3130", 420);
            }
            else{  //"EQP_VART==3110||EQP_VART==3120||EQP_VART==3130||EQP_VART==3210||EQP_VART==3220||EQP_VART==3410||EQP_VART==3420"
                rsp.useBase("3110", 540);    //电梯（乘客、载货）定期检验" FEE_COD":"3110"
            }
        }
        else if(SAFETYINS.equals(bstp)) {     //OPE_TYPE":"4",
            if(null==eqp.getMoney() || eqp.getMoney()<25000)  rsp.getParms().add("money");
            else {   //进口电梯安全性监督检验" FEE_COD":"3230"
                rsp.useBase("3230", eqp.getMoney() * 0.009);
            }
        }
        else if(MANUFACT.equals(bstp)) {     //OPE_TYPE":"1",
            if(null==bus.getZmoney() || bus.getZmoney()<10000)  rsp.getParms().add("zmoney");
            else {   //电梯制造过程监督检验" FEE_COD":"3240"
                rsp.useBase("3240", eqp.getMoney() * 0.007);
            }
        }
        else if(INSTA.equals(bstp)) {     //OPE_TYPE":"2",
            if ("343".equals(vart) ) {   //杂物电梯安装过程监督检验" FEE_COD":"3250"
                rsp.useBase("3250", 1000);
            }else{      //电梯安装过程监督检验" FEE_COD":"3210"
                double eqcost=rsp.校验造价(eqp,bus,15000,5000);
                if(eqcost>0) {
                    if (eqcost <= 200000) {  //按X（电梯单台工程造价）的2.0%收取，但不低于2000元/台，计费按超额累进制。FEE_COD":"3211"
                        rsp.useBase("3211", eqcost * 0.02,2000);
                    } else if (eqcost <= 300000) rsp.useBase("3212", eqcost * 0.015+4000);
                    else rsp.useBase("3213", eqcost * 0.01+5500);
                }
            }
        }
        else if(REFORM.equals(bstp) || REPAIR.equals(bstp) ){    //OPE_TYPE":"14,15"｛
            if ("343".equals(vart) ) {   //杂物电梯改造、修理过程监督检验" FEE_COD":"3260"
                rsp.useBase("3260", 1000);
            }else{      //电梯改造、修理过程监督检验" FEE_COD":"3220" "((INST_PRICE+EQP_PRICE)*35)/1000" "按电梯单台修理、改造工程费的3.5%收取，但不低于1000元/台"
                //【来源】实际情况EQP_PRICE取值很小，不是不可随意修改的eqp.money; 实际收费多数是扭曲的；所以直接重新定义新的可以检验每次变动的设备价参数：替换它。
                double eqcost=rsp.use改造修理造价(bus,2000,1000);
                if(eqcost>0) {  //FEE_COD":"3220" 但不低于1000元/台
                    rsp.useBase("3220", eqcost * 0.035, 1000);
                }
            }
        }
        Double mod1fee =rsp.addUpMod1fee();    //合计fee_mod 1:
        //【第2大块】进入加收收费fee_mod=8：还需提供mod1的金额
        if(INSTA.equals(bstp)) {     //OPE_TYPE":"2",
            if(null!=eqp.getVlg() && "棚户区".equals(eqp.getVlg().getType()) ) {
                rsp.addFeeItem("B50", (double) 0,9);      //"棚户区电梯安装过程监督检验，收费全部减免","EXTRAFEE_ID":"50",
            }
            if(Boolean.TRUE.equals(eqp.getOldb()) ){  //EXTRAFEE_ID":"15",IF_OLDBUILD_INST==1","-(srcPrice*50)/100",
                rsp.addFeeItem("A15", mod1fee*(-0.5) ,8);
            }
        }
        else if(REGUL.equals(bstp)) {     //OPE_TYPE":"3",
            if(null==eqp.getFlo() || eqp.getFlo()<2)  rsp.getParms().add("flo");
            else if(eqp.getFlo()>6){    //EXTRAFEE_ID":"110","ELEFLOORNUMBER>6.0 , RS_TIME":"ELEFLOORNUMBER-6",
                rsp.addFeeItem("A110", (double) (30*(eqp.getFlo()-6)),8, (double)(eqp.getFlo()-6));
            }
            if(Boolean.TRUE.equals(eqp.getSpec()) ){  //EXTRAFEE_ID":"11",IF_SPEC_EQP==1", RS_EXPR":"(srcPrice*30)/100"
                rsp.addFeeItem("A11", mod1fee*0.3 ,8);
            }
            if("332".equals(vart)){     //EXTRAFEE_ID":"7", EQP_VART==3320&&SLIDWAY_USE_LENG>30.0"
                if(null==eqp.getLesc() || eqp.getLesc()<2)  rsp.getParms().add("lesc");
                else if(eqp.getLesc()>30){
                    int times=(int)Math.ceil( (eqp.getLesc()-30)/5);
                    rsp.addFeeItem("A7", mod1fee*0.1*times ,8, (double)times);
                }
            }
            else if("331".equals(vart)){     //EXTRAFEE_ID":"6", EQP_VART==3310&&DT_ELEHEIGHT>6.0",
                if(null==eqp.getHlf() || eqp.getHlf()<1)  rsp.getParms().add("hlf");
                else if(eqp.getHlf()>6){
                    int times=(int)Math.ceil( (eqp.getHlf()-6)/2);
                    rsp.addFeeItem("A6", mod1fee*0.1*times ,8, (double)times);
                }
            }
        }
    }
    /**厂（场）内机动车 自动计算收费; subv=是非国家目录扩展的子设备品种
     * */
    private void chargeCalc厂车(FactoryVehicle eqp, Detail bus, BusinessCat_Enum bstp, String sort, String vart,String subv,CalcFeeResp rsp)
    {
        //【第1大块】基础收费fee_mod=1
        if(REGUL.equals(bstp)) {     //OPE_TYPE":"3"｛ 定期检验=3;
            if ("51".equals(sort) ) {   //厂（场）内机动车辆定期检验" FEE_COD":"5110",
                rsp.useBase("5110", 260);
            }
            else if ("52".equals(sort) ) {
                if(!StringUtils.hasText(subv))   rsp.getParms().add("subv");     //subv可能是'',null;
                else if ("5002".equals(subv) ) {   //电动类非公路用旅游观光车辆定期检验" FEE_COD":"5320"
                    rsp.useBase("5320", 400);
                }
                else {   //缺省该是收钱多的那种： 内燃类非公路用旅游观光车辆定期检验" FEE_COD":"5310" 原定规则附加条件"SUB_EQP_VART==5001"
                    rsp.useBase("5310", 500);
                }
            }
        }
        else if(INSTA.equals(bstp) || DELIVERY.equals(bstp) || FIRST.equals(bstp) ) {    //OPE_TYPE":"2,10,16"
            if ("51".equals(sort) ) {   //厂（场）内机动车辆首次检验" FEE_COD":"5210"
                rsp.useBase("5210", 260);
            }
            else if ("52".equals(sort) ) {
                if(!StringUtils.hasText(subv))   rsp.getParms().add("subv");
                else if ("5002".equals(subv) ) {   //电动类非公路用旅游观光车辆首次检验" FEE_COD":"5420"
                    rsp.useBase("5420", 600);
                }
                else {   //缺省：内燃类非公路用旅游观光车辆首次检验" FEE_COD":"5410"
                    rsp.useBase("5410", 600);
                }
            }
        }
        Double mod1fee =rsp.addUpMod1fee();    //合计fee_mod 1:
        //【第2大块】进入加收收费fee_mod=8：还需提供mod1的金额
        if(REGUL.equals(bstp) || INSTA.equals(bstp) || DELIVERY.equals(bstp) || FIRST.equals(bstp) ) {    //OPE_TYPE":"2,3,10,16"
            if ("51".equals(sort) ) {   //需要进行工作装置测试，加收50%","EXTRAFEE_ID":"42"
                //Detail bus：里面的参数针对每一次Isp检验的，前端:依据设备种类业务类型的实际需要：都要要求确认输入。
                if(Boolean.TRUE.equals(bus.isTest() )) {
                    rsp.addFeeItem("A42",mod1fee*0.5 ,8);
                }
            }
        }
    }
    /**游乐 自动计算收费;
     * */
    private void chargeCalc游乐(Amusement eqp, Detail bus, BusinessCat_Enum bstp, String sort, String vart,String subv,CalcFeeResp rsp)
    {
        //【第1大块】基础收费fee_mod=1
        if(REGUL.equals(bstp)) {     //OPE_TYPE":"3"｛ 定期检验=3;
            if ("61".equals(sort) ) {   //观览车类定期检验" "FEE_COD":"6110"
                rsp.useBase("6110", 1000);
            }
            else if ("65".equals(sort) ) {  //飞行塔类定期检验"  20,  *,
                rsp.useBase("6120", 1200);
            }
            else if ("67".equals(sort) ) {  //自控飞机类定期检验" 6130,   *,
                rsp.useBase("6130", 1200);
            }
            else if ("63".equals(sort) ) {  //架空游览车类定期检验"  4,   *
                rsp.useBase("6140", 800);
            }
            else if ("69".equals(sort) ) {  //小火车类型定期检验"  5,   *
                rsp.useBase("6150", 700);
            }
            else if ("64".equals(sort) ) {  //陀螺类定期检验"  7,   *
                rsp.useBase("6170", 1200);
            }
            else if ("66".equals(sort) ) {  //转马类定期检验"  9，   *
                rsp.useBase("6190", 1200);
            }
            else if ("68".equals(sort) ) {  //赛车类定期检验" FEE_COD":"6200"   *
                rsp.useBase("6200", 640);
            }
            else if ("6A".equals(sort) ) {  //碰碰车类"  FEE_COD":"6210"   *
                rsp.useBase("6210", 640);
            }
            else if ("6B".equals(sort) ) {  //滑道定期检验" FEE_COD":"6222"   *
                rsp.useBase("6222", 1200);
            }
            else if ("62".equals(sort) ) {  //滑行车系列定期检验" FEE_COD":"6221"
                rsp.useBase("6221", 1500);
            }
            else if ("6D1".equals(vart) ) {  //峡谷漂流系列定期检验" FEE_COD":"6231"  *
                rsp.useBase("6231", 1500);
            }
            else if ("6D2".equals(vart) ) {  //水滑梯系列定期检验" FEE_COD":"6232" *
                rsp.useBase("6232", 400);
            }
            else if ("6D4".equals(vart) ) {  //碰碰船系列定期检验" 废? FEE_COD":"6234" *", 监察没有此类设备,迁出
                rsp.useBase("6234", 160);
            }
            else if ("6E1".equals(vart) ) {  //蹦极系列
                if(!StringUtils.hasText(subv))   rsp.getParms().add("subv");
                else if ("6001".equals(subv) ) {   //高空蹦极系列定期检验" FEE_COD":"6241"
                    rsp.useBase("6241", 2000);
                }
                else if ("6003".equals(subv) ) {   //小蹦极系列定期检验"  3,  *
                    rsp.useBase("6243", 1000);
                }
                else {   //缺省：弹射蹦极系列定期检验" FEE_COD":"6242"
                    rsp.useBase("6242", 2000);
                }
            }
            else if ("6E4".equals(vart) ) {  //滑索系列定期检验"  4, * ,
                rsp.useBase("6244", 1500);
            }
            else if ("6E5".equals(vart) ) {  //空中飞人系列定期检验"  5, *
                rsp.useBase("6245", 1200);
            }
            else if ("6E6".equals(vart) ) {  //系留式观光气球系列定期检验"  6,  *
                rsp.useBase("6246", 1200);
            }
        }
        else if(INSTA.equals(bstp) || DELIVERY.equals(bstp) ) {    //OPE_TYPE":"2,10"
            double eqcost=rsp.校验造价(eqp,bus,1000,500);
            if (eqcost <= 200000) {     //大型游乐设施安装过程监督检验" FEE_COD":"6310"
                rsp.useBase("6311", eqcost * 0.025);
            }
            else if (eqcost <= 1000000) rsp.useBase("6312", eqcost * 0.02);
            else if (eqcost <= 10000000) rsp.useBase("6313", eqcost * 0.012);
            else rsp.useBase("6314", eqcost * 0.01);
        }
        else if(REFORM.equals(bstp) || REPAIR.equals(bstp) ){    //OPE_TYPE":"14,15"｛
            double eqcost=rsp.校验造价(eqp,bus,1000,500);
            rsp.useBase("6320", eqcost * 0.04);     //大型游乐设施改造、修理过程监督检验" FEE_COD":"6320"
        }
        else if(MANUFACT.equals(bstp) ){  //OPE_TYPE":"1"
            if(null==bus.getZmoney() || bus.getZmoney()<1000)  rsp.getParms().add("zmoney");
            else {   //大型游乐设施制造过程监督检验" FEE_COD":"6330"
                rsp.useBase("6330", bus.getZmoney() * 0.009);
            }
        }
        else if(SAFETYINS.equals(bstp)) {     //OPE_TYPE":"4",
            if(null==eqp.getMoney() || eqp.getMoney()<5000)  rsp.getParms().add("money");
            else {   //进口大型游乐设施安全性能监督检验" FEE_COD":"6340"
                rsp.useBase("6340", eqp.getMoney() * 0.008);
            }
        }
        Double mod1fee =rsp.addUpMod1fee();    //合计fee_mod 1:
        //【第2大块】进入加收收费fee_mod=8：还需提供mod1的金额
        if(REGUL.equals(bstp)) {     //OPE_TYPE":"3"｛ 定期检验=3;
            //游乐设施级别=['A级','B级','C级'];  C级大型游乐设施，减收30%","EXTRAFEE_ID":"47",
            if(StringUtils.hasText(eqp.getLevel()) && "C级".equals(eqp.getLevel()))
                rsp.addFeeItem("A47", mod1fee*(-0.3) ,8);
            else if(StringUtils.hasText(eqp.getLevel()) && "'A级".equals(eqp.getLevel()))     //EXTRAFEE_ID":"46"
                rsp.addFeeItem("A46", mod1fee*0.3 ,8);
            else
                rsp.getParms().add("level");
            if(Boolean.TRUE.equals(eqp.getMbig() )){  //移动大型式游乐设施，加收50%","EXTRAFEE_ID":"44",
                rsp.addFeeItem("A44", mod1fee*0.5 ,8);
            }
            if(3==eqp.getImpt()){  //整机进口大型游乐设施，加收50%","EXTRAFEE_ID":"45"
                rsp.addFeeItem("A45", mod1fee*0.5 ,8);
            }
        }
    }
    /**锅炉 自动计算收费; RATCON:参数怪异，两种单位，如何数值上统一的？   "xx".equals(null)不会NPE爆出异常；
     * */
    private void chargeCalc锅炉(Boiler eqp, Detail bus, BusinessCat_Enum bstp, String sort, String vart,String subv,CalcFeeResp rsp)
    {
        Float gn=eqp.getPower();   //基本分叉参数：RATCONS 额定功率(MW)/蒸发量=gn     ；"2＜D≤4"
        if( null==gn || gn<=0 )  rsp.getParms().add("power");
        //【第1大块】基础收费fee_mod=1
        if(MANUFACT.equals(bstp)) {     //OPE_TYPE":"1"
            if (null == bus.getZmoney() || bus.getZmoney() < 2000) rsp.getParms().add("zmoney");
            else if (!"1002".equals(subv) && null != gn) {   //工业锅炉制造过程监督检验" FEE_COD":"1110"
                if (gn < 4)
                    rsp.useBase("1111", bus.getZmoney() * 0.007);
                else
                    rsp.useBase("1112", bus.getZmoney() * 0.005);
            }
        }
        else if(INSTA.equals(bstp) ) {    //OPE_TYPE":"2"
            if (!"1002".equals(subv) && null != gn) {
                if (Boolean.TRUE.equals(eqp.getAsemb())) {    //整组装锅炉安装过程监督检验" FEE_COD":"1210"
                    if(gn<=2)    rsp.useBase("1211", 1000);
                    else if(gn<=4)  rsp.useBase("1215", 1300);
                    else {
                        if(null==bus.getCcost() || bus.getCcost()<3000)   rsp.getParms().add("ccost");
                        else{
                            if(gn<=6)    rsp.useBase("1216", bus.getCcost()*0.04,1800);
                            else if(gn<=10)  rsp.useBase("1217", bus.getCcost()*0.04,2500);
                            else if(gn<=20)  rsp.useBase("1218", bus.getCcost()*0.04,3500);
                            else    rsp.useBase("1219", bus.getCcost()*0.04,5500);
                        }
                    }
                }
                else{    //散装锅炉安装过程监督检验" FEE_COD":"1230"
                    if(null==bus.getCcost() || bus.getCcost()<3000)   rsp.getParms().add("ccost");
                    else {
                        if(gn < 4)  rsp.useBase("1231", bus.getCcost()*0.04);    //* "D＜4"  FEE_COD":"1231",
                        else  rsp.useBase("1232", bus.getCcost()*0.04,6000);
                    }
                }
            }
            else if ("1002".equals(subv) && null != gn) {
                if(gn<=35)    rsp.useBase("1311",0, 1350, gn);
                else if(gn<=75)  rsp.useBase("1312",14000, 950, gn);
                else if(gn<=130)  rsp.useBase("1313",21500, 850, gn);
                else if(gn<=220)  rsp.useBase("1314",34500, 750, gn);
                else if(gn<=400)  rsp.useBase("1315",56500, 650, gn);
                else if(gn<=670)  rsp.useBase("1316",96500, 550, gn);
                else if(gn<=1000)  rsp.useBase("1317",96500, 550, gn);
                else if(gn<=2000)  rsp.useBase("1318",196500, 450, gn);
                else   rsp.useBase("1319",196500, 450, gn);
            }
        }
        else if(REFORM.equals(bstp) || REPAIR.equals(bstp) ){    //OPE_TYPE":"14,15"｛
            if (!"1002".equals(subv) && null != gn) {
                if (Boolean.TRUE.equals(eqp.getAsemb())) {    //整组装锅炉改造、修理过程监督检验" FEE_COD":"1220"
                    if(gn<=2)    rsp.useBase("1221", 650);
                    else if(gn<=4)  rsp.useBase("1225", 1100);
                    else {
                        if(null==bus.getCcost() || bus.getCcost()<3000)   rsp.getParms().add("ccost");
                        else{
                            if(gn<=6)    rsp.useBase("1226", bus.getCcost()*0.04,1300);
                            else if(gn<=10)  rsp.useBase("1227", bus.getCcost()*0.04,1800);
                            else if(gn<=20)  rsp.useBase("1228", bus.getCcost()*0.04,2800);
                            else    rsp.useBase("1229", bus.getCcost()*0.04,4500);
                        }
                    }
                }
                else{   //散装锅炉改造、修理过程监督检验" FEE_COD":"1240"
                    if(null==bus.getCcost() || bus.getCcost()<3000)   rsp.getParms().add("ccost");
                    else {
                        if(gn < 4)  rsp.useBase("1241", bus.getCcost()*0.04);     //"D＜4"  FEE_COD":"1241"
                        else  rsp.useBase("1242", bus.getCcost()*0.04,3000);
                    }
                }
            }
            else if ("1002".equals(subv) ) {    //电站锅炉改造、修理过程监督检验" FEE_COD":"1320"；修理、改造费的4%，但不低于10000元"
                if(null==bus.getCcost() || bus.getCcost()<30000)   rsp.getParms().add("ccost");
                else {
                    rsp.useBase("1320", bus.getCcost()*0.04,10000);
                }
            }
        }
        else if(ANNUAL.equals(bstp) ) {    //OPE_TYPE":"8"
            if (!"1002".equals(subv) && null != gn) {   //工业锅炉外部检验" FEE_COD":"1410"
                if(gn<=1)    rsp.useBase("1411", 340);
                else if(gn<=2)  rsp.useBase("1412", 400);
                else if(gn<=4)  rsp.useBase("1413", 450);
                else if(gn<=6)  rsp.useBase("1414", 560);
                else if(gn<=10)  rsp.useBase("1415", 670);
                else if(gn<=20)  rsp.useBase("1416", 830);
                else   rsp.useBase("1417", 1050);
            }
            else if ("1002".equals(subv) && null != gn) {
              if("垃圾".equals(eqp.getFuel()) || "生活垃圾".equals(eqp.getFuel())){   //垃圾焚烧发电锅炉外部检验" FEE_COD":"1610"
                  if(gn<=35)    rsp.useBase("1611",0, 130, gn);
                  else if(gn<=75)  rsp.useBase("1612",140, 126, gn);
                  else if(gn<=130)  rsp.useBase("1613",590, 120, gn);
                  else if(gn<=220)  rsp.useBase("1614",590, 120, gn);
                  else if(gn<=400)  rsp.useBase("1615",2790, 110, gn);
                  else if(gn<=670)  rsp.useBase("1616",6790, 100, gn);
                  else if(gn<=1000)  rsp.useBase("1617",33590, 60, gn);
                  else   rsp.useBase("1618",63590, 30, gn);
              }else{    //电站锅炉外部检验" FEE_COD":"1510"
                  if(gn<=35)    rsp.useBase("1511",0, 65, gn);
                  else if(gn<=75)  rsp.useBase("1512",70, 63, gn);
                  else if(gn<=130)  rsp.useBase("1513",295, 60, gn);
                  else if(gn<=220)  rsp.useBase("1514",295, 60, gn);
                  else if(gn<=400)  rsp.useBase("1515",1395, 55, gn);
                  else if(gn<=670)  rsp.useBase("1516",3395, 50, gn);
                  else if(gn<=1000)  rsp.useBase("1517",16795, 30, gn);
                  else   rsp.useBase("1518",31795, 15, gn);
              }
            }
        }
        else if(REGUL.equals(bstp) ) {    //OPE_TYPE":"3"
            if (!"1002".equals(subv) && null != gn) {    //工业锅炉内部检验" FEE_COD":"1420"
                if(gn<=1)    rsp.useBase("1421", 460);
                else if(gn<=2)  rsp.useBase("1422", 550);
                else if(gn<=4)  rsp.useBase("1423", 660);
                else if(gn<=6)  rsp.useBase("1424", 800);
                else if(gn<=10)  rsp.useBase("1425", 980);
                else if(gn<=20)  rsp.useBase("1426", 1320);
                else   rsp.useBase("1427", 1800);
            }
            else if ("1002".equals(subv) && null != gn) {
                if("垃圾".equals(eqp.getFuel()) || "生活垃圾".equals(eqp.getFuel())){  //垃圾焚烧发电锅炉内部检验" FEE_COD":"1620"
                    if(gn<=35)    rsp.useBase("1621",0, 700, gn);
                    else if(gn<=75)  rsp.useBase("1622",7000, 500, gn);
                    else if(gn<=130)  rsp.useBase("1623",-15500, 800, gn);
                    else if(gn<=220)  rsp.useBase("1624",-28500, 900, gn);
                    else if(gn<=400)  rsp.useBase("1625",-28500, 900, gn);
                    else if(gn<=670)  rsp.useBase("1626",-68500, 1000, gn);
                    else if(gn<=1000)  rsp.useBase("1627",-1500, 900, gn);
                    else   rsp.useBase("1628",358500, 540, gn);
                }else{    //电站锅炉内部检验" FEE_COD":"1520"
                    if(gn<=35)    rsp.useBase("1521",0, 350, gn);
                    else if(gn<=75)  rsp.useBase("1522",3500, 250, gn);
                    else if(gn<=130)  rsp.useBase("1523",-7750, 400, gn);
                    else if(gn<=220)  rsp.useBase("1524",-14250, 450, gn);
                    else if(gn<=400)  rsp.useBase("1525",-14250, 450, gn);
                    else if(gn<=670)  rsp.useBase("1526",-34250, 500, gn);
                    else if(gn<=1000)  rsp.useBase("1527",-750, 450, gn);
                    else   rsp.useBase("1528",179250, 270, gn);
                }
            }
        }
        else if(OTHER.equals(bstp) ) {    //OPE_TYPE":"13"
            if (!"1002".equals(subv) && null != gn) {    //整组装锅炉化学清洗监督检验" FEE_COD":"1710"
                if(gn<=1)    rsp.useBase("1711", 300);
                else if(gn<=2)  rsp.useBase("1712", 600);
                else {
                    if(null==bus.getCcost() || bus.getCcost()<1000)   rsp.getParms().add("ccost");
                    else{
                        if(gn<=4)    rsp.useBase("1713", bus.getCcost()*0.04,1300);
                        else if(gn<=6)  rsp.useBase("1714", bus.getCcost()*0.04,1800);
                        else if(gn<=10)  rsp.useBase("1715", bus.getCcost()*0.04,2500);
                        else if(gn<=20)  rsp.useBase("1716", bus.getCcost()*0.04,3500);
                        else    rsp.useBase("1717", bus.getCcost()*0.04,5500);
                    }
                }
            }
            else if ("1002".equals(subv) ) {
                if(null==bus.getCcost() || bus.getCcost()<5000)   rsp.getParms().add("ccost");
                else{       //电站锅炉化学清洗监督检验" FEE_COD":"1720"
                    rsp.useBase("1720", bus.getCcost()*0.04,10000);
                }
            }
        }
        else if(THERMAL.equals(bstp) ) {    //OPE_TYPE":"19"
            if(Boolean.TRUE.equals(bus.isApprp() )){   //工业锅炉定型产品热效率测试" FEE_COD":"1810", "REP_TYPE==710016"
                if (null != gn) {
                    if(gn<=1)    rsp.useBase("1811", 4400);
                    else if(gn<=2)  rsp.useBase("1812", 11000);
                    else if(gn<=4)  rsp.useBase("1813", 12000);
                    else if(gn<=6)  rsp.useBase("1814", 18000);
                    else if(gn<=10)  rsp.useBase("1815", 26000);
                    else if(gn<=20)  rsp.useBase("1816", 28000);
                    else   rsp.useBase("1817", 33000);
                }
            }
            else if(Boolean.TRUE.equals(bus.isCheap() )){   //热效率简单测试
                if("11".equals(sort)||"12".equals(sort) ){   //蒸汽（热水）锅炉运行工况热效率简单测试" FEE_COD":"1910"
                    if (null != gn) {
                        if(gn<=1)    rsp.useBase("1911", 1000);
                        else if(gn<=2)  rsp.useBase("1912", 1200);
                        else if(gn<=4)  rsp.useBase("1913", 1500);
                        else if(gn<=6)  rsp.useBase("1914", 1800);
                        else if(gn<=10)  rsp.useBase("1915", 2200);
                        else if(gn<=20)  rsp.useBase("1916", 3000);
                        else   rsp.useBase("1917", 4000);
                    }
                }
                else if("13".equals(sort)){
                    if (null != gn) {    //有机热载体锅炉运行工况热效率简单测试" FEE_COD":"1920"
                        if(gn<=1)    rsp.useBase("1921", 1200);
                        else if(gn<=2)  rsp.useBase("1922", 1400);
                        else if(gn<=4)  rsp.useBase("1923", 1700);
                        else if(gn<=6)  rsp.useBase("1924", 2000);
                        else if(gn<=10)  rsp.useBase("1925", 2400);
                        else if(gn<=20)  rsp.useBase("1926", 3200);
                        else   rsp.useBase("1927", 4200);
                    }
                }
            }
            else {    //热效率详细测试
                if("11".equals(sort)||"12".equals(sort) ){   //蒸汽（热水）锅炉运行工况热效率详细测试" FEE_COD":"1930"
                    if (null != gn) {
                        if(gn<=1)    rsp.useBase("1931", 3500);
                        else if(gn<=2)  rsp.useBase("1932", 3500);
                        else if(gn<=4)  rsp.useBase("1933", 6000);
                        else if(gn<=6)  rsp.useBase("1934", 9000);
                        else if(gn<=10)  rsp.useBase("1935", 13500);
                        else if(gn<=20)  rsp.useBase("1936", 18000);
                        else   rsp.useBase("1937", 23000);
                    }
                }
                else if("13".equals(sort)){
                    if (null != gn) {    //有机热载体锅炉运行工况热效率详细测试" FEE_COD":"1940"
                        if(gn<=1)    rsp.useBase("1941", 4250);
                        else if(gn<=2)  rsp.useBase("1942", 4250);
                        else if(gn<=4)  rsp.useBase("1943", 6750);
                        else if(gn<=6)  rsp.useBase("1944", 9750);
                        else if(gn<=10)  rsp.useBase("1945", 14250);
                        else if(gn<=20)  rsp.useBase("1946", 18750);
                        else   rsp.useBase("1947", 23750);
                    }
                }
            }
        }
        else if(EXPERIMENT.equals(bstp) ) {    //OPE_TYPE":"12";    推定是? [{"OPE_TYPE"都是:"12"='试验'}]
            if ("1002".equals(subv) && null != gn) {      //"电站锅炉能效测试" FEE_COD":"1A10"
                if(gn<=35)    rsp.useBase("1A11", 36000);
                else if(gn<=75)  rsp.useBase("1A12", 47000);
                else if(gn<=130)  rsp.useBase("1A13", 57000);
                else if(gn<=220)  rsp.useBase("1A14", 67000);
                else if(gn<=400)  rsp.useBase("1A15", 77000);
                else if(gn<=670)  rsp.useBase("1A16", 87000);
                else if(gn<=1000)  rsp.useBase("1A17", 97000);
                else   rsp.useBase("1A18", 107000);
            }
        }
        else if(SAFETYINS.equals(bstp) ) {   //OPE_TYPE":"4"; 锅炉 6种
            if(null==eqp.getMoney() || eqp.getMoney()<1000)  rsp.getParms().add("money");
            if(!StringUtils.hasText(bus.getFcode()))    rsp.getParms().add("fcode");
            if(rsp.getParms().size()<=0) {
                if("J110".equals(bus.getFcode()))  rsp.useBase(bus.getFcode(), eqp.getMoney()*0.01);
                else if("J120".equals(bus.getFcode()))  rsp.useBase(bus.getFcode(), eqp.getMoney()*0.01);
                else if("J180".equals(bus.getFcode()))  rsp.useBase(bus.getFcode(), eqp.getMoney()*0.01);
                else if("J210".equals(bus.getFcode()))  rsp.useBase(bus.getFcode(), eqp.getMoney()*0.02);
                else if("J220".equals(bus.getFcode()))  rsp.useBase(bus.getFcode(), eqp.getMoney()*0.024);
                else if("J270".equals(bus.getFcode()))  rsp.useBase(bus.getFcode(), eqp.getMoney()*0.02);
            }
        }
        Double mod1fee =rsp.addUpMod1fee();    //合计fee_mod 1:
        //【第2大块】进入加收收费fee_mod=8：还需提供mod1的金额
        if(REGUL.equals(bstp) || ANNUAL.equals(bstp) ) {    //OPE_TYPE":"3,8"
            if ("1003".equals(subv) && null != gn) {
                if(Boolean.TRUE.equals(eqp.getWall() )) {    //工业锅炉炉墙检验（D≤1），每台加收20元","EXTRAFEE_ID":"114",
                    if(gn<=1)    rsp.addFeeItem("A114",20 ,8);
                    else if(gn<=2)  rsp.addFeeItem("A115",30 ,8);
                    else if(gn<=4)  rsp.addFeeItem("A116",50 ,8);
                    else if(gn<=6)  rsp.addFeeItem("A117",70 ,8);
                    else if(gn<=10)  rsp.addFeeItem("A118",90 ,8);
                    else if(gn<=20)  rsp.addFeeItem("A119",150 ,8);
                    else   rsp.addFeeItem("A120",gn*8-10 ,8);      //RS_EXPR":"(RATCONS*8)-10",
                }
            }
        }
    }
    /**压力容器 自动计算收费;
     * */
    private void chargeCalc容器(Vessel eqp, Detail bus, BusinessCat_Enum bstp, String sort, String vart,String subv,CalcFeeResp rsp){
        //【特别】gn 有2种映射定义：超高压容器EQP_VART==2110" 是DESPRE参数的；其它情况配对CONTAINERVOLUME。
        Double gn=null;     //基本分叉参数2种：CONTAINERVOLUME容积或 DESPRE压力  =gn 根据设备种类和业务类型具体定夺;
        String  nVvmemo=null;    //提示CONTAINERVOLUME: eqp.vol参数 或 DESPRE 参数可能存在问题。
        if(("213".equals(vart) || "215".equals(vart) || "217".equals(vart)) && (ANNUAL.equals(bstp) || REGUL.equals(bstp))) {
            gn=Tool.castDouble(eqp.getVol());
            if(null==gn || gn<0.02)    rsp.getParms().add("vol");   //检查明显不正常的 CONTAINERVOLUME 参数！
            else if(!Tool.testNumericalNormal(gn,eqp.getVol()))    nVvmemo= "V=" + gn + ";台账容积=" + eqp.getVol();
        }
        else if("211".equals(vart) && (ANNUAL.equals(bstp) || REGUL.equals(bstp)) ) {    //基本分叉参数:第二种 DESPRE
            gn=Tool.castDouble(eqp.getPrs());
            if(null==gn || gn<10)    rsp.getParms().add("prs");      //检查明显不正常的 DESPRE压力 参数！ 设计压力（MPa）超高压(代号U) p≥100.0MPa。
            else if(!Tool.testNumericalNormal(gn,eqp.getPrs()))    nVvmemo= "MPa=" + gn + ";台账压力=" + eqp.getPrs();
        }
        //【第1大块】基础收费fee_mod=1
        if(ANNUAL.equals(bstp) ) {    //OPE_TYPE":"8"
            if("217".equals(vart) && null!=gn){     //一类压力容器年度检查" FEE_COD":"2310",
                if(gn<1)    rsp.useBase("2311", 120 ,nVvmemo);    //"V＜1"  FEE_COD":"2311"
                else if(gn<10)  rsp.useBase("2312", 150 ,nVvmemo);
                else if(gn<20)  rsp.useBase("2313", 200 ,nVvmemo);
                else if(gn<50)  rsp.useBase("2314", 250 ,nVvmemo);
                else if(gn<100)  rsp.useBase("2315", 350 ,nVvmemo);
                else   rsp.useBase("2316", 0, 5, gn,nVvmemo);
            }
            else if("215".equals(vart) && null!=gn){    //二类压力容器年度检查" FEE_COD":"2330"
                if(gn<1)    rsp.useBase("2331", 156 ,nVvmemo);
                else if(gn<10)  rsp.useBase("2332", 195 ,nVvmemo);
                else if(gn<20)  rsp.useBase("2333", 260 ,nVvmemo);
                else if(gn<50)  rsp.useBase("2334", 325 ,nVvmemo);
                else if(gn<100)  rsp.useBase("2335", 455 ,nVvmemo);
                else   rsp.useBase("2336", 0, 6.5, gn,nVvmemo);
            }
            else if("213".equals(vart) && null!=gn){    //三类压力容器年度检查" FEE_COD":"2350"
                if(gn<1)    rsp.useBase("2351", 180 ,nVvmemo);
                else if(gn<10)  rsp.useBase("2352", 225 ,nVvmemo);
                else if(gn<20)  rsp.useBase("2353", 300 ,nVvmemo);
                else if(gn<50)  rsp.useBase("2354", 375 ,nVvmemo);
                else if(gn<100)  rsp.useBase("2355", 525 ,nVvmemo);
                else   rsp.useBase("2356", 0, 7.5, gn,nVvmemo);
            }
            else if("211".equals(vart) && null!=gn){    //超高压容器年度检查" FEE_COD":"2420"
                if(gn<200)    rsp.useBase("2421", 700 ,nVvmemo);
                else if(gn<500)  rsp.useBase("2422", 800 ,nVvmemo);
                else   rsp.useBase("2423", 1000 ,nVvmemo);
            }
            else if("241".equals(vart) ){   //医用氧舱年度检验" FEE_COD":"2510"
                if(null==eqp.getPnum() || eqp.getPnum()<1)
                    rsp.getParms().add("pnum2");    //参数同名pnum，可实际含义描述差别太大。
                else if(1==eqp.getPnum())   rsp.useBase("2511", 580);
                else if(2==eqp.getPnum())   rsp.useBase("2512", 1000);
                else if(eqp.getPnum()<=6)   rsp.useBase("2513", 1300);
                else if(eqp.getPnum()<=14)   rsp.useBase("2514", 1500);
                else    rsp.useBase("2515", 2700);
            }
            //EQP_VART==2110;
            //EQP_VART==2410"
            //EQP_VART==2210||EQP_VART==2220||EQP_VART==2240||EQP_VART==2230||EQP_VART==2250";
        }
        else if(REGUL.equals(bstp) ) {    //OPE_TYPE":"3"
            if("217".equals(vart) && null!=gn){     //一类压力容器定期检验" FEE_COD":"2320"
                if(gn<1)    rsp.useBase("2321", 300 ,nVvmemo);    //"V＜1"  FEE_COD":"2311"
                else if(gn<10)  rsp.useBase("2322", 470 ,nVvmemo);
                else if(gn<20)  rsp.useBase("2323", 520 ,nVvmemo);
                else if(gn<50)  rsp.useBase("2324", 720 ,nVvmemo);
                else if(gn<100)  rsp.useBase("2325", 1000 ,nVvmemo);
                else if(gn<300)  rsp.useBase("2326", 0, 10, gn,nVvmemo);
                else   rsp.useBase("2327", 0, 13, gn,nVvmemo);
            }
            else if("215".equals(vart) && null!=gn){     //二类压力容器定期检验" FEE_COD":"2340"
                if(gn<1)    rsp.useBase("2341", 390 ,nVvmemo);
                else if(gn<10)  rsp.useBase("2342", 611 ,nVvmemo);
                else if(gn<20)  rsp.useBase("2343", 676 ,nVvmemo);
                else if(gn<50)  rsp.useBase("2344", 936 ,nVvmemo);
                else if(gn<100)  rsp.useBase("2345", 1300 ,nVvmemo);
                else if(gn<300)  rsp.useBase("2346", 0, 13, gn,nVvmemo);
                else   rsp.useBase("2347", 0, 16.9, gn,nVvmemo);
            }
            else if("213".equals(vart) && null!=gn){     //三类压力容器定期检验" FEE_COD":"2360"
                if(gn<1)    rsp.useBase("2361", 450 ,nVvmemo);
                else if(gn<10)  rsp.useBase("2362", 705 ,nVvmemo);
                else if(gn<20)  rsp.useBase("2363", 780 ,nVvmemo);
                else if(gn<50)  rsp.useBase("2364", 1080 ,nVvmemo);
                else if(gn<100)  rsp.useBase("2365", 1500 ,nVvmemo);
                else if(gn<300)  rsp.useBase("2366", 0, 15, gn,nVvmemo);
                else   rsp.useBase("2367", 0, 19.5, gn,nVvmemo);
            }
            else if("211".equals(vart) && null!=gn){    //超高压容器全面检验" FEE_COD":"2410"
                if(gn<200)    rsp.useBase("2411", 1300 ,nVvmemo);
                else if(gn<500)  rsp.useBase("2412", 1400 ,nVvmemo);
                else   rsp.useBase("2413", 1600 ,nVvmemo);
            }
            else if("241".equals(vart) ){   //医用氧舱全面检验" FEE_COD":"2520"
                if(null==eqp.getPnum() || eqp.getPnum()<1)    rsp.getParms().add("pnum2");
                else if(1==eqp.getPnum())   rsp.useBase("2521", 900);
                else if(2==eqp.getPnum())   rsp.useBase("2522", 1200);
                else if(eqp.getPnum()<=6)   rsp.useBase("2523", 1500);
                else if(eqp.getPnum()<=14)   rsp.useBase("2524", 2000);
                else    rsp.useBase("2525", 3200);
            }
        }
        else if(INSTA.equals(bstp) ) {    //OPE_TYPE":"2"
            if ("2001".equals(subv) || "2002".equals(subv)) {   //大型压力容器和球形容器（含同一装置内的容器连接管道）安装过程监督检验" FEE_COD":"2210"
                double eqcost = rsp.校验造价(eqp, bus, 2000, 500);
                rsp.useBase("2210", eqcost * 0.013);
            }
            else if ("2003".equals(subv) ) {    //液化气体储配站压力容器安装过程监督检验" 2230",
                if(null==bus.getCcost() || bus.getCcost()<3000)   rsp.getParms().add("ccost");
                else    rsp.useBase("2230", bus.getCcost() * 0.04);
            }
            else if ("2004".equals(subv) ) {   //非大型压力容器安装过程监督检验" 2250"
                double eqcost = rsp.校验造价(eqp, bus, 500, 500);
                rsp.useBase("2250", eqcost * 0.01);
            }
            else if ("2006".equals(subv) ) {   //超高压水晶釜安装过程监督检验" 2280"
                rsp.useBase("2280", 1500);
            }
            else if ("2005".equals(subv) && "211".equals(vart)) {   //超高压容器安装过程监督检验" 2270"
                double eqcost = rsp.校验造价(eqp, bus, 1000, 500);
                rsp.useBase("2270", eqcost * 0.01);
            }
        }
        else if(REFORM.equals(bstp) || REPAIR.equals(bstp) ) {    //OPE_TYPE":"14,15"
            if(null==bus.getCcost() || bus.getCcost()<1000)   rsp.getParms().add("ccost");
            else {
                if ("2003".equals(subv)) {    //液化气体储配站压力容器改造、修理过程监督检验" 2240"
                    rsp.useBase("2240", bus.getCcost() * 0.02);
                } else if ("2004".equals(subv)) {   //非大型压力容器改造、修理过程监督检验" 2260"
                    rsp.useBase("2260", bus.getCcost() * 0.02,400);
                }
                else if ("21".equals(sort)) {   //大型压力容器和球形容器（含同一装置内的容器连接管道）改造、修理过程监督检验" 2220",隐含+2005 +2006的SUB_EQP_VART
                    rsp.useBase("2220", bus.getCcost() * 0.02,2000);
                }
            }
        }
        else if(MANUFACT.equals(bstp)) {     //OPE_TYPE":"1"
            if (null == bus.getZmoney() || bus.getZmoney() < 500)   rsp.getParms().add("zmoney");
            else if ("213".equals(vart) || "215".equals(vart) || "217".equals(vart)) {   //"一、二、三类压力容器制造过程监督检验" FEE_COD":"2110"
                rsp.useBase("2110", bus.getZmoney() * 0.01);
            }
            else if("22".equals(sort)){     //槽车制造过程监督检验" 2160"
                rsp.useBase("2160", bus.getZmoney() * 0.015);
            }
            else if("23".equals(sort)){
                if(!StringUtils.hasText(bus.getFcode()))    rsp.getParms().add("fcode");
                if(rsp.getParms().size()<=0) {
                    if ("2120".equals(bus.getFcode())) rsp.useBase(bus.getFcode(), bus.getZmoney() * 0.01);
                    else if ("2130".equals(bus.getFcode())) rsp.useBase(bus.getFcode(), bus.getZmoney() * 0.01);
                    else if ("2140".equals(bus.getFcode())) rsp.useBase(bus.getFcode(), bus.getZmoney() * 0.008);
                    else if ("2150".equals(bus.getFcode())) rsp.useBase(bus.getFcode(), bus.getZmoney() * 0.009);
                }
            }
            else if (!"21".equals(sort) ) {     //其它设备制造过程监督检验" 2170"
                rsp.useBase("2170", bus.getZmoney() * 0.01);
            }
        }
        else if(SAFETYINS.equals(bstp)) {    //OPE_TYPE":"4"
            if(null==eqp.getMoney() || eqp.getMoney()<1000)  rsp.getParms().add("money");
            if(!StringUtils.hasText(bus.getFcode()))    rsp.getParms().add("fcode");
            if(rsp.getParms().size()<=0) {   //进口承压类特种设备安全性能监督检验收费标准" FEE_COD":"J000"
                if("J130".equals(bus.getFcode()))  rsp.useBase(bus.getFcode(), eqp.getMoney()*0.01);
                else if("J150".equals(bus.getFcode()))  rsp.useBase(bus.getFcode(), eqp.getMoney()*0.01);
                else if("J160".equals(bus.getFcode()))  rsp.useBase(bus.getFcode(), eqp.getMoney()*0.01);
                else if("J140".equals(bus.getFcode()))  rsp.useBase(bus.getFcode(), eqp.getMoney()*0.01);
                else if("J190".equals(bus.getFcode()))  rsp.useBase(bus.getFcode(), eqp.getMoney()*0.01);
                else if("J230".equals(bus.getFcode()))  rsp.useBase(bus.getFcode(), eqp.getMoney()*0.024);
                else if("J250".equals(bus.getFcode()))  rsp.useBase(bus.getFcode(), eqp.getMoney()*0.03);
                else if("J260".equals(bus.getFcode()))  rsp.useBase(bus.getFcode(), eqp.getMoney()*0.015);
                else if("J240".equals(bus.getFcode()))  rsp.useBase(bus.getFcode(), eqp.getMoney()*0.02);
            }
        }
        //交叉逻辑分支:
        if(REGUL.equals(bstp) || ANNUAL.equals(bstp) ) {    //OPE_TYPE":"3,8";  特殊部分:【22 移动式】
                //汽车罐车、铁路罐车检验收费标准" FEE_COD":"2600", "内外表面检验" FEE_COD":"2610",
            if("221".equals(vart) || "222".equals(vart) || "224".equals(vart) || "223".equals(vart) || "225".equals(vart) ){
                if (bus.getVarea() < 1  && bus.getDiame() <=0)      //似乎有耐压试验就不需要内外表面检验!
                    rsp.getParms().add("varea");
                else {
                    //旧平台允许没有FEE_COD":"2610"这一项？[考虑归并]最后一项特别定制条款 代码"B107" 手动收费调整。
                    if (bus.getVarea() >= 1)    rsp.useBase("2610", 0, 70, bus.getVarea());
                }
                //"安全阀性能试验"  FEE_COD":"2650", , RS_EXPR":"30" 手选改 改成自动计算+参数必须输入的:
                if (bus.getNum() >0)  rsp.addFeeItem("2650", 30*bus.getNum(),2, bus.getNum().doubleValue());
                //有些根本就没有这一项目的收费? "气密性试验"  FEE_COD":"2630"
                if(!Boolean.TRUE.equals(bus.getNtscop() )) {
                    //"气密性试验"  FEE_COD":"2630", , RS_EXPR":"100" 2000 OPE_TYPE":"3,8" "气密性试验" 乘数 立方米， FEE_COD":"2630"
                    Double gasTight=null;
                    String  nVvmemo2=null;
                    gasTight=Tool.castDouble(eqp.getVol());
                    if(null==gasTight || gasTight<0.02)    rsp.getParms().add("vol");       //检查明显不正常的 CONTAINERVOLUME 参数！
                    else if(!Tool.testNumericalNormal(gasTight,eqp.getVol()))    nVvmemo2= "计算容积=" + gasTight + ";台账容积=" + eqp.getVol();
                    if(rsp.getParms().size()<=0) {
                        //"气密性试验" 手选 FEE_COD":"2630", , RS_EXPR":"100",  @元/立方米,！乘 立方；FEE_MOD":"2" -?直接利用台账的字段
                        rsp.addFeeItem("2630", 100 * gasTight, 2, gasTight,nVvmemo2);
                    }
                }
                //"液面计、紧急切断阀及其它附件检验" FEE_COD":"2660",FEE_STANDARD":"30~60", 元/只
                if (null!=bus.getTotm() && bus.getTotm()>0 && null!=bus.getIsum() && bus.getIsum() >0) {
                    float uprice=bus.getTotm()/bus.getIsum();       //若要生效，【输入参数】2个：要求输入个数和合计收费金额。 totm折扣后总金额 + 参数 isum只;
                    if(uprice<30 || uprice>60)   rsp.getParms().add("totm");    //收费单价不合理"30~60"元/只
                    else  rsp.addFeeItem("2660", bus.getTotm(),2, bus.getIsum().doubleValue());
                }
                //"耐压试验"   FEE_COD":"2620", , RS_EXPR":"70",耐压试验数据!=台账的容积(vol字段)。 同时存在 气密性试验 耐压试验的两个立方米 还不一样;
                if (bus.getDiame() >0)   rsp.addFeeItem("2620", 70*bus.getDiame(),2, bus.getDiame().doubleValue());
                //rustc 改成了可空!
                //"除锈喷漆"   FEE_COD":"2640", , RS_EXPR":"6",
                if (null!=bus.getRustc() && bus.getRustc() >0)   rsp.addFeeItem("2640", 6*bus.getRustc(),2, bus.getRustc().doubleValue());
            }
        }
        else if(INSTA.equals(bstp) || REFORM.equals(bstp) ) {    //OPE_TYPE":"2,14";  特殊部分:【医用氧舱】
            if("241".equals(vart) ) {
                if (null == bus.getCcost() || bus.getCcost() < 1000)    rsp.getParms().add("ccost");
                else {      //医用氧舱安装改造过程监督检验" FEE_COD":"2530"
                     rsp.useBase("2530", bus.getCcost() * 0.012, 2400);
                }
            }
        }
        Double mod1fee =rsp.addUpMod1fee();    //合计fee_mod 1:
        //【第2大块】进入加收收费fee_mod=8：还需提供mod1的金额
        if(REGUL.equals(bstp) ) {    //OPE_TYPE":"3"
            if (null == bus.getMeter() || bus.getMeter() < 0)    rsp.getParms().add("meter");
            else {      //容器检验部位>=8m时，全面检验加收40%","EXTRAFEE_ID":"96",    部位>=6m时，全面检验加收20%","EXTRAFEE_ID":"95"
                if(bus.getMeter()>=8)   rsp.addFeeItem("A96", mod1fee*0.4 ,8);
                else if(bus.getMeter()>=6)   rsp.addFeeItem("A95", mod1fee*0.2 ,8);
            }
        }
    }
    /**压力管道 自动计算收费
     【特例】无法和普通设备一样处理这一个条件附加收费："LAY_MODE=!埋地||LAY_MODE=!架空"。只能合并进基础收费中，另外把fee_mod=1进行拆分处理：分解为两个 fee_cod;
     * */
    private void chargeCalc管道(Pipeline eqp, Detail bus, BusinessCat_Enum bstp, String sort, String vart, CalcFeeResp rsp)
    {
        //每一条单元看看:  【数据规范化@特别要求】管道长度(m) 在选择管道单元派工编制时刻必须确保 leng>0。
       //【性能】后端重启后进入第一次运行1483毫秒和(由于缓存后)第二次执行469毫秒，在前端看见3倍性能差距，从数据库读取getCells()数据耗时!，瓶颈是从数据库查询读取JPA实体?。
//        eqp.getCells().forEach(a -> {
//            if(null!=a.getDet() && bus.equals(a.getDet()) && null!=a.getLeng() && a.getLeng()>0 ){    //已经选定了 这次Isp所关联单元
//                boolean ispe=false;     //PIPELINE_MEDIUM==PE管  否则 PIPELINE_MEDIUM==钢制
//                boolean little=false;     //NOMINAL_DIA 钢制 "Ф≤150" PE管 "Ф<150"   否则 "Ф＞|》=150"  默认值应当是收费贵的，可督促检验员修改录入真实数据。
//                if(StringUtils.hasText(a.getMatr()) && "PE管".equals(a.getMatr()))   ispe=true;
//                if(ispe) {
//                    if (null != a.getDia() && a.getDia() < 150)    little = true;
//                }else{
//                    if (null != a.getDia() && a.getDia() <= 150)    little = true;
//                }
//                boolean aerial=false;    //【数据规范化@特别要求】LAY_MODE除了['埋地','架空','埋地+架空','无','其它']+""+null取值异常;
//                if("架空".equals(a.getLay()) || "埋地".equals(a.getLay()) || "埋地+架空".equals(a.getLay()) )
//                    aerial=true;
//                String level=a.getLevel();       //规范化：PIPELINE_LEVEL没有默认值：依据设备种类区分后分别按照GA/B/C/D+A1 B1 GC2 GD2来计费;
//                //.stream(standardLevel).parallel().才是并行的，大载荷计算强度情况可采用。 parallel() 与sequential() 在并行流与串行流之间进行切换
//                //final String finalLevel = level;   //Arrays.stream(new String [] {"GA1","GA2","GB1","GB2","GC1","GC2","GC3","GD1","GD2"})
//                boolean isformal= List.of("GA1", "GA2", "GB1", "GB2", "GC1", "GC2", "GC3", "GD1", "GD2").contains(level);
//                if(!isformal){
//                    if("81".equals(sort))    level="GA1";
//                    else if("82".equals(sort))   level="GB1";
//                    else if("83".equals(sort) && "832".equals(vart))  level="GD2";
//                    else level="GC2";
//                }
//                chargeCalc管道单元(a,bus,bstp,level,ispe,little,aerial,rsp);
//            }
//        });
//        //【第2大块】进入加收收费fee_mod=8：还需提供mod1的金额
    }
    /**每一个管道单元 计算收费, level已经规范化了。
     * */
    private void chargeCalc管道单元(PipingUnit punit, Detail bus, BusinessCat_Enum bstp, String level,boolean ispe,boolean little,boolean aerial,CalcFeeResp rsp)
    {
        //【第1大块】基础收费fee_mod=1
        if(INSTA.equals(bstp) || REFORM.equals(bstp) || REPAIR.equals(bstp) ){    //OPE_TYPE":"2,14,15"
            if(!ispe) {
                if ("GA1".equals(level) || "GA2".equals(level)) {    //GA1、GA2压力管道安装过程监督检验" FEE_COD":"8211"
                    if(little){       //"50≤Ф≤150" FEE_COD":"82112",【一个收费码拆成2个码】"架空、埋地压力管道，收费标准上浮10%","EXTRAFEE_ID":"1"也合并进去;
                        //无法按照普通的fee_mod=8来进行附加费的收费："LAY_MODE=!埋地||LAY_MODE=!架空", RS_EXPR":"(srcPrice*10)/100" #必须直接把fee_code拆分带上附加费;
                        if(aerial && INSTA.equals(bstp))    rsp.addPipingFee(punit,"8A11",  8.8);
                        else    rsp.addPipingFee(punit,"8A12",  8);         //RS_EXPR":"8*LENGTH", FEE_STANDARD":"8元/米"
                    }
                    else {        //"Ф＞150"  82113", RS_EXPR":"9*LENGTH",
                        if(aerial && INSTA.equals(bstp))    rsp.addPipingFee(punit,"8A21",  9.9);    //"架空、埋地压力管道，收费标准上浮10%","EXTRAFEE_ID":"1"
                        else    rsp.addPipingFee(punit,"8A22",  9);     //管道单元每条都aerial不一样！只能把+10%加收项直接摊派个每个单元。
                    }
                }
                else if ("GC1".equals(level) || "GC2".equals(level)) {
                    if(little){       //"50≤Ф≤150" FEE_COD":"82122",
                        if(aerial && INSTA.equals(bstp))    rsp.addPipingFee(punit,"8C11",  8.8);
                        else    rsp.addPipingFee(punit,"8C12",  8);
                    }
                    else {       //"Ф＞150"   82123",
                        if(aerial && INSTA.equals(bstp))    rsp.addPipingFee(punit,"8C21",  9.9);
                        else    rsp.addPipingFee(punit,"8C22",  9);
                    }
                }
                else if ("GC3".equals(level)) {
                    if(little){       //"50≤Ф≤150" FEE_COD":"82122",
                        if(aerial && INSTA.equals(bstp))    rsp.addPipingFee(punit,"8T11",  6.6);
                        else    rsp.addPipingFee(punit,"8T12",  6);
                    }
                    else {       //"Ф＞150"   82123",
                        if(aerial && INSTA.equals(bstp))    rsp.addPipingFee(punit,"8T21",  7.7);
                        else    rsp.addPipingFee(punit,"8T22",  7);
                    }
                }
                else if ("GB1".equals(level) || "GB2".equals(level)) {
                    if(little){       //"50≤Ф≤150" FEE_COD":"82142",
                        if(aerial && INSTA.equals(bstp))    rsp.addPipingFee(punit,"8B11",  8.8);
                        else    rsp.addPipingFee(punit,"8B12",  8);
                    }
                    else {       //"Ф＞150"    3",
                        if(aerial && INSTA.equals(bstp))    rsp.addPipingFee(punit,"8B21",  9.9);
                        else    rsp.addPipingFee(punit,"8B22",  9);
                    }
                }
                else if ("GD1".equals(level) || "GD2".equals(level)) {
                    if(little){       //"50≤Ф≤150" FEE_COD":"82152",
                        if(aerial && INSTA.equals(bstp))    rsp.addPipingFee(punit,"8D11",  8.8);
                        else    rsp.addPipingFee(punit,"8D12",  8);
                    }
                    else {       //"Ф＞150"    3",
                        if(aerial && INSTA.equals(bstp))    rsp.addPipingFee(punit,"8D21",  9.9);
                        else    rsp.addPipingFee(punit,"8D22",  9);
                    }
                }
            }
            else if(INSTA.equals(bstp) ){   //"PE管压力管道安装过程监督检验" FEE_COD":"8310", "EQP_TYPE":"8000","OPE_TYPE":"2" PE管没OPE_TYPE":14,15啊
                if(little){       //"Ф＜150"  FEE_COD":"8311",  ？ PE管 ：钢制: 大小little判别标准不同！ >=150 ？ >150；
                    if(aerial)    rsp.addPipingFee(punit,"8K11",  5.5);
                    else    rsp.addPipingFee(punit,"8K12",  5);
                }
                else {       //"Ф≥150"    2",   BL_EXPR":"NOMINAL_DIA>=150.0"
                    if(aerial)    rsp.addPipingFee(punit,"8K21",  7.7);
                    else    rsp.addPipingFee(punit,"8K22",  7);
                }
            }
        }
        else if(REGUL.equals(bstp) ) {    //OPE_TYPE":"3"
            if(!ispe) {
                if ("GA1".equals(level) || "GA2".equals(level)) {
                    if(little){       //"50≤Ф≤150" FEE_COD":"82212",
                        if(aerial)    rsp.addPipingFee(punit,"8E11",  11);
                        else    rsp.addPipingFee(punit,"8E12",  10);
                    }
                    else {       //"Ф＞150"    3",
                        if(aerial)    rsp.addPipingFee(punit,"8E21",  12.1);
                        else    rsp.addPipingFee(punit,"8E22",  11);
                    }
                }
                else if ("GC1".equals(level) || "GC2".equals(level)) {
                    if(little){       //"50≤Ф≤150" FEE_COD":"82222",
                        if(aerial)    rsp.addPipingFee(punit,"8F11",  9.9);
                        else    rsp.addPipingFee(punit,"8F12",  9);
                    }
                    else {       //"Ф＞150"    3",
                        if(aerial)    rsp.addPipingFee(punit,"8F21",  11);
                        else    rsp.addPipingFee(punit,"8F22",  10);
                    }
                }
                else if ("GC3".equals(level)) {
                    if(little){       //"50≤Ф≤150" FEE_COD":"82232"
                        if(aerial)    rsp.addPipingFee(punit,"8G11",  6.6);
                        else    rsp.addPipingFee(punit,"8G12",  6);
                    }
                    else {       //"Ф＞150"    3",
                        if(aerial)    rsp.addPipingFee(punit,"8G21",  7.7);
                        else    rsp.addPipingFee(punit,"8G22",  7);
                    }
                }
                else if ("GB1".equals(level) || "GB2".equals(level)) {
                    if(little){       //"50≤Ф≤150" FEE_COD":"82242",
                        if(aerial)    rsp.addPipingFee(punit,"8H11",  9.9);
                        else    rsp.addPipingFee(punit,"8H12",  9);
                    }
                    else {       //"Ф＞150"    3",
                        if(aerial)    rsp.addPipingFee(punit,"8H21",  11);
                        else    rsp.addPipingFee(punit,"8H22",  10);
                    }
                }
                else if ("GD1".equals(level) || "GD2".equals(level)) {
                    if(little){       //"50≤Ф≤150" FEE_COD":"82252",
                        if(aerial)    rsp.addPipingFee(punit,"8J11",  9.9);
                        else    rsp.addPipingFee(punit,"8J12",  9);
                    }
                    else {       //"Ф＞150"    3",
                        if(aerial)    rsp.addPipingFee(punit,"8J21",  11);
                        else    rsp.addPipingFee(punit,"8J22",  10);
                    }
                }
            }
            else {   //"PE管压力管道全面检验" FEE_COD":"8320",
                if(little){       //"Ф＜150"  FEE_COD":"8321",
                    if(aerial)    rsp.addPipingFee(punit,"8L11",  6.6);
                    else    rsp.addPipingFee(punit,"8L12",  6);
                }
                else {       //"Ф≥150"    2",
                    if(aerial)    rsp.addPipingFee(punit,"8L21",  9.9);
                    else    rsp.addPipingFee(punit,"8L22",  9);
                }
            }
        }
        else if(ANNUAL.equals(bstp) ) {    //OPE_TYPE":"8"
            if(!ispe) {          //钢制压力管道在线检验" FEE_COD":"8410",  BL_EXPR":"PIPELINE_MEDIUM==钢制"
                if ("GA1".equals(level) || "GA2".equals(level)) {
                    if(little){       //"50≤Ф≤150" FEE_COD":"84112",  "GA1、GA2压力管道在线检验" FEE_COD":"8411"
                        if(aerial)    rsp.addPipingFee(punit,"8M11",  5.5);
                        else    rsp.addPipingFee(punit,"8M12",  5);
                    }
                    else {       //"Ф＞150"   3",        @旧标准还有一个"50>Ф"的收费；收费基数标准确实不相等；
                        if(aerial)    rsp.addPipingFee(punit,"8M21",  5.5);
                        else    rsp.addPipingFee(punit,"8M22",  5);
                    }
                }
                else if ("GC1".equals(level) || "GC2".equals(level)) {
                    if(little){       //"50≤Ф≤150" FEE_COD":"84122",
                        if(aerial)    rsp.addPipingFee(punit,"8N11",  4.4);
                        else    rsp.addPipingFee(punit,"8N12",  4);
                    }
                    else {       //"Ф＞150"    3",
                        if(aerial)    rsp.addPipingFee(punit,"8N21",  4.4);
                        else    rsp.addPipingFee(punit,"8N22",  4);
                    }
                }
                else if ("GC3".equals(level)) {
                    if(little){       //"50≤Ф≤150" FEE_COD":"84132",
                        if(aerial)    rsp.addPipingFee(punit,"8P11",  4.4);
                        else    rsp.addPipingFee(punit,"8P12",  4);
                    }
                    else {       //"Ф＞150"    3",
                        if(aerial)    rsp.addPipingFee(punit,"8P21",  4.4);
                        else    rsp.addPipingFee(punit,"8P22",  4);
                    }
                }
                else if ("GB1".equals(level) || "GB2".equals(level)) {
                    if(little){       //"50≤Ф≤150" FEE_COD":"84142",
                        if(aerial)    rsp.addPipingFee(punit,"8Q11",  4.4);
                        else    rsp.addPipingFee(punit,"8Q12",  4);
                    }
                    else {       //"Ф＞150"    3",
                        if(aerial)    rsp.addPipingFee(punit,"8Q21",  4.4);
                        else    rsp.addPipingFee(punit,"8Q22",  4);
                    }
                }
                else if ("GD1".equals(level) || "GD2".equals(level)) {
                    if(little){       //"50≤Ф≤150" FEE_COD":"84152",
                        if(aerial)    rsp.addPipingFee(punit,"8R11",  4.4);
                        else    rsp.addPipingFee(punit,"8R12",  4);
                    }
                    else {       //"Ф＞150"    3",
                        if(aerial)    rsp.addPipingFee(punit,"8R21",  4.4);
                        else    rsp.addPipingFee(punit,"8R22",  4);
                    }
                }
            }
            else{         //PE管压力管道在线检验" FEE_COD":"8420"
                if(little){       //"Ф＜150"  FEE_COD":"8421",
                    if(aerial)    rsp.addPipingFee(punit,"8S11",  3.3);
                    else    rsp.addPipingFee(punit,"8S12",  3);
                }
                else {       //"Ф≥150"    8422",
                    if(aerial)    rsp.addPipingFee(punit,"8S21",  5.5);
                    else    rsp.addPipingFee(punit,"8S22",  5);
                }
            }
        }
    }
    /**合计收费总金额, 最后总金额==([mod1+mod2 +mod8 ?$mod5?<500]) *mod9 +mod6;
     * mod5特殊处理,    【合计mod1+2+8后的最低标准】
     * */
    private void chargeCalcSumMod5Floor(BusinessCat_Enum bstp, String type, String sort, CalcFeeResp rsp){
        //【前提】收费项目：后端自动计算和手动人工前端选择只能二这一，不允许两个场景都能添加。 rsp.getFees()是当前自动计算的；而rsp.getBus().getFees()是数据库保存的；
        double autoSum;      //后端自动计算合计金额
        List<Double>  autoSumL= rsp.getFees().stream().filter(item ->
                ((!item.getManual()) && (1==item.getFm() || 8==item.getFm()|| 2==item.getFm()))
        ).map(Charging::getAmount).collect(Collectors.toList());
        autoSum= autoSumL.stream().reduce(Double::sum).orElseGet(()-> (double) 0);
        double manualSum;    //前端人工选择的收费项目合计金额。 mod1只能后端搞;
        List<Double>  manualSumL=rsp.getBus().getFees().stream().filter(item ->
                ((item.getManual()) && (2==item.getFm() || 8==item.getFm()))
        ).map(Charging::getAmount).collect(Collectors.toList());
        manualSum= manualSumL.stream().reduce(Double::sum).orElseGet(()-> (double) 0);
        double  mod128Total= autoSum + manualSum;    //所有mod1+2+8收费项目总金额
        if("8".equals(type)) {          //压力管道: EQP_TYPE":"8000"OPE_TYPE":"2,3,8":"检验费低于500元的按500元收取","EXTRAFEE_ID":"109",
            if (INSTA.equals(bstp) || REGUL.equals(bstp) || ANNUAL.equals(bstp)) {     //OPE_TYPE":"2,3,8"
                //"检验费低于500元的按500元收取","EXTRAFEE_ID":"109",｛mod1+mod2+mod8总收费>=500｝
                if (mod128Total < 500) {        //触发fee_mod=5 最低的保底金额=500；
                    rsp.addFeeItem("A109", 500 - mod128Total, 5);
                    mod128Total= 500;           //保持【一致】
                }
            }
        }
        //mod9 系数合计：｛自动计算和手动人工前端选择只能二这一｝，不允许前后端都能添加。
        List<Double>  autoMod9L= rsp.getFees().stream().filter(item ->
                ((!item.getManual()) && 9==item.getFm() )
        ).map(Charging::getAmount).collect(Collectors.toList());
        AtomicReference<Double> mod9sumFactor= new AtomicReference<>(autoMod9L.stream().reduce((a, b) -> {
            return a * b;
        }).orElseGet(() -> (double) 1));
        AtomicReference<Double> sumMod6= new AtomicReference<>((double) 0);   //mod6 特别定制条款,代码"B107",允许前端自主添加多条的"B107"
        //mod9sumFactor *= rsp.getBus().getFees().stream().filter(charging -> charging.getManual() && 9 == charging.getFm()).mapToDouble(Charging::getAmount).reduce(1, (a, b) -> a * b);
        rsp.getBus().getFees().forEach(charging -> {
            if(charging.getManual()) {
                if ( 9 == charging.getFm())
                    mod9sumFactor.updateAndGet(v -> (double) (v * charging.getAmount()));
                else if ( 6 == charging.getFm())     //mod6定制条款
                    sumMod6.updateAndGet(v -> (double) (v + charging.getAmount()));
            }
        });
        double endTotal= mod128Total * mod9sumFactor.get() +sumMod6.get();
        if(endTotal<0)   endTotal=0;
        rsp.getBus().setSprice(endTotal);
    }
    /**安全阀 自动计算,
     * */
    private void chargeCalc安全阀(Detail bus,BusinessCat_Enum bstp,CalcFeeResp rsp) {
        //【第1大块】基础收费fee_mod=1
        if (OTHER.equals(bstp)) {        //OPE_TYPE":"13",
            if(null==bus.getOpprn() || bus.getOpprn()<0.1)   rsp.getParms().add("opprn");
            if(null==bus.getDiame() || bus.getDiame()<5)   rsp.getParms().add("diame");
            if(rsp.getParms().size()<=0) {
                if (Boolean.TRUE.equals(bus.isOnline())) {         //安全阀在线校验" FEE_COD":"F200" , 元/只
                    if (bus.getOpprn() < 1.6) {        //安全阀 "13" 业务都是：单只出报告Isp?     元/只;  安全阀离线校验（Pg≤1.6）"
                        if (bus.getDiame() < 32) rsp.useBase("F211", 500);
                        else if (bus.getDiame() < 40) rsp.useBase("F212", 800);
                        else if (bus.getDiame() < 80) rsp.useBase("F213", 950);
                        else if (bus.getDiame() < 150) rsp.useBase("F214", 1200);
                        else   rsp.useBase("F215", 1500);
                    }
                    else if (bus.getOpprn() < 3.8) {        //安全阀在线校验（1.6≤Pg＜3.8）" FEE_COD":"F220"
                        if (bus.getDiame() < 32) rsp.useBase("F221", 750);
                        else if (bus.getDiame() < 40) rsp.useBase("F222", 1200);
                        else if (bus.getDiame() < 80) rsp.useBase("F223", 1425);
                        else if (bus.getDiame() < 150) rsp.useBase("F224", 1800);
                        else   rsp.useBase("F225", 2250);
                    }
                    else if (bus.getOpprn() < 5.0) {
                        if (bus.getDiame() < 32) rsp.useBase("F231", 1000);
                        else if (bus.getDiame() < 40) rsp.useBase("F232", 1600);
                        else if (bus.getDiame() < 80) rsp.useBase("F233", 1900);
                        else if (bus.getDiame() < 150) rsp.useBase("F234", 2400);
                        else   rsp.useBase("F235", 3000);
                    }
                    else if (bus.getOpprn() < 7.0) {
                        if (bus.getDiame() < 32) rsp.useBase("F241", 1500);
                        else if (bus.getDiame() < 40) rsp.useBase("F242", 2400);
                        else if (bus.getDiame() < 80) rsp.useBase("F243", 2850);
                        else if (bus.getDiame() < 150) rsp.useBase("F244", 3600);
                        else   rsp.useBase("F245", 4500);
                    }
                    else {       //安全阀在线校验（Pg≥7.0）" FEE_COD":"F250"
                        if (bus.getDiame() < 32) rsp.useBase("F251", 2000);
                        else if (bus.getDiame() < 40) rsp.useBase("F252", 3200);
                        else if (bus.getDiame() < 80) rsp.useBase("F253", 3800);
                        else if (bus.getDiame() < 150) rsp.useBase("F254", 4800);
                        else   rsp.useBase("F255", 6000);
                    }
                } else {       //默认是：离线校验
                    if (bus.getOpprn() <= 1.6) {        //安全阀 "13" 业务都是：单只出报告Isp?     元/只;  安全阀离线校验（Pg≤1.6）"
                        if (bus.getDiame() < 32) rsp.useBase("F111", 25);
                        else if (bus.getDiame() < 40) rsp.useBase("F112", 52);
                        else if (bus.getDiame() < 80) rsp.useBase("F113", 115);
                        else if (bus.getDiame() < 150) rsp.useBase("F114", 140);
                        else if (bus.getDiame() < 200) rsp.useBase("F115", 200);
                        else
                            rsp.useBase("F116", 250 + 3 * (bus.getDiame() - 200));  //RS_EXPR":"(GC_CALIBER*3)-350",  FEE_STANDARD":"250+3×(Ф-200)"
                    } else if (bus.getOpprn() <= 9.8) {      //"安全阀离线校验（1.6＜Pg≤9.8）"  FEE_COD":"F120"
                        if (bus.getDiame() < 32) rsp.useBase("F121", 50);
                        else if (bus.getDiame() < 40) rsp.useBase("F122", 104);
                        else if (bus.getDiame() < 80) rsp.useBase("F123", 230);
                        else if (bus.getDiame() < 150) rsp.useBase("F124", 280);
                        else if (bus.getDiame() < 200) rsp.useBase("F125", 400);
                        else
                            rsp.useBase("F126", 500 + 6 * (bus.getDiame() - 200));  //FEE_STANDARD":"500+6×(Ф-200)",
                    } else {           //安全阀离线校验（Pg>9.8）"  FEE_COD":"F130"
                        if (bus.getDiame() < 32) rsp.useBase("F131", 75);
                        else if (bus.getDiame() < 40) rsp.useBase("F132", 156);
                        else if (bus.getDiame() < 80) rsp.useBase("F133", 345);
                        else if (bus.getDiame() < 150) rsp.useBase("F134", 420);
                        else if (bus.getDiame() < 200) rsp.useBase("F135", 600);
                        else
                            rsp.useBase("F136", 750 + 9 * (bus.getDiame() - 200));    //FEE_STANDARD":"750+9×(Ф-200)",
                    }
                }
            }
        }
        else if(SAFETYINS.equals(bstp)) {    //OPE_TYPE":"4"
            if(null==bus.getZmoney() || bus.getZmoney()<500)  rsp.getParms().add("zmoney");
            if(!StringUtils.hasText(bus.getFcode()))    rsp.getParms().add("fcode");
            if(rsp.getParms().size()<=0) {      //进口承压类特种设备安全性能监督检验收费标准" FEE_COD":"J000"
                if("J170".equals(bus.getFcode()))  rsp.useBase(bus.getFcode(), bus.getZmoney()*0.01);
                else if("J270".equals(bus.getFcode()))  rsp.useBase(bus.getFcode(), bus.getZmoney()*0.02);
            }
        }
        double mod1fee =rsp.addUpMod1fee();    //合计fee_mod 1:
        //【第2大块】进入加收收费fee_mod=8：还需提供mod1的金额
        if (OTHER.equals(bstp)) {        //OPE_TYPE":"13",
            if(Boolean.TRUE.equals(bus.isTest() )) {    //使用安全阀在线检测仪进行安全阀在线排放试验，按安全阀在线校验费的20%收费","EXTRAFEE_ID":"103"
                rsp.addFeeItem("A103", mod1fee*0.2 ,8);
            }
            if(Boolean.TRUE.equals(bus.getNtscop() )) {    //"对安全阀密封面直径进行现场检测计算的，按安全阀在线校验费的20%收取","EXTRAFEE_ID":"102"
                rsp.addFeeItem("A102", mod1fee*0.2 ,8);
            }
            if(Boolean.TRUE.equals(bus.isApprp() )) {    //"检验机构到使用单位进行离线安全阀校验的，校验费上浮50%","EXTRAFEE_ID":"99"
                rsp.addFeeItem("A99", mod1fee*0.5 ,8);
            }
            if(Boolean.TRUE.equals(bus.isImpor() )) {    //"进口安全阀在线校验按标准收费的200%收取","EXTRAFEE_ID":"100"
                rsp.addFeeItem("A100", mod1fee ,8);
            }
            if(Boolean.TRUE.equals(bus.isCheap() )) {    //"采用实跳方式进行安全阀在线校验的，收费标准下浮50%","EXTRAFEE_ID":"105"
                rsp.addFeeItem("A105", mod1fee*(-0.5) ,8);
            }
        }
    }
    /**水质 自动计算, 【推定】锅炉水质  Z000 OPE_TYPE:"11" ， 有机热载体； Z000 OPE_TYPE:"12"
     * */
    private void chargeCalc水质(Detail bus,BusinessCat_Enum bstp,CalcFeeResp rsp) {
        if (TEST.equals(bstp)) {        //OPE_TYPE":"11",锅炉水质
            if(bus.getTurbi()>0) {    //"浊度" FEE_COD":"S110"
                rsp.useBase("S110",0, 25,bus.getTurbi());
            }
            if(bus.getHardd()>0) {    //"硬度"  "S120",
                rsp.useBase("S120",0, 25,bus.getHardd());
            }
            if(bus.getPhval()>0)    rsp.useBase("S130",0, 25,bus.getPhval());   //"PH值"  S130",
            if(bus.getConduc()>0)    rsp.useBase("S140",0, 25,bus.getConduc());   //"电导率"  4,
            if(bus.getPalka()>0)    rsp.useBase("S150",0, 25,bus.getPalka());   //"酚酞碱度" 5,
            if(bus.getTotalk()>0)    rsp.useBase("S160",0, 25,bus.getTotalk());   //"全碱度（总碱度）"  6,
            if(bus.getDsolid()>0)    rsp.useBase("S170",0, 30,bus.getDsolid());   //"溶解固形物"  7,
            if(bus.getPhosph()>0)    rsp.useBase("S180",0, 30,bus.getPhosph());   //"磷酸根" 8,
            if(bus.getOilw()>0)    rsp.useBase("S190",0, 30,bus.getOilw());   //"油"  9,
            if(bus.getTiron()>0)    rsp.useBase("S1A0",0, 30,bus.getTiron());   //"全铁" "S1A0",
            if(bus.getSulfit()>0)    rsp.useBase("S1B0",0, 30,bus.getSulfit());   //"亚硫酸根"  B,
            if(bus.getRebasi()>0)    rsp.useBase("S1C0",0, 30,bus.getRebasi());   //"相对碱度" "S1C0",
            if(bus.getChrion()>0)    rsp.useBase("S1D0",0, 30,bus.getChrion());   //"氯离子"  D,
            if(bus.getDioxy()>0)    rsp.useBase("S1E0",0, 200,bus.getDioxy());   //"溶解氧"  E,
            if(bus.getCarbon()>0)    rsp.useBase("S1F0",0, 60,bus.getCarbon());   //"水垢全分析" F,
            if(bus.getCaptem()>0)    rsp.useBase("S1G0",0, 50,bus.getCaptem());   //"树脂交换容量分析" FEE_COD":"S1G0"
        }
        else if (EXPERIMENT.equals(bstp)) {        //OPE_TYPE":"12",有机热载体
            if(bus.getTurbi()>0) {    //"运动黏度" FEE_COD":"T110",
                rsp.useBase("T110",0, 130,bus.getTurbi());
            }
            if(bus.getPhosph()>0) {    //"酸值"  "T120",
                rsp.useBase("T120",0, 130,bus.getPhosph());
            }
            if(bus.getCarbon()>0) {    //"残炭"   130",
                rsp.useBase("T130",0, 130,bus.getCarbon());
            }
            if(bus.getConduc()>0) {    //"闭口闪点"  4,
                rsp.useBase("T140",0, 130,bus.getConduc());
            }
            if(bus.getOilw()>0) {    //"水分"   5,
                rsp.useBase("T150",0, 130,bus.getOilw());
            }
            if(bus.getCaptem()>0) {    //"5%低沸物馏出温度"  6,
                rsp.useBase("T160",0, 130,bus.getCaptem());
            }
            if(bus.getPhval()>0) {    //"水溶性酸碱"  7,
                rsp.useBase("T170",0, 130,bus.getPhval());
            }
            if(bus.getHardd()>0) {    //"密度"  8,
                rsp.useBase("T180",0, 130,bus.getHardd());
            }
        }
    }
    /**管道元件 自动计算
     * */
    private void chargeCalc管道元件(Detail bus,BusinessCat_Enum bstp,CalcFeeResp rsp) {
        if (MANUFACT.equals(bstp)) {        //OPE_TYPE":"1"
            if (null == bus.getZmoney() || bus.getZmoney() < 500)   rsp.getParms().add("zmoney");
            else {
                //压力管道元件制造质量监督检验" FEE_COD":"8110" RS_EXPR":"(EQP_PRICE*1)/100", "按受检产品设备价的1.0%收取"
                rsp.useBase("8110", bus.getZmoney() * 0.01);
            }
        }
        //旧的业务？ PRODUCT.equals(bstp)  OPE_TYPE":"617"  阀门检验；      另外:管道元件+OPE_TYPE":"4"删除；
    }
    /**常压容器 自动计算收费; 旧平台没有使用收费标准=全人工自己定价格。 汽车运输液体危险货物常压容器（罐体）内外部检验"  FEE_COD":"Q300"
     * */
    private void chargeCalc常压容器(Vessel eqp, Detail bus, BusinessCat_Enum bstp, String sort, String vart,CalcFeeResp rsp) {
        //【第1大块】基础收费fee_mod=1
        if (ANNUAL.equals(bstp)) {    //【推断】OPE_TYPE":"8", 委托的年度检验 OPE_TYPE: "608"
            if (bus.getVarea() < 2)       //委托的不用检查参数 合格性?
                rsp.getParms().add("varea");
            else {           //"内外部检验" FEE_COD":"Q310", RS_EXPR":"50",  元/立方米[?奇怪]
                rsp.useBase("Q310", 0, 50, bus.getVarea());
            }
            //"导静电测试"  FEE_COD":"Q360", RS_EXPR":"20";  FEE_MOD":"2"
            if (bus.getNum() >0)  rsp.addFeeItem("Q360", 20*bus.getNum(),2, bus.getNum().doubleValue());
            if(!Boolean.TRUE.equals(bus.getNtscop() )) {      //有些根本就没有这一项目的收费? "气密性试验"  Q330
                //"气密性试验"  Q330, RS_EXPR":"25"  FEE_MOD":"2"
                Double gasTight=null;
                String  nVvmemo2=null;
                gasTight=Tool.castDouble(eqp.getVol());
                if(null==gasTight || gasTight<0.02)    rsp.getParms().add("vol");       //检查明显不正常的 CONTAINERVOLUME 参数！
                else if(!Tool.testNumericalNormal(gasTight,eqp.getVol()))    nVvmemo2= "计算容积=" + gasTight + ";台账容积=" + eqp.getVol();
                if(rsp.getParms().size()<=0) {
                    rsp.addFeeItem("Q330", 25 * gasTight, 2, gasTight,nVvmemo2);
                }
            }
            //"通气阀性能试验"  Q340, RS_EXPR":"40";  FEE_MOD":"2" ?乘数   */只   ？参数 isum只 ；
            if (null!=bus.getIsum() && bus.getIsum() >0)   rsp.addFeeItem("Q340", 40*bus.getIsum(),2, bus.getIsum().doubleValue());
            //"其他附件检查"  Q350", RS_EXPR":"10" , FEE_MOD":"2" ?乘数    */只乘数?(可变动乘数，改成 Float)改成 随意乘数单位。
            if (bus.getTotm()>0 ) {
                rsp.addFeeItem("Q350", 10*bus.getTotm(),2, bus.getTotm().doubleValue());
            }
            //"水压试验"  Q320", RS_EXPR":"15" , FEE_MOD":"2" ?乘数 */m3
            if (bus.getDiame() >0)   rsp.addFeeItem("Q320", 15*bus.getDiame(),2, bus.getDiame().doubleValue());
            //"盛水试验"  Q370", RS_EXPR":"15" , FEE_MOD":"2" ?乘数 */m3
            if (null!=bus.getRustc() && bus.getRustc() >0)   rsp.addFeeItem("Q370", 15*bus.getRustc(),2, bus.getRustc().doubleValue());
        }
    }
    /**前端手动就做的添加修改收费项
     *针对某一个实体模型的：新增C，修改U，删除D 3个case都能收进这个接口：聚合 cud+模型名称+;
     * */
    @MutationMapping
    @Transactional
    public CudFeeItemResp cudChargingFee(@Argument String busTaskId, @Argument Ifop_Enu opt, @Argument FeeItemInput inp) {
        CudFeeItemResp resp=new CudFeeItemResp();
        Tool.ResolvedGuuid gId= Tool.fromGluuId(busTaskId);
        gId.getType();  //“Detail”  todo:    UUID?
        Detail bus=null;
        if("Detail".equals(gId.getType())) {
            bus = detailRepository.findById(gId.getId()).orElse(null);
            Assert.isTrue(bus != null, "未找到Detail:" + busTaskId);
            Isp isp =bus.getIsp();
            Assert.isTrue(isp != null, "未找到Isp:" + bus.getId());
        }
        Charging charging=null;
        if(null!=bus && Ifop_Enu.ADD.equals(opt)) {
            charging = cudChargingFee增加(bus, inp, resp);
            if(null==charging.getCode())    resp.setWarn("非法收费");
            else {      //增加收费项！
                charging.setDetail(bus);
                chargingRepository.save(charging);
                bus.getFees().add(charging);
                resp.setFee(charging);
            }
        }
        else if(null!=bus && Ifop_Enu.DEL.equals(opt))   cudChargingFee删除(bus,inp,resp);
        resp.setBus(bus);
        return resp;
    }
    /**前端人工选择收费输入的; 针对收费项代码逐个处理*/
    public Charging cudChargingFee增加(Detail bus, FeeItemInput inp, CudFeeItemResp resp) {
        Charging charging=new Charging();
        Isp isp =bus.getIsp();
        Assert.isTrue(inp != null && StringUtils.hasText(inp.getCode()), "inp参数:" + bus.getId());
        BusinessCat_Enum bstp=isp.getBsType();
        String  type,  sort, vart="", subv="";
        Object unproxyEqp =null;
        if(null!=isp.getDev()) {
            unproxyEqp = Hibernate.unproxy(isp.getDev());     //报错$HibernateProxy$bXbHH2io cannot be cast to class md.specialEqp.type.
            type = isp.getDev().getType();
            sort = isp.getDev().getSort();
            vart = isp.getDev().getVart();
            subv = isp.getDev().getSubv();
        }
        else{       //单独报告，没有设备台账的业务。if ("F".equals(type))  安全阀;
            type =bus.getType();
            sort="";
        }
        //前端可能提供的收费代码：按照收费编码的@首字母@做大类的拆解分支分块思路。
        if(inp.getCode().charAt(0)=='2') {
            if ("2".equals(type)) {
                Vessel eqp=(Vessel)unproxyEqp;
                if ("2370".equals(inp.getCode()) || "2380".equals(inp.getCode())) {
                    Double gn=null;     //基本分叉参数2种：CONTAINERVOLUME容积或 DESPRE压力  =gn 根据设备种类和业务类型具体定夺;
                    String  nVvmemo=null;    //提示CONTAINERVOLUME: eqp.vol参数 或 DESPRE 参数可能存在问题。
                    if(("213".equals(vart) || "215".equals(vart) || "217".equals(vart)) ) {
                        gn=Tool.castDouble(eqp.getVol());
                        if(null==gn || gn<0.02)    resp.setWarn("容积非法");
                        else if(!Tool.testNumericalNormal(gn,eqp.getVol()))    nVvmemo= "V=" + gn + ";台账容积=" + eqp.getVol();
                    }
                    else if("211".equals(vart) ) {    //基本分叉参数:第二种 DESPRE
                        gn=Tool.castDouble(eqp.getPrs());
                        if(null==gn || gn<10)    resp.setWarn("压力非法");
                        else if(!Tool.testNumericalNormal(gn,eqp.getPrs()))    nVvmemo= "MPa=" + gn + ";台账压力=" + eqp.getPrs();
                    }
                    if(null==resp.getWarn()){
                        if("217".equals(vart)){     //一类压力容器耐压试验" 手选 FEE_COD":"2370"
                            if("2370".equals(inp.getCode())) {
                                if (gn < 1) charging.fillFeeFe("2371", 150, 2, null, nVvmemo);
                                else if (gn < 10) charging.fillFeeFe("2372", 170, 2, null, nVvmemo);
                                else if (gn < 20) charging.fillFeeFe("2373", 180, 2, null, nVvmemo);
                                else if (gn < 50) charging.fillFeeFe("2374", 200, 2, null, nVvmemo);
                                else if (gn < 100) charging.fillFeeFe("2375", 300, 2, null, nVvmemo);
                                else charging.fillFeeFe("2376", 3 * gn, 2, null, nVvmemo);
                            }
                            else{    //一类压力容器气密性试验" 手选 FEE_COD":"2380"
                                if (gn < 1) charging.fillFeeFe("2381", 50, 2, null, nVvmemo);
                                else if (gn < 10) charging.fillFeeFe("2382", 80, 2, null, nVvmemo);
                                else if (gn < 20) charging.fillFeeFe("2383", 130, 2, null, nVvmemo);
                                else if (gn < 50) charging.fillFeeFe("2384", 190, 2, null, nVvmemo);
                                else if (gn < 100) charging.fillFeeFe("2385", 320, 2, null, nVvmemo);
                                else charging.fillFeeFe("2386", 4 * gn, 2, null, nVvmemo);
                            }
                        }else if("215".equals(vart)){
                            if("2370".equals(inp.getCode())) {   //二类压力容器耐压试验" 手选 FEE_COD":"2390"
                                if (gn < 1) charging.fillFeeFe("2391", 195, 2, null, nVvmemo);
                                else if (gn < 10) charging.fillFeeFe("2392", 221, 2, null, nVvmemo);
                                else if (gn < 20) charging.fillFeeFe("2393", 234, 2, null, nVvmemo);
                                else if (gn < 50) charging.fillFeeFe("2394", 260, 2, null, nVvmemo);
                                else if (gn < 100) charging.fillFeeFe("2395", 390, 2, null, nVvmemo);
                                else charging.fillFeeFe("2396", 3.9 * gn, 2, null, nVvmemo);
                            }
                            else{    //二类压力容器气密性试验" 手选 FEE_COD":"23A0"
                                if (gn < 1) charging.fillFeeFe("23A1", 65, 2, null, nVvmemo);
                                else if (gn < 10) charging.fillFeeFe("23A2", 104, 2, null, nVvmemo);
                                else if (gn < 20) charging.fillFeeFe("23A3", 169, 2, null, nVvmemo);
                                else if (gn < 50) charging.fillFeeFe("23A4", 247, 2, null, nVvmemo);
                                else if (gn < 100) charging.fillFeeFe("23A5", 416, 2, null, nVvmemo);
                                else charging.fillFeeFe("23A6", 5.2 * gn, 2, null, nVvmemo);
                            }
                        }else if("213".equals(vart)){
                            if("2370".equals(inp.getCode())) {   //三类压力容器耐压试验" 手选 FEE_COD":"23B0"
                                if (gn < 1) charging.fillFeeFe("23B1", 225, 2, null, nVvmemo);
                                else if (gn < 10) charging.fillFeeFe("23B2", 255, 2, null, nVvmemo);
                                else if (gn < 20) charging.fillFeeFe("23B3", 270, 2, null, nVvmemo);
                                else if (gn < 50) charging.fillFeeFe("23B4", 300, 2, null, nVvmemo);
                                else if (gn < 100) charging.fillFeeFe("23B5", 450, 2, null, nVvmemo);
                                else charging.fillFeeFe("23B6", 4.5 * gn, 2, null, nVvmemo);
                            }
                            else{    //三类压力容器气密性试验" 手选 FEE_COD":"23C0"
                                if (gn < 1) charging.fillFeeFe("23C1", 75, 2, null, nVvmemo);
                                else if (gn < 10) charging.fillFeeFe("23C2", 120, 2, null, nVvmemo);
                                else if (gn < 20) charging.fillFeeFe("23C3", 195, 2, null, nVvmemo);
                                else if (gn < 50) charging.fillFeeFe("23C4", 285, 2, null, nVvmemo);
                                else if (gn < 100) charging.fillFeeFe("23C5", 480, 2, null, nVvmemo);
                                else charging.fillFeeFe("23C6", 6 * gn, 2, null, nVvmemo);
                            }
                        }
                        else if("211".equals(vart)){
                            if("2370".equals(inp.getCode())) {   //"超高压容器耐压试验" 手选 FEE_COD":"2430"
                                if(gn<200)    charging.fillFeeFe("2431", 1000, 2, null, nVvmemo);
                                else if(gn<500)  charging.fillFeeFe("2432", 1200, 2, null, nVvmemo);
                                else   charging.fillFeeFe("2433", 1400, 2, null, nVvmemo);
                            }
                        }
                    }
                }
            }
        }else if(inp.getCode().charAt(0)=='M') {
            if(inp.getCode().charAt(1)=='2'){       //M2xx: 4种FeeC("M210"),FeeC("M220"),FeeC("M230"),FeeC("M240")
                if ("2".equals(type)) {
                    int bcls = inp.getCode().charAt(2)-'0';
                    int pvi = inp.getCode().charAt(3)-'0';
                    double feeit = 容器设计收费[bcls-1][pvi-1];       //正好工整，排序好的。
                    charging.fillFeeFe(inp.getCode(), feeit, 2, null, null);
                }
            }
            else if(inp.getCode().charAt(1)=='1' || "M310".equals(inp.getCode())){
                //7种情况： "M110"),FeeC("M120"),FeeC("M130"),FeeC("M140"),FeeC("M150"),FeeC("M160"),FeeC("M310"
                if ("1".equals(type)) {
                    Boiler eqp = (Boiler) unproxyEqp;
                    Float gn = eqp.getPower();     //RATCONS 额定功率(MW)/蒸发量=gn
                    if (null == gn || gn <= 0) resp.setWarn("蒸发量非法");
                    else {      //"锅炉修理方案审查" @ 手选 @ FEE_COD":"M110"
                        int clst= inp.getCode().charAt(2)-'0';
                        if ("M310".equals(inp.getCode()))   clst=7;
                        final int [] divPam={1,2,4,6,10,20,35};
                        int gnIdx=Tool.seqAreaCompare(gn, divPam);
                        double feeit = 锅炉设计收费[clst-1][gnIdx];
                        String newCod=inp.getCode().substring(0,3);    //前3个字符
                        charging.fillFeeFe(newCod+(gnIdx+1), feeit, 2, null, null);
                    }
                }
            }else if("M320".equals(inp.getCode())){
                if ("1".equals(type)){  //"锅炉设计文件备案" 手选 FEE_COD":"M320"
                    charging.fillFeeFe("M320", 450, 2, null, null);
                }
            }
        }else if(inp.getCode().charAt(0)=='1'){
            if ("1".equals(type)){
                Boiler eqp=(Boiler)unproxyEqp;
                Float gn=eqp.getPower();     //RATCONS 额定功率(MW)/蒸发量=gn
                if("1430".equals(inp.getCode())) {
                    if (null == gn || gn <= 0) resp.setWarn("蒸发量非法");
                    else {
                        if (!"1002".equals(subv)) {   //"工业锅炉水压试验" @手选 FEE_MOD":"2" FEE_COD":"1430"
                            if (gn <= 1) charging.fillFeeFe("1431", 160, 2, null, null);
                            else if (gn <= 2) charging.fillFeeFe("1432", 170, 2, null, null);
                            else if (gn <= 4) charging.fillFeeFe("1433", 180, 2, null, null);
                            else if (gn <= 6) charging.fillFeeFe("1434", 200, 2, null, null);
                            else if (gn <= 10) charging.fillFeeFe("1435", 210, 2, null, null);
                            else if (gn <= 20) charging.fillFeeFe("1436", 230, 2, null, null);
                            else charging.fillFeeFe("1437", 350, 2, null, null);
                        } else {
                            if ("垃圾".equals(eqp.getFuel()) || "生活垃圾".equals(eqp.getFuel())) {   //垃圾焚烧发电锅炉水压试验" @手选 FEE_COD":"1630"
                                if (gn <= 35) charging.fillFeeFe("1631", 50 * gn, 2, null, null);
                                else if (gn <= 75) charging.fillFeeFe("1632", 50 * gn, 2, null, null);
                                else if (gn <= 130) charging.fillFeeFe("1633", 450 + 44 * gn, 2, null, null);
                                else if (gn <= 220) charging.fillFeeFe("1634", 970 + 40 * gn, 2, null, null);
                                else if (gn <= 400) charging.fillFeeFe("1635", 4930 + 22 * gn, 2, null, null);
                                else if (gn <= 670) charging.fillFeeFe("1636", 10530 + 8 * gn, 2, null, null);
                                else if (gn <= 1000) charging.fillFeeFe("1637", 11870 + 6 * gn, 2, null, null);
                                else charging.fillFeeFe("1638", 15870 + 2 * gn, 2, null, null);
                            } else {    //电站锅炉水压试验" 手选 FEE_COD":"1530"
                                if (gn <= 35) charging.fillFeeFe("1531", 25 * gn, 2, null, null);
                                else if (gn <= 75) charging.fillFeeFe("1532", 25 * gn, 2, null, null);
                                else if (gn <= 130) charging.fillFeeFe("1533", 225 + 22 * gn, 2, null, null);
                                else if (gn <= 220) charging.fillFeeFe("1534", 485 + 20 * gn, 2, null, null);
                                else if (gn <= 400) charging.fillFeeFe("1535", 2465 + 11 * gn, 2, null, null);
                                else if (gn <= 670) charging.fillFeeFe("1536", 5265 + 4 * gn, 2, null, null);
                                else if (gn <= 1000) charging.fillFeeFe("1537", 5935 + 3 * gn, 2, null, null);
                                else charging.fillFeeFe("1538", 7935 + gn, 2, null, null);
                            }
                        }
                    }
                }
            }
        }else if(inp.getCode().charAt(0)=='S'){
            if ("1".equals(type)) {
                if("S210".equals(inp.getCode()))    //"电站锅炉水汽质量检验"  手选  FEE_COD":"S210"
                    charging.fillFeeFe("S210", 2500, 2, null, null);
                else if("S220".equals(inp.getCode()))
                    charging.fillFeeFe("S220", 4800, 2, null, null);
            }
        }else if(inp.getCode().charAt(0)=='H'){
            if("H100".equals(inp.getCode()))    //"采用真空度测试" 手选 FEE_COD":"H100"
                charging.fillFeeFe("H100", 60, 2, null, null);
            else if("H200".equals(inp.getCode()))
                charging.fillFeeFe("H200", 4000, 2, null, null);
        }else if(inp.getCode().charAt(0)=='L'){
            if(null!=inp.getMnum() && inp.getMnum()>0)    //"理化、金相试验收费标准" @手选  FEE_COD":"L000",  6个分支；13子项
            {
                double amount=金相试验收费.get(inp.getCode()) * inp.getMnum();
                charging.fillFeeFe(inp.getCode(), amount, 2, inp.getMnum(), null);
            }
        }else if(inp.getCode().charAt(0)=='W'){
            if(null!=inp.getMnum() && inp.getMnum()>0)
            {   //无损检测和其他检测项目收费标准（现场检测）"  ^手选  FEE_COD":"W000", ^FEE_MOD":"2",  11个分支;  @全部30子项目
                String mCode=inp.getCode().substring(1);        //首字母剔除
                double amount=无损检测收费.get(mCode) * inp.getMnum();
                charging.fillFeeFe('W'+mCode, amount, 2, inp.getMnum(), null);
            }
        }else if(inp.getCode().charAt(0)=='B'){
            String mCode=inp.getCode().substring(1);
            if("107".equals(mCode)){        //EXTRAFEE_ID":"107"；"EXTRAFEE_NAME":"其它加收、减收条款"
                if(StringUtils.hasText(inp.getMemo()) && null!=inp.getAmount() && 0!=inp.getAmount())
                    charging.fillFeeFe("B107", inp.getAmount(), 6, null, inp.getMemo());
            }else{
                Double  mpect=null;     //@[加收减免条款] @所有业务 所有设备的     "EXTRAFEE_ID":"82" "B82"
                if("82".equals(mCode)) mpect=1.3;           //"在化学介质、易燃介质等毒性危害和爆炸危险环境下进行检验   RS_EXPR":"(tmpCalPrice*30)/100",
                else if("84".equals(mCode)) mpect=1.2;
                else if("86".equals(mCode)) mpect=1.15;
                else if("85".equals(mCode)) mpect=1.1;
                else if("88".equals(mCode) || "87".equals(mCode)) mpect=0d;
                if("90".equals(mCode) && "1".equals(type) && THERMAL.equals(bstp)) mpect=0d;
                if(null!=mpect)
                    charging.fillFeeFe(inp.getCode(), mpect, 9, null, null);
            }
        }else if(inp.getCode().charAt(0)=='A'){
            String mCode=inp.getCode().substring(1);
            if("108".equals(mCode) || "104".equals(mCode)) {     //先导式安全阀离线校验，由双方协商确定具体收费","EXTRAFEE_ID":"108" A108
                if ("F".equals(type)) {      //无设备台账的: 安全阀;     FEE_MOD=8, mod8是增加收费的+
                    if (null != inp.getAmount() && 0 < inp.getAmount())
                        charging.fillFeeFe(inp.getCode(), inp.getAmount(), 8, null, null);
                }
            }
        }
        return charging;
    }
    /**删除人工录入收费项目*/
    public void cudChargingFee删除(Detail bus, FeeItemInput inp, CudFeeItemResp resp) {
        Tool.ResolvedGuuid gId= Tool.fromGluuId(inp.getId());
        Charging charging=chargingRepository.findById(gId.getId()).orElse(null);
        if(null!=charging) {
            //直接删除后，有关联的实体->Detail.fees[]集合若是在缓存的并没有剔除，导致报错。
            charging.getDetail().getFees().remove(charging);
            chargingRepository.delete(charging);
        }else
            resp.setWarn("没找到");
    }
    /**确认某个Detail的最终收费金额*/
    @MutationMapping
    @Transactional
    public CalcFeeResp confirmIspFee(@Argument String id) {
        Detail bus = entityOf(id,Detail.class);
        Assert.notNull(bus,"未找到Detail:"+id);
        bus.setFeeOk(true);
        CalcFeeResp calcFeeResp = new CalcFeeResp();
        calcFeeResp.setBus(bus);
        return calcFeeResp;
    }
}
