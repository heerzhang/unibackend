/*
 * Copyright 2002-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fjsei.yewu.resolver.base;

import com.querydsl.core.BooleanBuilder;
import graphql.relay.Connection;
import graphql.schema.DataFetchingEnvironment;
import md.cm.base.Companies;
import md.cm.unit.Division;
import md.specialEqp.ReportRepository;
import md.specialEqp.inspect.*;
import md.specialEqp.type.PipingUnit;
import md.system.QUser;
import md.system.User;
import org.fjsei.yewu.filter.SimpleReport;
import org.fjsei.yewu.graphql.DbPageConnection;
import org.fjsei.yewu.graphql.IdMapper;
import org.fjsei.yewu.graphql.MemoryListConnection;
import org.fjsei.yewu.input.IspCommonInput;
import org.fjsei.yewu.jpa.PageOffsetFirst;
import org.fjsei.yewu.resolver.Comngrql;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.Sort;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.graphql.execution.BatchLoaderRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**默认Unit模型的属性； graphQL接口;
 * 底下批处理方案还不是一种的。 注释掉的也是1个方案。
 * 每一个graphQL的Object对象type都需单独声明一个XxxController implements IdMapper<type> {} 否则Id都无法转换成Relay要的GlobalID的。
 * 可直接在实体类定义 String getId(DataFetchingEnvironment env)｛｝就行了，不用：implements IdMapper<Division>模式。
 * */

@Controller
public class DivisionController extends Comngrql {
	public DivisionController(BatchLoaderRegistry registry) {
	}
	/**部门内不归属科室的职员*/
	@SchemaMapping(field="staff")
	public List<User> staff(Division division, DataFetchingEnvironment env) {
		BooleanBuilder builder = new BooleanBuilder();
		QUser qm = QUser.user;
		builder.and(qm.dep.eq(division)).and(qm.office.isNull());
		List<User> list= (List<User>)userRepository.findAll(builder);
		return list;
	}
}
