package org.fjsei.yewu.resolver.sei.original;

import md.cm.unit.Units;
import md.computer.File;
import md.computer.FileRepository;
import md.specialEqp.Equipments;
import md.specialEqp.Report;
import md.specialEqp.ReportRepository;
import md.specialEqp.inspect.IspRepository;
import md.specialEqp.inspect.TaskRepository;
import md.system.AuthorityRepository;
import md.system.UserRepository;
import org.fjsei.yewu.exception.CommonGraphQLException;
import org.fjsei.yewu.security.JwtUserDetailsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.List;

/**按照高内聚，把结合紧密的功能都放在一个模块MutationResolver/QueryResolver文件中。
 * */
@Component
public class ReportMgrMutation {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;
    @Autowired
    private Equipments eQPRepository;
    @Autowired
    private IspRepository iSPRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private ReportRepository reportRepository;
    @Autowired
    private Units units;
    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private FileRepository fileRepository;


    @PersistenceContext(unitName = "entityManagerFactorySei")
    private EntityManager emSei;                //EntityManager相当于hibernate.Session：

//    @Autowired
//    private final JwtTokenUtil jwtTokenUtil=new JwtTokenUtil();

    /*
@Transactional
public OriginalRecord newOriginalRecord(String modeltype, String modelversion, Long ispId, String data) {
    if(!emSei.isJoinedToTransaction())      emSei.joinTransaction();

    Isp isp = iSPRepository.findById(ispId).orElse(null);
    if(isp == null)     throw new BookNotFoundException("没有该ISP", ispId);

    OriginalRecord originalRecord = new OriginalRecord(modeltype,modelversion,isp,data);
    originalRecordRepository.save(originalRecord);
    return originalRecord;
}
    */

//    @Transactional
//    public Report modifyOriginalRecordFiles(Long id, List<Long> fileIDs) {
//        if(!emSei.isJoinedToTransaction())      emSei.joinTransaction();
//        Report originalRecord= reportRepository.findById(id).orElse(null);
//        if(originalRecord == null)     throw new CommonGraphQLException("没有该原始记录", id);
//        fileIDs.stream().forEach(item -> {
//            File file=fileRepository.findById(item).orElse(null);
//            file.getUrl();
//        });
//        originalRecord.setFiles(null);
//
//        reportRepository.save(originalRecord);
//        return originalRecord;
//    }


}
