package org.fjsei.yewu.resolver.incp.module;

import org.fjsei.yewu.entity.incp.JcAuthor;
import org.fjsei.yewu.entity.incp.JcAuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//实际相当于controller;

@Component
public class JcQuery  {
        @Autowired
        private JcAuthorRepository jcauthorRepository;

        public Iterable<JcAuthor> findAllJcAuthors() {
            return jcauthorRepository.findAll();
        }

}
