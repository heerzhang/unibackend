package org.fjsei.yewu.resolver.sei.oldsys;

import md.cm.unit.Units;
import md.specialEqp.Equipments;
import md.specialEqp.ReportRepository;
import md.specialEqp.inspect.IspRepository;
import md.specialEqp.inspect.TaskRepository;
import md.system.AuthorityRepository;
import md.system.User;
import md.system.UserRepository;
import org.fjsei.yewu.entity.fjtj.*;
import org.fjsei.yewu.resolver.Comngrql;
import org.fjsei.yewu.security.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;


@Component
public class SysMgrQuery extends Comngrql {

    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;
    @Autowired
    private Equipments eQPRepository;
    @Autowired
    private IspRepository iSPRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private ReportRepository reportRepository;
    @Autowired
    private Units units;
    @Autowired
    private AuthorityRepository authorityRepository;
    @Autowired
    private EqpMgeRepository eqpMgeRepository;
    @Autowired
    private ElevParaRepository elevParaRepository;
    @Autowired
    private HouseMgeRepository houseMgeRepository;
    @Autowired
    private UntMgeRepository untMgeRepository;


    @PersistenceContext(unitName = "entityManagerFactorySei")
    private EntityManager emSei;


    public EqpMge findEqpMge(Long id) {
        User user= checkAuth();
        if(user==null)   return null;
        //Pageable pageable =PageRequest.of(1,100);
         // List<EqpMge> eqps= eqpMgeRepository.findAllByEQPCODIsLike("%958%");
        EqpMge eqp=eqpMgeRepository.findById(id).orElse(null);

        ElevPara elevPara=elevParaRepository.getByEqpcodEquals(eqp.getEqpcod());
       // Long build_id= eqp.getBUILD_ID();
        //HouseMge houseMge=houseMgeRepository.getOne(5194L); 不可使用getOne，未加载的实体记录就查不到。
        HouseMge houseMge=houseMgeRepository.findById(eqp.getBUILD_ID()).orElse(null);
        UntMge untMge=null;//untMgeRepository.findById(eqp.getUSE_UNT_ID()).orElse(null);

        return eqp;
    }

}


/*
graphql需要参数的接口函数/Type输出isp(带参数)的/等，注意！参数不能直接用POJO的java类来传递参数对象，需要基本数据类型或其嵌套结构；否则报错：
输出可以用java对象，输入却免谈． graphql.kickstart.tools.SchemaError: Expected type 'Isp' to be a GraphQLInputType, but it wasn't!
 Was a type only permitted for object types incorrectly used as an input type, or vice-versa? at graphql.kickstart.tools.SchemaParser.d。
*/
